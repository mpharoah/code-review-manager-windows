# Code Review Manager
A Windows 10 system tray app to keep track of pull requests that you are reviewing

![image](https://gitlab.com/mpharoah/code-review-manager-windows/uploads/ac412e9e2392a42b24c2a262f80c71f0/Image1.png)
![image](https://gitlab.com/mpharoah/code-review-manager-windows/uploads/a3ccfe85bf363fabd12a0bb03215e7a2/Image2.png)

## Q&A

### What version of Windows does this support?

Only 64-bit Windows 10 is supported. It may work on Windows 8 as well, but has only been tested on Windows 10.

### What sites are supported?

The app currently supports integration with GitHub, BitBucket Server, and BitBucket Cloud. GitLab support is planned for version 2.1.0.

### How do I build and install this app?

You don't need to build the app from source. Just go to the [tags page](https://gitlab.com/mpharoah/code-review-manager-windows/tags), then download and run the *.msi* file from the latest release.


If you want to build the app from source, you will need the _Microsoft Visual Studio 2017 Installer Projects_ plugin to build the installer (or disable the installer project and build without it).

### How do I start the app?

The installer will automatically add the app as a startup application to automatically launch when you log in. To launch it for the first time, go to your **Recently Added** apps in the start menu, and you should see it.

### How can I make the app open on a different monitor?

By default, the app expands from the system tray, which Windows only puts on your primary monitor. However, if you disable the **Docked to system tray** setting, the app will instead open as a normal window that you can move around.

### How do I prevent Windows from hiding the system tray icon?

Right click on the taskbar and select Taskbar Settings. Then go to _Select which icons appear on the taskbar_ and flip **CodeReviewManager** to be ON.

### I don't want to receive periodic notifications about pull requests that I am reviewing. Can I disable this feature?

Yes. In the Settings view, check the **Mute new pull requests** option, then choose **Do not unmute** for all 3 unmute triggers.

### Can I change how frequently I am reminded about pull requests I am reviewing?

Yes. Change the **Notification Interval** in the Settings view.

### What is the difference between _Refresh Interval_ and _Notification Interval_?

**Refresh Interval** is the interval at which the app polls the source control server for updates. This affects how quickly the app can notifiy you about being newly assigned to a pull request. If you have the **Refresh immediately on open** option checked, the app will also refresh when you click on it in the system tray. If this option is disabled, the UI will only be updated with the latest pull request information when either the refresh interval or the notification interval passes.

**Notification Interval** is how often the app sends you a notification reminding you about unmuted pull requests you are reviewing. A refresh is automatically done as well. If you have the **Notify on app start** option checked, you will also receive a notification when the app starts.
