﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using CodeReviewManager.Backend.Config;
using CodeReviewManager.Backend.Exceptions;
using CodeReviewManager.Backend.Integrations;
using CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud;
using CodeReviewManager.Backend.Integrations.Implementations.BitBucketServer;
using CodeReviewManager.Backend.Integrations.Implementations.GitHub;
using CodeReviewManager.Backend.Profiles;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Version = CodeReviewManager.Util.Version;

namespace CodeReviewManager.Backend {

	public sealed class CodeReviewManagerCore {

		private static JsonConfigFile<List<PullRequest>> m_cacheFile = new JsonConfigFile<List<PullRequest>>(
			ProfileManager.CurrentProfilePath,
			"cache.json"
		);

		private volatile List<PullRequest> m_pullRequests;
		private volatile IReadOnlyList<string> m_warnings;
		private readonly ISourceControlIntegration[] m_integrations;
		private readonly object m_lock = new object();

		private readonly Timer m_updateTimer;
		private readonly Timer m_notifyTimer;

		private DateTime m_lastUpdate = new DateTime( 1970, 1, 1, 0, 0, 0, DateTimeKind.Utc );
		
		private CodeReviewManagerCore() {
			m_integrations = new ISourceControlIntegration[]{
				GitHubIntegration.Instance,
				BitBucketServerIntegration.Instance,
				BitBucketCloudIntegration.Instance
			};

			m_warnings = Array.Empty<string>();
			if( Version.TryParse( ConfigManager.Config.Version, out Version configVersion ) && configVersion >= new Version( 1, 3, 1 )  ) {
				m_pullRequests = m_cacheFile.TryLoadAsync().WaitSync() ?? new List<PullRequest>();
			} else {
				m_pullRequests = new List<PullRequest>();
			}

			m_integrations.ForEachInParallelAsync(
				integration => integration.SignIns.ForEachInParallelAsync(
					signIn => signIn.Verify()
				)
			).WaitSync();

			m_updateTimer = new Timer{
				AutoReset = true,
				Enabled = true,
				Interval = ConfigManager.Config.RefreshIntervalMinutes * 60_000.0
			};

			m_notifyTimer = new Timer {
				AutoReset = true,
				Enabled = true,
				Interval = ConfigManager.Config.NotificationIntervalMinutes * 60_000.0
			};

			m_updateTimer.Elapsed += AutoUpdateAsync;
			m_notifyTimer.Elapsed += AutoNotifyAsync;

			ProfileManager.OnProfileChangeBegin += OnProfileWillSwitch;
			ProfileManager.OnProfileChangeFinish += OnProfileSwitched;

			this.NotifyOnStartup();
		}

		public static CodeReviewManagerCore Instance = new CodeReviewManagerCore();

		public IReadOnlyList<IPullRequest> GetPullRequests() {
			List<IPullRequest> pullRequests;
			lock( m_lock ) {
				pullRequests = new List<IPullRequest>( m_pullRequests );
			}
			pullRequests.Sort( new PullRequestComparer( ConfigManager.Config ) );
			return pullRequests;
		}

		public IReadOnlyList<string> GetWarnings() {
			lock( m_lock ) {
				return new List<string>( m_warnings );
			}
		}

		public IReadOnlyList<ISourceControlIntegrationInfo> Integrations => m_integrations;

		public ConfigData GetSettings() {
			return ConfigManager.Config.Clone();
		}

		public Task UpdateSettingsAsync( ConfigData config ) {
			ConfigData oldConfig = GetSettings();
			ConfigManager.Config = config;
			NotifySettingsChanged( oldConfig, config, false );
			return ConfigManager.SaveAsync();
		}

		private void NotifySettingsChanged(
			ConfigData oldConfig,
			ConfigData newConfig,
			bool profileChanged
		) {
			bool changedSortOrder = (
				oldConfig.OrderBy != newConfig.OrderBy ||
				oldConfig.MutedAtBottom != newConfig.MutedAtBottom
			);

			double updateInterval = newConfig.RefreshIntervalMinutes * 60_000.0;
			double notifyInterval = newConfig.NotificationIntervalMinutes * 60_000.0;

			if( m_updateTimer.Interval != updateInterval ) {
				m_updateTimer.Interval = updateInterval;
			}

			if( m_notifyTimer.Interval != notifyInterval ) {
				m_notifyTimer.Interval = notifyInterval;
			}

			if( newConfig.RefreshOnOpen && !oldConfig.RefreshOnOpen ) {
				AutoUpdateAsync( null, null );
			}

			if( changedSortOrder && m_pullRequests.Count > 0 ) {
				OnOrderChanged?.Invoke();
			}

			OnConfigUpdated?.Invoke( new ConfigUpdatedEvent( oldConfig, newConfig, profileChanged ) );
		}

		public event Action OnBeginUpdate;
		public event Action<PullRequestsUpdatedEvent> OnUpdate;
		public event Action<UpdateFailedException> OnUpdateFailure;

		public event Action<IReadOnlyList<IPullRequest>> OnReminderAlert;

		public event Action OnOrderChanged;
		public event Action<ConfigUpdatedEvent> OnConfigUpdated;

		public event Action OnPullRequestMuted;
		public event Action OnPullRequestUnmuted;

		public event Action<IProfile> OnProfileChanged;

		public async Task UpdateAsync( bool force = false ) {

			if( !force ) {
				lock( m_lock ) {
					if( DateTime.UtcNow - m_lastUpdate < TimeSpan.FromSeconds( ConfigManager.Config.CacheDurationSeconds ) ) {
						return;
					}
				}
			}

			string profileId = ProfileManager.CurrentProfileId;

			OnBeginUpdate?.Invoke();

			PullRequestFetchResults updateResults = (await m_integrations.MapInParallelAsync(
				integration => integration.GetPullRequestsAsync()
			).SafeAsync()).Merge();

			// Cancel update if the profile has switched
			if( !string.Equals( profileId, ProfileManager.CurrentProfileId ) ) {
				return;
			}

			if( !ConfigManager.Config.IncludeSelfAssigned ) {
				updateResults.PullRequests = updateResults.PullRequests.Where( pr => !pr.SelfAssigned ).ToArray();
			}

			if( updateResults.Errors.Length > 0 ) {
				OnUpdateFailure?.Invoke( new UpdateFailedException( updateResults.Errors ) );
			}

			bool prMuted = false;
			bool prUnmuted = false;

			IDictionary<string, PullRequestInfo> infoMap = updateResults.PullRequests.ToDictionary( pr => pr.Key );
			HashSet<Guid> failedSignIns = new HashSet<Guid>(
				updateResults.Errors.Select( e => e.Id )
			);

			List<PullRequest> removedPullRequests, updatedPullRequests, newPullRequests, erroredPullRequests;
			lock( m_lock ) {
				updatedPullRequests = new List<PullRequest>( m_pullRequests.Count );
				removedPullRequests = new List<PullRequest>( m_pullRequests.Count );
				erroredPullRequests = new List<PullRequest>( m_pullRequests.Count );
				foreach( PullRequest pullRequest in m_pullRequests ) {
					if( failedSignIns.Contains( pullRequest.SignInId ) ) {
						pullRequest.Errored = true;
						erroredPullRequests.Add( pullRequest );
						continue;
					}

					pullRequest.Errored = false;
					string key = pullRequest.Key;
					if( infoMap.ContainsKey( key ) ) {
						PullRequest updatedPullRequest = pullRequest.CloneAndUpdate( infoMap[key] );
						updatedPullRequests.Add( updatedPullRequest );
						infoMap.Remove( key );

						prMuted |= updatedPullRequest.Muted && !pullRequest.Muted;
						prUnmuted |= !updatedPullRequest.Muted && pullRequest.Muted;
					} else {
						removedPullRequests.Add( pullRequest );
						if( !pullRequest.Muted ) {
							prMuted = true;
						}
					}
				}

				newPullRequests = infoMap.Values.Select( info => new PullRequest( info ) ).ToList();
				m_pullRequests = updatedPullRequests.Concat( newPullRequests ).Concat( erroredPullRequests ).ToList();
				m_warnings = updateResults.Warnings;

				if( newPullRequests.Count > 0 && !ConfigManager.Config.MuteNewPullRequests ) {
					prUnmuted = true;
				}

				m_lastUpdate = DateTime.UtcNow;
			}

			if( prMuted ) {
				OnPullRequestMuted?.Invoke();
			}

			if( prUnmuted ) {
				OnPullRequestUnmuted?.Invoke();
			}

			OnUpdate?.Invoke( new PullRequestsUpdatedEvent(
				removedPullRequests,
				updatedPullRequests,
				newPullRequests,
				erroredPullRequests,
				warnings: updateResults.Warnings,
				completedWithErrors: updateResults.Errors.Length > 0
			));
		}

		public Task SavePullRequestsAsync() {
			List<PullRequest> pullRequestsCopy;
			lock( m_lock ) {
				pullRequestsCopy = new List<PullRequest>( m_pullRequests );
			}
			return m_cacheFile.SaveAsync( pullRequestsCopy );
		}

		internal static void NotifyOrderChanged() {
			Instance.OnOrderChanged?.Invoke();
		}

		internal static void NotifyPullRequestMuted() {
			Instance.OnPullRequestMuted?.Invoke();
		}

		internal static void NotifyPullRequestUnmuted() {
			Instance.OnPullRequestUnmuted?.Invoke();
		}

		private async void AutoNotifyAsync( object sender, ElapsedEventArgs e ) {
			try {
				await this.UpdateAsync().SafeAsync();
			} catch( Exception exception ) {
				Log.WriteException( exception, "Automatic update failed" );
			}

			List<PullRequest> requestNotifications;
			lock( m_lock ) {
				requestNotifications = m_pullRequests.Where( pr => !pr.Muted ).ToList();
			}

			if( requestNotifications.Count > 0 ) {
				OnReminderAlert?.Invoke( requestNotifications );
			}
		}

		private async void AutoUpdateAsync( object sender, ElapsedEventArgs e ) {
			try {
				await UpdateAsync().SafeAsync();
			} catch( Exception exception ) {
				Log.WriteException( exception, "Automatic update failed" );
			}
		}

		private async void NotifyOnStartup() {
			if( !ConfigManager.Config.NotifyOnStartup ) {
				return;
			}

			await Task.Delay( TimeSpan.FromSeconds( 5.0 ) ).SafeAsync();
			AutoNotifyAsync( this, null );
		}

		private void OnProfileWillSwitch( ProfileChangeBeginEvent @event ) {
			SavePullRequestsAsync().WaitSync();
		}

		private void OnProfileSwitched( ProfileChangeFinishedEvent @event ) {
			OnConfigUpdated?.Invoke( new ConfigUpdatedEvent( @event.OldConfig, ConfigManager.Config, true ) );
			lock( m_lock ) {
				m_cacheFile = new JsonConfigFile<List<PullRequest>>(
					ProfileManager.GetProfilePath( @event.NewProfile ),
					"cache.json"
				);
				m_pullRequests = m_cacheFile.TryLoadAsync().WaitSync() ?? new List<PullRequest>();
				m_warnings = Array.Empty<string>();
			}

			UpdateAsync( force: true ).SafeDetach();
			OnProfileChanged?.Invoke( @event.NewProfile );
		}

		~CodeReviewManagerCore() {
			m_updateTimer?.Dispose();
			m_notifyTimer?.Dispose();
		}

	}

}
