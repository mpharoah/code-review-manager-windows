﻿using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CodeReviewManager.Backend.Config {

	[JsonObject( MemberSerialization.OptIn )]
	public sealed partial class ConfigData {

		#region Refresh Options
		[JsonProperty] public uint RefreshIntervalMinutes { get; set; }
		[JsonProperty] public bool RefreshOnOpen { get; set; }
		[JsonProperty] public uint CacheDurationSeconds { get; set; }

		/* Added in 1.4.0 */
		[JsonProperty( DefaultValueHandling = DefaultValueHandling.Populate)]
		[DefaultValue( false )]
		public bool IncludeSelfAssigned { get; set; }
		#endregion

		#region Notification Preferences
		[JsonProperty] public bool NotifyOnNewPullRequest { get; set; }
		[JsonProperty] public bool NotifyOnStartup { get; set; }
		[JsonProperty] public uint NotificationIntervalMinutes { get; set; }
		[JsonProperty] public bool MuteNewPullRequests { get; set; }
		[JsonProperty] public bool MuteAfterCommenting { get; set; }
		[JsonProperty] public UnmuteBehaviour UnmuteOnPush { get; set; }
		[JsonProperty] public UnmuteBehaviour UnmuteOnAuthorComment { get; set; }
		[JsonProperty] public UnmuteBehaviour UnmuteOnMention { get; set; }

		/* Added in 1.1.0 */
		[JsonProperty( DefaultValueHandling = DefaultValueHandling.Populate )]
		[DefaultValue( 3 )]
		public uint SnoozeTimeHours { get; set; }
		#endregion

		#region User Interface
		[JsonProperty] public SortOrder OrderBy { get; set; }
		[JsonProperty] public bool NewestAtTop { get; set; }
		[JsonProperty] public bool MutedAtBottom { get; set; }

		/* Added in 1.0.0 */
		[JsonProperty( DefaultValueHandling = DefaultValueHandling.Populate )]
		[DefaultValue( true )]
		public bool DockedToTray { get; set; }

		/* Added in 2.0.0-alpha */
		[JsonProperty( DefaultValueHandling = DefaultValueHandling.Populate )]
		[DefaultValue( true )]
		public bool ShowAvatars { get; set; }
		#endregion

		#region Hidden
		[JsonProperty] public uint WidthPixels { get; set; }
		[JsonProperty] public uint HeightPixels { get; set; }

		/* Added in 1.0.0 */
		[JsonProperty( DefaultValueHandling = DefaultValueHandling.Populate )]
		[DefaultValue( "" )]
		public string Version { get; set; }
		#endregion

		#region enums
		[JsonConverter( typeof( StringEnumConverter ) )]
		public enum UnmuteBehaviour : byte {
			AlwaysUnmute = 0,
			UnmuteIfAutoMuted = 1,
			NeverUnmute = 2
		}

		[JsonConverter( typeof( StringEnumConverter ) )]
		public enum SortOrder : byte {
			OpenDate = 0,
			LastUpdated = 1,
			LastAuthorComment = 2,
			LastReviewerComment = 3,
			LastPush = 4
		}
		#endregion

		internal ConfigData Clone() {
			return (ConfigData)MemberwiseClone();
		}

	}

}
