﻿using System.Threading.Tasks;
using CodeReviewManager.Backend.Profiles;
using CodeReviewManager.Util;

namespace CodeReviewManager.Backend.Config {

	internal static class ConfigManager {

		private static volatile ConfigData m_config;
		private static JsonConfigFile<ConfigData> m_file = new JsonConfigFile<ConfigData>(
			ProfileManager.CurrentProfilePath,
			"config.json"
		);
		private static object m_lock = new object();

		static ConfigManager() {
			m_config = m_file.TryLoadAsync().WaitSync() ?? ConfigData.DefaultConfig;

			ProfileManager.OnProfileChangeBegin += @event => {
				SaveAsync().WaitSync();
				lock( m_lock ) {
					m_file = new JsonConfigFile<ConfigData>(
						ProfileManager.GetProfilePath( @event.NewProfile ),
						"config.json"
					);
					m_config = m_file.TryLoadAsync().WaitSync() ?? ConfigData.DefaultConfig;
				}
			};
		}

		public static Task SaveAsync() {
			JsonConfigFile<ConfigData> file;
			ConfigData data;
			lock( m_lock ) {
				file = m_file;
				data = m_config;
			}
			return file.SaveAsync( data.Clone() );
		}

		public static ConfigData Config {
			get => m_config;
			set => m_config = value.Clone();
		}

		internal static Task CopyToProfileAsync( IProfile targetProfile ) {
			if( targetProfile.IsActive ) {
				return Task.CompletedTask;
			}

			return new JsonConfigFile<ConfigData>(
				ProfileManager.GetProfilePath( targetProfile ),
				"config.json"
			).SaveAsync( m_config );
		}

	}

}
