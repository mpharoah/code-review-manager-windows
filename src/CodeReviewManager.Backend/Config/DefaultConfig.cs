﻿using CodeReviewManager.Util;

namespace CodeReviewManager.Backend.Config {

	public sealed partial class ConfigData {

		internal static readonly ConfigData DefaultConfig = new ConfigData {
			RefreshIntervalMinutes = 5,
			RefreshOnOpen = true,
			CacheDurationSeconds = 10,
			IncludeSelfAssigned = false,

			NotifyOnNewPullRequest = true,
			NotifyOnStartup = true,
			NotificationIntervalMinutes = 30,
			MuteNewPullRequests = false,
			MuteAfterCommenting = true,
			UnmuteOnPush = UnmuteBehaviour.UnmuteIfAutoMuted,
			UnmuteOnAuthorComment = UnmuteBehaviour.NeverUnmute,
			UnmuteOnMention = UnmuteBehaviour.AlwaysUnmute,
			SnoozeTimeHours = 3,

			OrderBy = SortOrder.LastUpdated,
			NewestAtTop = false,
			MutedAtBottom = false,
			WidthPixels = 420,
			HeightPixels = 540,
			DockedToTray = true,

			Version = AppInfo.Version

		};

	}

}
