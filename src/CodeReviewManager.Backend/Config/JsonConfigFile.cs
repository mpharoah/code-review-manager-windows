﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json;

namespace CodeReviewManager.Backend.Config {

	internal class JsonConfigFile<TObject> where TObject : class {

		private readonly string m_path;
		private readonly AsyncLock m_lock = new AsyncLock();

		public JsonConfigFile( string relativePath ) {
			string home = Environment.GetFolderPath( Environment.SpecialFolder.UserProfile );
			m_path = Path.Combine( home, ".config\\code-review-manager", relativePath );
		}

		public JsonConfigFile( string profilePath, string relativePath ) : this(
			Path.Combine( profilePath, relativePath )
		) { }

		public async Task SaveAsync( TObject configData ) {
			using( await m_lock.AcquireAsync().SafeAsync() ) {
				Directory.CreateDirectory( Path.GetDirectoryName( m_path ) );
				using( FileStream file = new FileStream( m_path, FileMode.Create ) ) {
					string json = JsonConvert.SerializeObject( configData, Formatting.Indented );
					byte[] data = Encoding.UTF8.GetBytes( json );
					await file.WriteAsync( data, 0, data.Length ).SafeAsync();
				}
			}
		}

		public virtual Task<TObject> LoadAsync() {
			return LoadAsTypeAsync<TObject>();
		}

		protected async Task<TType> LoadAsTypeAsync<TType>() where TType : class {
			using( await m_lock.AcquireAsync().SafeAsync() )
			using( FileStream file = new FileStream( m_path, FileMode.Open ) ) {
				List<byte> data = new List<byte>( 1024 );

				byte[] buffer = new byte[1024];
				while( true ) {
					int bytesRead = await file.ReadAsync( buffer, 0, 1024 ).SafeAsync();
					if( bytesRead == 1024 ) {
						data.AddRange( buffer );
						continue;
					}

					if( bytesRead > 0 ) {
						byte[] smallBuffer = new byte[bytesRead];
						Array.Copy( buffer, smallBuffer, bytesRead );
						data.AddRange( smallBuffer );
					}
					break;
				}

				string json = Encoding.UTF8.GetString( data.ToArray() );
				return JsonConvert.DeserializeObject<TType>( json );
			}
		}

		public async Task<TObject> TryLoadAsync() {
			try {
				return await LoadAsync().SafeAsync();
			} catch( FileNotFoundException ) {
				return null;
			} catch( DirectoryNotFoundException ) {
				return null;
			} catch( Exception exception ) {
				Log.WriteException( exception, $"Error reading config file {m_path}" );
				return null;
			}
		}

		public async Task DeleteAsync() {
			using( await m_lock.AcquireAsync().SafeAsync() ) {
				try {
					File.Delete( m_path );
				} catch( DirectoryNotFoundException ) {}
			}
		}

	}

	internal sealed class JsonConfigFile<TDerived, TBase> : JsonConfigFile<TBase>
		where TDerived : class, TBase
		where TBase : class {

		public JsonConfigFile( string relativePath ) : base( relativePath ) { }

		public JsonConfigFile( string profilePath, string relativePath ) : this(
			Path.Combine( profilePath, relativePath )
		) { }

		public override async Task<TBase> LoadAsync() {
			return await LoadAsTypeAsync<TDerived>().SafeAsync();
		}

	}

}
