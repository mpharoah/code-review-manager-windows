﻿using CodeReviewManager.Backend.Config;

namespace CodeReviewManager.Backend {

	public sealed class ConfigUpdatedEvent {

		internal ConfigUpdatedEvent(
			ConfigData oldConfig,
			ConfigData newConfig,
			bool profileChanged = false
		) {
			OldConfig = oldConfig;
			NewConfig = newConfig;
			ProfileChanged = profileChanged;
		}

		public ConfigData OldConfig { get; }
		public ConfigData NewConfig { get; }
		public bool ProfileChanged { get; }

	}

}
