﻿using System;
using System.Runtime.CompilerServices;
using CodeReviewManager.Backend.Integrations;

namespace CodeReviewManager.Backend.Exceptions {

	public class SourceControlIntegrationException : ApplicationException {

		internal SourceControlIntegrationException(
			ISourceControlSignInInfo signIn,
			ISourceControlIntegrationInfo integration,
			Exception innerException
		) : base( $"Failed to fetch pull requests from {integration.Name}.", innerException ) {
			Integration = integration;
			SignIn = signIn;
		}

		public ISourceControlIntegrationInfo Integration { get; }
		public ISourceControlSignInInfo SignIn { get; }

		internal Guid Id {
			[MethodImpl( MethodImplOptions.AggressiveInlining )]
			get => SignIn.Id;
		}


	}

}
