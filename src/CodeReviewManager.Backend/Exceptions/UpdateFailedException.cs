﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeReviewManager.Backend.Exceptions {

	public sealed class UpdateFailedException : AggregateException {

		internal UpdateFailedException(
			IEnumerable<SourceControlIntegrationException> exceptions
		) : base( exceptions ) {}

		public new IEnumerable<SourceControlIntegrationException> InnerExceptions {
			get => base.InnerExceptions.Cast<SourceControlIntegrationException>();
		}

		public int Count => base.InnerExceptions.Count;

	}

}
