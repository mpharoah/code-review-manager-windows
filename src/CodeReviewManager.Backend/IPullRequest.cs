﻿using System;

namespace CodeReviewManager.Backend {

	public interface IPullRequest {

		void Mute( bool automatic = false );
		void Unmute();
		void Snooze();

		Uri Url { get; }
		Guid SignInId { get; }
		string Repository { get; }
		string AuthorName { get; }
		string Name { get; }
		DateTime Opened { get; }
		DateTime LastUpdated { get; }
		DateTime LastPushed { get; }
		DateTime LastAuthorComment { get; }
		DateTime LastReviewerComment { get; }
		Uri AuthorAvatarUrl { get; }

		IntegrationType Source { get; }
		NotificationStatus NotificationStatus { get; }
		bool Muted { get; }
		bool Snoozed { get; }

		bool Errored { get; }

	}

}
