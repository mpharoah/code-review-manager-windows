﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CodeReviewManager.Backend {

	[JsonConverter( typeof( StringEnumConverter ) )]
	public enum IntegrationType {
		GitHub,
		BitBucketServer,
		BitBucketCloud
	}

}
