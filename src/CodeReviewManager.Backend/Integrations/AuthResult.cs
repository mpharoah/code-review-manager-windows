﻿using System;
using System.Net;

namespace CodeReviewManager.Backend.Integrations {
	public sealed class AuthResult {

		internal AuthResult(
			ICredentials credentials,
			HttpStatusCode statusCode
		) {
			Type = AuthResultType.Success;
			StatusCode = statusCode;
			Credentials = credentials;
			Exception = null;
			Message = null;
		}

		internal AuthResult(
			AuthResultType type,
			HttpStatusCode statusCode,
			string message = null
		) {
			Type = type;
			StatusCode = statusCode;
			Credentials = null;
			Exception = null;
			Message = message;
		}

		internal AuthResult(
			Exception exception
		) {
			Type = AuthResultType.Error;
			StatusCode = 0;
			Credentials = null;
			Exception = exception;
			Message = null;
		}

		public AuthResultType Type { get; }
		public string Message { get; }
		public HttpStatusCode StatusCode { get; }
		public Exception Exception { get; }

		internal ICredentials Credentials { get; }
	}

}
