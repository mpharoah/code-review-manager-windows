﻿namespace CodeReviewManager.Backend.Integrations {

	public enum AuthResultType {
		Success,
		Requires2FA,
		Denied,
		Error,
		Cancelled
	}

}
