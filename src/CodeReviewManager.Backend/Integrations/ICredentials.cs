﻿using System;
using System.Net.Http;

namespace CodeReviewManager.Backend.Integrations {

	internal interface ICredentials {

		Guid IntegrationId { get; }

		string CachedUsername { get; set; }

		HttpClient CreateHttpClient();

	}

}
