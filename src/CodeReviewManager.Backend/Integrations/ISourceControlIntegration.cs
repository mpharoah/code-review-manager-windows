﻿using System.Threading.Tasks;

namespace CodeReviewManager.Backend.Integrations {

	internal interface ISourceControlIntegration : ISourceControlIntegrationInfo {

		Task<PullRequestFetchResults> GetPullRequestsAsync();
		Task SaveAsync();

	}

}
