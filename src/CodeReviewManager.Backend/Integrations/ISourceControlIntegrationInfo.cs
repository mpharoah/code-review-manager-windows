﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CodeReviewManager.Backend.Integrations.UISpec;
using FormData = System.Collections.Generic.IDictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations {

	public interface ISourceControlIntegrationInfo {

		IntegrationType Type { get; }
		string Name { get; }

		IEnumerable<ISourceControlSignInInfo> SignIns { get; }

		Task<AuthResult> TryCreateSignInAsync(
			FormData formData,
			string mfaCode = null,
			CancellationToken cancellationToken = default( CancellationToken )
		);

		Task RemoveSignIn( ISourceControlSignInInfo signIn );

		Task<ModeViewSpec[]> GetUiSpecAsync();

	}

}
