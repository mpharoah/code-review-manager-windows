﻿using System.Threading.Tasks;

namespace CodeReviewManager.Backend.Integrations {

	internal interface ISourceControlSignIn : ISourceControlSignInInfo {

		Task<PullRequestFetchResults> GetPullRequestsAsync();
		ICredentials Credentials { get; }

	}

}
