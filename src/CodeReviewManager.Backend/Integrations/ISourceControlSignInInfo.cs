﻿using System;
using System.Threading.Tasks;

namespace CodeReviewManager.Backend.Integrations {

	public interface ISourceControlSignInInfo {

		Guid Id { get; }
		string Username { get; }
		string LocationInfo { get; }
		bool Errored { get; }

		Task<bool> Verify();

	}

}
