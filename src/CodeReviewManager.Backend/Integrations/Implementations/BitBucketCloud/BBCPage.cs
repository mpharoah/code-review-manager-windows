﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CodeReviewManager.Util;
using Newtonsoft.Json.Linq;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud {

	public sealed class BBCPage : OnDemandPagedIterator<JObject> {

		private readonly ICredentials m_credentials;
		private Uri m_nextUrl;

		internal BBCPage(
			ICredentials credentials,
			Uri apiUrl
		) {
			m_credentials = credentials;
			m_nextUrl = apiUrl;
		}

		protected override async Task<IEnumerable<JObject>> TryFetchNextPageAsync() {
			if( m_nextUrl == null ) {
				return null;
			}

			using( HttpClient httpClient = m_credentials.CreateHttpClient() ) {
				SimpleResponse response = await httpClient.SimpleSendAsync( HttpMethod.Get, m_nextUrl, true ).SafeAsync();
				JObject page = JObject.Parse( response.ResponseBody );
				m_nextUrl = page.TryGetValue( "next", out JToken nextPage ) ? new Uri( nextPage.Value<string>() ) : null;
				return ((JArray)page["values"]).Cast<JObject>();
			}
		}

	}

}
