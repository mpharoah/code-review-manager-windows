﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CodeReviewManager.Util;
using FormData = System.Collections.Generic.IDictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud {

	internal sealed class BitBucketCloudAuthorizer : IIntegrationAuthorizer {

		internal static BitBucketCloudAuthorizer Instance = new BitBucketCloudAuthorizer();

		async Task<AuthResult> IIntegrationAuthorizer.TryLoginAsync(
			FormData formData,
			string mfaCode // not used for BitBucket
		) {
			string repository = formData["Repository"];
			string username = formData["Username"];
			string password = formData["Password"];

			if( string.IsNullOrWhiteSpace( repository ) || !repository.Contains( "/" ) ) {
				return new AuthResult(
					AuthResultType.Denied,
					HttpStatusCode.BadRequest,
					"Invalid repository name format. You must include the repository owner in the name."
				);
			}

			try {
				ICredentials credentials = new BitBucketCloudCredentials( username, password, repository );
				using( HttpClient httpClient = credentials.CreateHttpClient() ) {
					SimpleResponse response = await httpClient.SimpleSendAsync(
						HttpMethod.Get,
						new Uri( $"https://api.bitbucket.org/2.0/repositories/{repository}" ),
						ensureSuccess: false
					).SafeAsync();

					switch( response.StatusCode ) {
						case HttpStatusCode.Unauthorized:
							return new AuthResult( AuthResultType.Denied, response.StatusCode );
						case HttpStatusCode.Forbidden:
							return new AuthResult(
								AuthResultType.Denied,
								response.StatusCode,
								"This account has 2 factor authentication enabled, so you cannot sign in using your account password. Create an app password instead."
							);
						case HttpStatusCode.NotFound:
							return new AuthResult(
								AuthResultType.Denied,
								response.StatusCode,
								"Repository not found."
							);
						case HttpStatusCode.OK:
							return new AuthResult( credentials, response.StatusCode );
						default:
							return new AuthResult( AuthResultType.Error, response.StatusCode );
					}
				}
			} catch( Exception exception ) {
				return new AuthResult( exception );
			}

		}

	}

}
