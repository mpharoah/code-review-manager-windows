﻿using System;
using System.Net.Http;
using CodeReviewManager.Util;
using Newtonsoft.Json;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud {

	[JsonObject( MemberSerialization.OptIn )]
	internal sealed class BitBucketCloudCredentials : ICredentials {

		private readonly Guid m_id;

		[JsonConstructor]
		private BitBucketCloudCredentials(
			[JsonProperty( PropertyName = "Username", Required = Required.Always )] string username,
			[JsonProperty( PropertyName = "AppPassword", Required = Required.Always )] string password,
			[JsonProperty( PropertyName = "Repository", Required = Required.Always )] string repository,
			[JsonProperty( PropertyName = "Id", Required = Required.Always )] Guid id
		) {
			this.Password = password;
			m_id = id;
			Repository = repository;
			( this as ICredentials ).CachedUsername = username;
		}

		internal BitBucketCloudCredentials( string username, string password, string repository ) :
			this( username, password, repository, Guid.NewGuid() ) {}

		HttpClient ICredentials.CreateHttpClient() {
			ICredentials @this = this;
			HttpClient client = new HttpClient();
			client.DefaultRequestHeaders.Authorization = new BasicAuth( @this.CachedUsername, Password  );
			return client;
		}

		[JsonProperty( PropertyName = "Repository" )]
		internal string Repository { get; }

		[JsonProperty( PropertyName = "Username" )]
		string ICredentials.CachedUsername { get; set; }

		[JsonProperty( PropertyName = "AppPassword" )]
		internal string Password { get; set; }

		[JsonProperty( PropertyName = "Id" )]
		Guid ICredentials.IntegrationId => m_id;

	}

}
