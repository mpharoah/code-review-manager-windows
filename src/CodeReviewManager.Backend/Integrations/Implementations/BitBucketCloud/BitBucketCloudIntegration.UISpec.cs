﻿using CodeReviewManager.Backend.Integrations.UISpec;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud {

	partial class BitBucketCloudIntegration {

		private static readonly ModeViewSpec[] s_uiSpec = {
			new ModeViewSpec(
				new[]{
					new MessageFragment( "It is recommended that you create an app password and sign in using this password instead of your normal account password. App passwords require read permissions on repositories and pull requests." )
				},
				new[]{
					new Field( "Repository", "Full Repository Name (eg. repoOwner/repoName)" ),
					new Field( "Username", "Username" ),
					new Field( "Password", "Password", isPassword: true )
				}
			)
		};

	}

}
