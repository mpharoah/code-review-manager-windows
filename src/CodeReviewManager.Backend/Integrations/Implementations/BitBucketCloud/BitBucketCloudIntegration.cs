﻿using CodeReviewManager.Backend.Config;
using CodeReviewManager.Backend.Integrations.UISpec;
using System.Threading.Tasks;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud {

	internal sealed partial class BitBucketCloudIntegration : IntegrationBase {

		private BitBucketCloudIntegration() : base() { }
		internal static BitBucketCloudIntegration Instance = new BitBucketCloudIntegration();

		public override IntegrationType Type => IntegrationType.BitBucketCloud;
		public override string Name => "BitBucket Cloud";
		protected override IIntegrationAuthorizer Authorizer => BitBucketCloudAuthorizer.Instance;
		public override Task<ModeViewSpec[]> GetUiSpecAsync() => Task.FromResult( s_uiSpec );

		protected override ISourceControlSignIn CreateSignIn( ICredentials credentials ) => new BitBucketCloudSignIn( credentials );

		protected override JsonConfigFile<ICredentials[]> GetCredentialsFile( string profilePath ) {
			return new JsonConfigFile<BitBucketCloudCredentials[], ICredentials[]>(
				profilePath,
				"integrations\\BitBucketCloud.json"
			);
		}

	}

}
