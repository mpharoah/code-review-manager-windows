﻿using System;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud {

	partial class BitBucketCloudSignIn {

		private IAsyncIterator<ActivityInfo> FetchActivities( Uri activitiesUrl ) {
			// Results are returned in order of most recent to least recent
			string me = Credentials.CachedUsername;
			return new BBCPage( Credentials, activitiesUrl ).SelectWhere( activityData => {
				Log.WriteDebug( Log.BitBucketCloud, () => activityData.ToString( Formatting.Indented ) );

				if( activityData.TryGetValue( "comment", out JToken comment ) ) {
					return new ActivityInfo {
						Type = ActivityType.Comment,
						User = ( string )comment["user"]["username"],
						Timestamp = ((DateTime)comment["created_on"]).ToUniversalTime(),
						MentionsMe = ( (string)comment["content"]["raw"] ).Contains( $"@{me}" )
					};
				} else if( activityData.TryGetValue( "approval", out JToken approval ) ) {
					return new ActivityInfo {
						Type = ActivityType.Approval,
						User = (string)approval["user"]["username"],
						Timestamp = ((DateTime)approval["date"]).ToUniversalTime()
					};
				} else if( activityData.TryGetValue( "update", out JToken update ) ) {
					return new ActivityInfo {
						Type = ActivityType.Update,
						User = (string)update["author"]["username"],
						Timestamp = ((DateTime)update["date"]).ToUniversalTime(),
						Commit = (string)update["source"]["commit"]["hash"]
					};
				}
				return Optional<ActivityInfo>.Undefined;
			});
		}

		private enum ActivityType {
			Comment,
			Update,
			Approval
		}

		private struct ActivityInfo {
			public ActivityType Type;
			public string User;
			public DateTime Timestamp;

			public bool MentionsMe; // for commments
			public string Commit; // for updates
		}

	}

}
