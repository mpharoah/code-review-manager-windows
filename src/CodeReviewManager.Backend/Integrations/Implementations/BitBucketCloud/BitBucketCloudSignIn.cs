﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading.Tasks;
using CodeReviewManager.Backend.Exceptions;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FormData = System.Collections.Generic.Dictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketCloud {

	internal sealed partial class BitBucketCloudSignIn : ISourceControlSignIn {

		private DateTime m_lastActivity = DateTime.MinValue.ToUniversalTime();

		internal BitBucketCloudSignIn( ICredentials credentials ) {
			Credentials = credentials;
			Errored = false;
		}

		public ICredentials Credentials { get; }

		public Guid Id => Credentials.IntegrationId;
		public string Username => Credentials.CachedUsername;
		public string LocationInfo => Repository;

		public bool Errored { get; private set; }

		async Task<bool> ISourceControlSignInInfo.Verify() {
			IIntegrationAuthorizer authorizer = BitBucketCloudAuthorizer.Instance;
			AuthResult authResult = await authorizer.TryLoginAsync(
				new FormData{
					{ "Repository", Repository },
					{ "Username", Credentials.CachedUsername },
					{ "Password", ( Credentials as BitBucketCloudCredentials ).Password }
				}
			).SafeAsync();

			Errored = ( authResult.Type != AuthResultType.Success );
			return !Errored;
		}

		async Task<PullRequestFetchResults> ISourceControlSignIn.GetPullRequestsAsync() {
			try {
				string searchQuery = WebUtility.UrlEncode( $"state = \"OPEN\" AND reviewers.username = \"{Credentials.CachedUsername}\"" );
				Uri requestUrl = new Uri( $"https://api.bitbucket.org/2.0/repositories/{Repository}/pullrequests?q={searchQuery}" );

				IAsyncIterator<JObject> results = new BBCPage( Credentials, requestUrl );
				ConcurrentBag<PullRequestInfo> pullRequests = new ConcurrentBag<PullRequestInfo>();
				await results.ForEachInParallelAsync( async pullRequest => {
					Log.WriteDebug( Log.BitBucketCloud, () => pullRequest.ToString( Formatting.Indented ) );

					Uri url = new Uri( (string)pullRequest["links"]["self"]["href"] );
					string name = (string)pullRequest["title"];
					string repoName = (string)pullRequest["destination"]["repository"]["name"];
					DateTime opened = ((DateTime)pullRequest["created_on"]).ToUniversalTime();
					DateTime updated = ((DateTime)pullRequest["updated_on"]).ToUniversalTime();
					string authorUsername = (string)pullRequest["author"]["username"];
					string authorFullName = (string)pullRequest["author"]["display_name"] ?? authorUsername;
					string authorAvatar = (string)pullRequest["author"]["links"]["avatar"]["href"];

					if( authorAvatar.EndsWith( "/32/" ) ) {
						// User a higher resolution version of the avatar
						authorAvatar = string.Concat( authorAvatar.Substring( 0, authorAvatar.Length - 4 ), "/64/" );
					}

					Uri activitiesUrl = new Uri( (string)pullRequest["links"]["activity"]["href"] );
					IAsyncIterator<ActivityInfo> activities = FetchActivities( activitiesUrl );

					DateTime? lastPush  = null;
					DateTime? lastAuthorComment = null;
					DateTime? lastReviewerComment = null;
					DateTime? lastReviewerMention = null;

					DateTime maybeLastPush = DateTime.MaxValue.ToUniversalTime();
					string lastCommitHash = null;

					DateTime? newLastActivity = null;

					// Activities are in order of most recent to least recent
					string me = Credentials.CachedUsername;
					while( await activities.MoveNextAsync().SafeAsync() ) {
						ActivityInfo activity = activities.Current;

						if( activity.Timestamp < m_lastActivity ) {
							break;
						}

						if( !newLastActivity.HasValue ) {
							newLastActivity = activity.Timestamp;
						}

						switch( activity.Type ) {
							case ActivityType.Approval: {
								if( me.Equals( activity.User ) ) {
									return;
								}
								break;
							}
							case ActivityType.Update: {
								if( lastPush.HasValue ) {
									break;
								} else if( lastCommitHash == null ) {
									maybeLastPush = activity.Timestamp;
									lastCommitHash = activity.Commit;
								} else if( lastCommitHash.Equals( activity.Commit ) ) {
									maybeLastPush = activity.Timestamp;
								} else {
									lastPush = maybeLastPush;
								}
								break;
							}
							case ActivityType.Comment: {
								if( !lastReviewerMention.HasValue && activity.MentionsMe ) {
									lastReviewerMention = activity.Timestamp;
								}
								if( !lastAuthorComment.HasValue && authorUsername.Equals( activity.User ) ) {
									lastAuthorComment = activity.Timestamp;
								}
								if( !lastReviewerComment.HasValue && me.Equals( activity.User ) ) {
									lastReviewerComment = activity.Timestamp;
								}
								break;
							}
						}

					}

					m_lastActivity = newLastActivity ?? m_lastActivity;
					PullRequestInfo pullRequestInfo = new PullRequestInfo(
						source: IntegrationType.BitBucketCloud,
						integrationId: Id,
						url: url,
						repositoryName: repoName,
						authorName: authorFullName,
						pullRequestName: name,
						openedTimestamp: opened,
						updatedTimestamp: updated,
						lastPushTimestamp: lastPush ?? opened,
						lastAuthorCommentTimestamp: lastAuthorComment,
						lastReviewerCommentTimestamp: lastReviewerComment,
						lastReviewerMentionTimestamp: lastReviewerMention,
						lastReviewTimestamp: null, // BitBucket Cloud doesn't support "Needs Work" reviews, and retroactively deletes approvals on unapprove
						selfAssigned: string.Equals( Credentials.CachedUsername, authorUsername ),
						avatarUrl: new Uri( authorAvatar )
					);

					Log.WriteDebug( Log.BitBucketCloud, () => JsonConvert.SerializeObject( pullRequestInfo, Formatting.Indented ) );
					pullRequests.Add( pullRequestInfo );
				}, maxConcurrency: 4 ).SafeAsync();

				//TODO: add debug logging
				Errored = false;
				return new PullRequestFetchResults {
					PullRequests = pullRequests.ToArray(),
					Errors = new SourceControlIntegrationException[0],
					Warnings = Array.Empty<string>()
				};
			} catch( Exception exception ) {
				Errored = true;
				Log.WriteException( exception, "[BitBucket Cloud] Error fetching pull requests." );
				return new PullRequestFetchResults {
					Errors = new[] { new SourceControlIntegrationException( this, BitBucketCloudIntegration.Instance, exception ), },
					PullRequests = new PullRequestInfo[0]
				};
			}
		}

		private string Repository => ( (BitBucketCloudCredentials)Credentials ).Repository;

	}

}
