﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using FormData = System.Collections.Generic.IDictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketServer {

	internal sealed class BitBucketServerAuthorizer : IIntegrationAuthorizer {

		internal static BitBucketServerAuthorizer Instance = new BitBucketServerAuthorizer();

		Task<AuthResult> IIntegrationAuthorizer.TryLoginAsync(
			FormData formData,
			string mfaCode // not used for BitBucket
		) {
			BitBucketServerCredentials credentials = formData.ContainsKey( "AccessToken" ) ?
				new BitBucketServerCredentials( formData["Domain"], formData["AccessToken"] ) :
				new BitBucketServerCredentials( formData["Domain"], formData["Username"], formData["Password"] );

			return MakeTestApiCall( credentials );
		}

		private async Task<AuthResult> MakeTestApiCall( ICredentials credentials ) {
			try {
				string domain = ((BitBucketServerCredentials)credentials).Domain;
				using( HttpClient httpClient = credentials.CreateHttpClient() ) {
					Uri requestUri = new Uri( $"{domain}/rest/api/1.0/inbox/pull-requests/count" );
					using( HttpResponseMessage response = await httpClient.GetAsync( requestUri ).SafeAsync() ) {
						if( !response.IsSuccessStatusCode ) {
							return new AuthResult(
								response.StatusCode == HttpStatusCode.Unauthorized ? AuthResultType.Denied : AuthResultType.Error,
								response.StatusCode
							);
						}

						credentials.CachedUsername = response.Headers.GetValues( "X-AUSERNAME" ).First();
						return new AuthResult( credentials, response.StatusCode );
					}
				}
			} catch( Exception exception ) {
				Log.WriteException( exception, "Error calling BitBucket API" );
				return new AuthResult( exception );
			}
		}

	}

}
