﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using CodeReviewManager.Util;
using Newtonsoft.Json;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketServer {

	[JsonObject( MemberSerialization.OptIn )]
	internal sealed class BitBucketServerCredentials : ICredentials {

		private readonly Guid m_id;

		[JsonConstructor]
		private BitBucketServerCredentials(
			[JsonProperty( PropertyName = "Domain", Required = Required.Always )] string domain,
			[JsonProperty( PropertyName = "Username", Required = Required.DisallowNull )] string username,
			[JsonProperty( PropertyName = "Password", Required = Required.DisallowNull )] string password,
			[JsonProperty( PropertyName = "AccessToken", Required = Required.DisallowNull )] string accessToken,
			[JsonProperty( PropertyName = "Id", Required = Required.Always )] Guid integrationId
		) {
			this.Password = password;
			this.AccessToken = accessToken;
			Domain = domain;
			m_id = integrationId;

			(this as ICredentials).CachedUsername = username;
		}

		internal BitBucketServerCredentials( string domain, string username, string password ) :
			this( domain, username, password, null, Guid.NewGuid() ) { }

		internal BitBucketServerCredentials( string domain, string accessToken ) :
			this( domain, null, null, accessToken, Guid.NewGuid() ) { }

		HttpClient ICredentials.CreateHttpClient() {
			HttpClient client = new HttpClient();
			client.DefaultRequestHeaders.Authorization = ( this.AccessToken != null ) ?
				new AuthenticationHeaderValue( "Bearer", this.AccessToken ) :
				client.DefaultRequestHeaders.Authorization = new BasicAuth(
					(this as ICredentials).CachedUsername,
					this.Password
				);
			return client;
		}

		[JsonProperty( PropertyName = "Username" )]
		string ICredentials.CachedUsername { get; set; }

		[JsonProperty( PropertyName = "Domain" )]
		internal string Domain { get; }

		[JsonProperty( PropertyName = "Password", NullValueHandling = NullValueHandling.Ignore )]
		internal string Password { get; set; }

		[JsonProperty( PropertyName = "AccessToken", NullValueHandling = NullValueHandling.Ignore )]
		internal string AccessToken { get; set; }

		[JsonProperty( PropertyName = "Id" )]
		Guid ICredentials.IntegrationId => m_id;

	}

}
