﻿using System;
using System.Text.RegularExpressions;
using CodeReviewManager.Backend.Integrations.UISpec;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketServer {

	partial class BitBucketServerIntegration {

		private static readonly ModeViewSpec[] s_uiSpec = {
			new ModeViewSpec(
				new[]{
					new MessageFragment( "It is recommended that you create a personal access token as this allows the app to make API calls without storing your password; however, you may also " ),
					new MessageFragment( "sign in using your username and password", linksTo: 1 ),
					new MessageFragment( "." )
				},
				new[]{
					new Field( "Domain", "Domain", valueTransform: ParseDomain ),
					new Field( "AccessToken", "Personal Access Token", isPassword: true )
				}
			),
			new ModeViewSpec(
				new[]{
					new MessageFragment( "BitBucket Server does not support the creation of personal access tokens via API calls. If you sign in below using your username and password, your password will be stored locally. It is recommended that you instead manually create a personal access token and " ),
					new MessageFragment( "sign in with it here", linksTo: 0 ),
					new MessageFragment( "." )
				},
				new[]{
					new Field( "Domain", "Domain" ),
					new Field( "Username", "Username" ),
					new Field( "Password", "Password", isPassword: true )
				}
			)
		};

		private static string ParseDomain( string domain ) {
			domain = domain?.Trim() ?? string.Empty;
			if( domain.StartsWith( "//" ) ) {
				domain = string.Concat( "https:", domain );
			} else if( !domain.StartsWith( "http://" ) && !domain.StartsWith( "https://" ) ) {
				domain = string.Concat( "https://", domain );
			}

			Regex domainRegex = new Regex( @"^(https?:\/\/[^\/#\?]+)", RegexOptions.CultureInvariant | RegexOptions.IgnoreCase );
			Match match = domainRegex.Match( domain );

			if( match.Success && match.Groups.Count == 2 ) {
				return match.Groups[1].Value;
			}

			return String.Empty;
		}

	}

}
