﻿using CodeReviewManager.Backend.Config;
using CodeReviewManager.Backend.Integrations.UISpec;
using System.Threading.Tasks;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketServer {

	internal sealed partial class BitBucketServerIntegration : IntegrationBase {

		private BitBucketServerIntegration() : base() {}
		internal static BitBucketServerIntegration Instance = new BitBucketServerIntegration();

		public override IntegrationType Type => IntegrationType.BitBucketServer;
		public override string Name => "BitBucket Server";
		protected override IIntegrationAuthorizer Authorizer => BitBucketServerAuthorizer.Instance;
		public override Task<ModeViewSpec[]> GetUiSpecAsync() => Task.FromResult( s_uiSpec );

		protected override ISourceControlSignIn CreateSignIn( ICredentials credentials ) => new BitBucketServerSignIn( credentials );

		protected override JsonConfigFile<ICredentials[]> GetCredentialsFile( string profilePath ) {
			return new JsonConfigFile<BitBucketServerCredentials[], ICredentials[]>(
				profilePath,
				"integrations\\BitBucketServer.json"
			);
		}

	}

}
