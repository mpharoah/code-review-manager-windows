﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CodeReviewManager.Util;
using Newtonsoft.Json.Linq;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketServer {

	partial class BitBucketServerSignIn {

		private class ActivityInfo {
			public DateTime Timestamp;
			public long UserId;

			protected ActivityInfo() { }
		}

		private sealed class CommentInfo : ActivityInfo {
			public bool MentionsMe;
		}

		private sealed class ReviewInfo : ActivityInfo {
			public bool Approved;
		}

		private sealed class PushInfo : ActivityInfo { }

		private async Task<IEnumerable<ActivityInfo>> GetRelevantActivitiesAsync(
			HttpClient httpClient,
			string pullRequestApiUrl
		) {
			Uri url = new Uri( $"{pullRequestApiUrl}/activities" );
			Regex mentionRegex = new Regex( @"(^|\s)" + Regex.Escape( "@" + Credentials.CachedUsername ) + @"($|\s)" );

			IList<ActivityInfo> activities = new List<ActivityInfo>();
			while( true ) {
				SimpleResponse response = await httpClient.SimpleSendAsync( HttpMethod.Get, url, true ).SafeAsync();
				JObject page = JObject.Parse( response.ResponseBody );

				foreach( JToken activity in (JArray)page["values"] ) {
					string activityType = (string)activity["action"];

					switch( activityType ) {
						case "APPROVED":
						case "REVIEWED":
							activities.Add( new ReviewInfo {
								Timestamp = FromUnixTime( (long)activity["createdDate"] ),
								UserId = (long)activity["user"]["id"],
								Approved = activityType.Equals( "APPROVED" )
							} );
							break;
						case "RESCOPED":
							activities.Add( new PushInfo {
								Timestamp = FromUnixTime( (long)activity["createdDate"] ),
								UserId = (long)activity["user"]["id"]
							} );
							break;
						case "COMMENTED":
							if( "ADDED".Equals( (string)activity["commentAction"] ) ) {
								AddCommentThread( activity["comment"], mentionRegex, activities );
							}
							break;
					}

				}

				if( (bool)page["isLastPage"] ) {
					break;
				}

				int startFrom = (int)page["nextPageStart"];
				url = new Uri( $"{pullRequestApiUrl}/activities?start={startFrom}" );
			}

			return activities;
		}

		private static void AddCommentThread(
			JToken rootComment,
			Regex mentionRegex,
			IList<ActivityInfo> activities
		) {
			activities.Add( new CommentInfo {
				Timestamp = FromUnixTime( (long)rootComment["createdDate"] ),
				UserId = (long)rootComment["author"]["id"],
				MentionsMe = mentionRegex.IsMatch( (string)rootComment["text"] )
			} );

			foreach( JToken childComment in (JArray)rootComment["comments"] ) {
				AddCommentThread( childComment, mentionRegex, activities );
			}
		}

	}
}
