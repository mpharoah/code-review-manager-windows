﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CodeReviewManager.Backend.Exceptions;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json.Linq;
using FormData = System.Collections.Generic.Dictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations.Implementations.BitBucketServer {

	internal sealed partial class BitBucketServerSignIn : ISourceControlSignIn {

		private readonly LazyAsync<long> m_userId;

		internal BitBucketServerSignIn( ICredentials credentials ) {
			Credentials = credentials;
			Errored = false;
			m_userId = new LazyAsync<long>( WhoAmI );
		}

		public ICredentials Credentials { get; }

		public Guid Id => Credentials.IntegrationId;
		public string Username => Credentials.CachedUsername;
		public string LocationInfo => Domain;

		public bool Errored { get; private set; }

		public async Task<bool> Verify() {
			string token = ( Credentials as BitBucketServerCredentials ).AccessToken;
			string password = ( Credentials as BitBucketServerCredentials ).Password;
			IIntegrationAuthorizer authorizer = BitBucketServerAuthorizer.Instance;

			AuthResult testResult;
			if( token != null ) {
				testResult = await authorizer.TryLoginAsync(
					new FormData{
						{ "Domain", Domain },
						{ "AccessToken", token }
					}
				).SafeAsync();
			} else {
				testResult = await authorizer.TryLoginAsync(
					new FormData{
						{ "Domain", Domain },
						{ "Username", Credentials.CachedUsername },
						{ "Password", password }
					}
				).SafeAsync();
			}

			if( testResult.Type == AuthResultType.Success ) {
				Errored = false;
				if( !string.Equals( Credentials.CachedUsername, testResult.Credentials.CachedUsername ) ) {
					Credentials.CachedUsername = testResult.Credentials.CachedUsername;
					await BitBucketServerIntegration.Instance.SaveAsync().SafeAsync();
				}
				return true;
			}

			Errored = true;
			return false;
		}

		public async Task<PullRequestFetchResults> GetPullRequestsAsync() {
			try {
				List<PullRequestInfo> infos = new List<PullRequestInfo>();

				long myUserId = await m_userId.GetValue().SafeAsync();

				int position = 0;
				using( HttpClient httpClient = Credentials.CreateHttpClient() ) {
					while( true ) {
						SimpleResponse response = await httpClient.SimpleSendAsync(
							HttpMethod.Get,
							new Uri( $"{ApiBase}/inbox/pull-requests?start={position}&role=reviewer" ),
							ensureSuccess: true
						).SafeAsync();

						JObject page = JObject.Parse( response.ResponseBody );
						position += (int)page["size"];

						foreach( JToken pullRequest in (JArray)page["values"] ) {
							long id = (long)pullRequest["id"];
							Uri url = new Uri( (string)pullRequest["links"]["self"][0]["href"] );
							string name = (string)pullRequest["title"];
							string repoName = (string)pullRequest["toRef"]["repository"]["name"];
							string repoSlug = (string)pullRequest["toRef"]["repository"]["slug"];
							DateTime opened = FromUnixTime( (long)pullRequest["createdDate"] );
							DateTime lastUpdated = FromUnixTime( (long)pullRequest["updatedDate"] );
							string author = (string)pullRequest["author"]["user"]["displayName"];
							long authorId = (long)pullRequest["author"]["user"]["id"];
							string authorSlug = (string)pullRequest["author"]["user"]["slug"];
							string projectKey = (string)pullRequest["toRef"]["repository"]["project"]["key"];

							string apiUrlString = $"{ApiBase}/projects/{projectKey}/repos/{repoSlug}/pull-requests/{id}";

							bool alreadyApproved = false;
							foreach( JToken reviewer in (JArray)pullRequest["reviewers"] ) {
								if( (long)reviewer["user"]["id"] == myUserId && (bool)reviewer["approved"] ) {
									alreadyApproved = true;
									break;
								}
							}

							if( alreadyApproved ) {
								continue;
							}

							DateTime? lastAuthorComment = null;
							DateTime? lastReviewerComment = null;
							DateTime? lastReviewerMention = null;
							DateTime? lastReview = null;
							DateTime lastPush = opened;

							foreach( ActivityInfo activity in await GetRelevantActivitiesAsync( httpClient, apiUrlString ).SafeAsync() ) {

								if( activity.Timestamp > lastUpdated ) {
									lastUpdated = activity.Timestamp;
								}

								switch( activity ) {
									case ReviewInfo review:
										if( review.UserId == myUserId && ( !lastReview.HasValue || review.Timestamp > lastReview ) ) {
											lastReview = review.Timestamp;
										}
										break;
									case PushInfo push:
										if( push.Timestamp > lastPush ) {
											lastPush = push.Timestamp;
										}
										break;
									case CommentInfo comment:
										if( comment.UserId == myUserId && !( lastReviewerComment > comment.Timestamp ) ) {
											lastReviewerComment = comment.Timestamp;
										}

										if( comment.UserId == authorId && !( lastAuthorComment > comment.Timestamp ) ) {
											lastAuthorComment = comment.Timestamp;
										}

										if( comment.MentionsMe && !( lastReviewerMention > comment.Timestamp ) ) {
											lastReviewerMention = comment.Timestamp;
										}

										break;
								}

							}

							Uri avatarUri = new Uri( $"{Domain}/users/{authorSlug}/avatar.png" );
							infos.Add( new PullRequestInfo(
								source: IntegrationType.BitBucketServer,
								integrationId: Id,
								url: url,
								repositoryName: repoName,
								authorName: author,
								pullRequestName: name,
								openedTimestamp: opened,
								updatedTimestamp: lastUpdated,
								lastPushTimestamp: lastPush,
								lastAuthorCommentTimestamp: lastAuthorComment,
								lastReviewerCommentTimestamp: lastReviewerComment,
								lastReviewerMentionTimestamp: lastReviewerMention,
								lastReviewTimestamp: lastReview,
								selfAssigned: authorId == myUserId,
								avatarUrl: avatarUri
							) );

						}

						if( (bool)page["isLastPage"] ) {
							break;
						}
					}
				}

				Errored = false;
				return new PullRequestFetchResults{
					PullRequests = infos.ToArray(),
					Errors = new SourceControlIntegrationException[0],
					Warnings = Array.Empty<string>()
				};
			} catch( Exception exception ) {
				Errored = true;
				Log.WriteException( exception, "[BitBucket Server] Error fetching pull requests." );
				return new PullRequestFetchResults {
					Errors = new[]{ new SourceControlIntegrationException( this, BitBucketServerIntegration.Instance, exception ), },
					PullRequests = new PullRequestInfo[0]
				};
			}
		}

		private string Domain => ((BitBucketServerCredentials)Credentials).Domain;
		private string ApiBase => string.Concat( Domain, "/rest/api/1.0" );

		private static readonly DateTime UNIX_EPOCH = new DateTime( 1970, 1, 1, 0, 0, 0, DateTimeKind.Utc );
		private static DateTime FromUnixTime( long milliseconds ) {
			return UNIX_EPOCH.AddMilliseconds( milliseconds );
		}

		private async Task<long> WhoAmI() {
			HttpResponseMessage response = await Credentials.CreateHttpClient().GetAsync(
				new Uri( $"{ApiBase}/application-properties" )
			).SafeAsync();
			response.EnsureSuccessStatusCode();

			long userId = long.Parse( response.Headers.GetValues( "X-AUSERID" ).First() );
			string username = response.Headers.GetValues( "X-AUSERNAME" ).First();

			if( !string.Equals( Credentials.CachedUsername, username ) ) {
				Credentials.CachedUsername = username;
				await BitBucketServerIntegration.Instance.SaveAsync().SafeAsync();
			}

			return userId;
		}

	}

}
