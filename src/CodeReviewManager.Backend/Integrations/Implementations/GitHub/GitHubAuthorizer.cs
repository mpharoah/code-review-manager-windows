﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json.Linq;
using FormData = System.Collections.Generic.IDictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations.Implementations.GitHub {

	internal sealed class GitHubAuthorizer : IIntegrationAuthorizer {

		internal static GitHubAuthorizer Instance = new GitHubAuthorizer();

		internal static readonly ProductInfoHeaderValue UserAgent = new ProductInfoHeaderValue( new ProductHeaderValue( "CodeReviewManager" ) );
		internal const string ClientId = "Iv1.1dfbb499a1ba29fe";
		internal const string ClientSecret = "48029cfe62950cfff1d7ff9a1376acf5a484a8ea";

		Task<AuthResult> IIntegrationAuthorizer.TryLoginAsync(
			FormData formData,
			string mfaCode
		) {
			return this.TryLoginAsync( formData["DeviceCode"] );
		}

		internal static async Task<GitHubDeviceAuthInfo> InitDeviceAuthFlowAsync() {
			using HttpClient client = new HttpClient();
			using HttpRequestMessage request = new HttpRequestMessage(
				method: HttpMethod.Post,
				requestUri: new Uri( $"https://github.com/login/device/code?client_id={ClientId}" )
			);

			request.Headers.UserAgent.Add( GitHubAuthorizer.UserAgent );
			request.Headers.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/vnd.github.v3+json" ) );

			using HttpResponseMessage response = await client.SendAsync( request ).SafeAsync();
			response.EnsureSuccessStatusCode();

			string jsonResponse = await response.Content.ReadAsStringAsync().SafeAsync();
			JObject details = JObject.Parse( jsonResponse );

			return new GitHubDeviceAuthInfo(
				(string)details["device_code"],
				(string)details["user_code"],
				(string)details["verification_uri"],
				(int)details["expires_in"],
				(int)details["interval"]
			);
		}

		internal async Task<AuthResult> RefreshTokenAsync(
			string refreshToken
		) {
			using HttpClient client = new HttpClient();
			using HttpRequestMessage request = new HttpRequestMessage(
				method: HttpMethod.Post,
				requestUri: new Uri( $"https://github.com/login/oauth/access_token?client_id={ClientId}&client_secret={ClientSecret}&grant_type=refresh_token&refresh_token={refreshToken}" )
			);

			request.Headers.UserAgent.Add( GitHubAuthorizer.UserAgent );
			request.Headers.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/vnd.github.v3+json" ) );

			try {
				using HttpResponseMessage response = await client.SendAsync( request ).SafeAsync();
				if( !response.IsSuccessStatusCode ) {
					return new AuthResult( AuthResultType.Denied, response.StatusCode );
				}

				string jsonResponse = await response.Content.ReadAsStringAsync().SafeAsync();
				JObject details = JObject.Parse( jsonResponse );

				return new AuthResult(
					new GitHubCredentials(
						(string)details["access_token"],
						(string)details["refresh_token"] ?? refreshToken,
						await GetUsernameAsync( (string)details["access_token"] )
					),
					response.StatusCode
				);
			} catch( Exception exception ) {
				Log.WriteException( exception, "Error calling GitHub API" );
				return new AuthResult( exception );
			}
		}

		private async Task<AuthResult> TryLoginAsync( string deviceCode ) {
			using HttpClient client = new HttpClient();
			using HttpRequestMessage request = new HttpRequestMessage(
				method: HttpMethod.Post,
				requestUri: new Uri( $"https://github.com/login/oauth/access_token?client_id={ClientId}&device_code={deviceCode}&grant_type=urn:ietf:params:oauth:grant-type:device_code" )
			);

			request.Headers.UserAgent.Add( GitHubAuthorizer.UserAgent );
			request.Headers.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/vnd.github.v3+json" ) );

			try {
				using HttpResponseMessage response = await client.SendAsync( request ).SafeAsync();
				if( !response.IsSuccessStatusCode ) {
					return new AuthResult( AuthResultType.Error, response.StatusCode );
				}

				string jsonResponse = await response.Content.ReadAsStringAsync().SafeAsync();
				JObject details = JObject.Parse( jsonResponse );

				if( details["access_token"] != null ) {
					return new AuthResult(
						new GitHubCredentials(
							(string)details["access_token"],
							(string)details["refresh_token"],
							await GetUsernameAsync( (string)details["access_token"] )
						),
						response.StatusCode
					);
				}
				
				if( string.Equals( (string)details["error"], "slow_down" ) ) {
					await Task.Delay( TimeSpan.FromSeconds( (int)details["interval"] ) ).SafeAsync();
				}

				return new AuthResult( AuthResultType.Denied, response.StatusCode );
			} catch( Exception exception ) {
				Log.WriteException( exception, "Error calling GitHub API" );
				return new AuthResult( exception );
			}
		}

		private async Task<string> GetUsernameAsync( string accessToken ) {
			using HttpClient client = new HttpClient();
			using HttpRequestMessage request = new HttpRequestMessage(
				method: HttpMethod.Get,
				requestUri: new Uri( "https://api.github.com/user" )
			);

			request.Headers.UserAgent.Add( GitHubAuthorizer.UserAgent );
			request.Headers.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/vnd.github.v3+json" ) );
			request.Headers.Authorization = new AuthenticationHeaderValue( "bearer", accessToken );

			using HttpResponseMessage response = await client.SendAsync( request ).SafeAsync();
			response.EnsureSuccessStatusCode();

			string jsonResponse = await response.Content.ReadAsStringAsync().SafeAsync();
			JObject details = JObject.Parse( jsonResponse );

			return (string)details["login"];

		}

	}

}
