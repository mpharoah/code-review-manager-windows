﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace CodeReviewManager.Backend.Integrations.Implementations.GitHub {

	[JsonObject( MemberSerialization.OptIn )]
	internal sealed class  GitHubCredentials : ICredentials {

		private readonly Guid m_id;

		[JsonConstructor]
		private GitHubCredentials(
			[JsonProperty( PropertyName = "RefreshToken", Required = Required.Always )] string refreshToken,
			[JsonProperty( PropertyName = "Id", Required = Required.Always )] Guid integrationId,
			[JsonProperty( PropertyName = "CachedUsername", Required = Required.Default )] string cachedUsername = null
		) {
			this.AuthToken = null;
			this.RefreshToken = refreshToken;
			m_id = integrationId;
			( this as ICredentials ).CachedUsername = cachedUsername;
		}

		internal GitHubCredentials(
			string authToken,
			string refreshToken,
			string username
		) : this( refreshToken, Guid.NewGuid(), username ) {
			this.AuthToken = authToken;
		}

		internal event EventHandler CredentialsChanged;

		internal void Update( GitHubCredentials newCredentials ) {
			this.AuthToken = newCredentials.AuthToken;
			this.RefreshToken = newCredentials.RefreshToken;
			(this as ICredentials).CachedUsername = (newCredentials as ICredentials).CachedUsername;
			CredentialsChanged?.Invoke( this, EventArgs.Empty );
		}

		HttpClient ICredentials.CreateHttpClient() {
			HttpClient client = new HttpClient();
			client.DefaultRequestHeaders.UserAgent.Add( GitHubAuthorizer.UserAgent );
			if( this.AuthToken != null ) {
				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue( "bearer", this.AuthToken );
			}
			client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/vnd.github.v3+json" ) );
			return client;
		}

		[JsonProperty( PropertyName = "CachedUsername" )]
		string ICredentials.CachedUsername { get; set; }

		[JsonIgnore]
		internal string AuthToken { get; private set; }

		[JsonProperty( PropertyName = "RefreshToken" )]
		internal string RefreshToken { get; private set; }

		[JsonProperty( PropertyName = "Id" )]
		Guid ICredentials.IntegrationId => m_id;

	}

}
