﻿using System;

namespace CodeReviewManager.Backend.Integrations.Implementations.GitHub {

	internal sealed class GitHubDeviceAuthInfo {

		internal GitHubDeviceAuthInfo(
			string deviceCode,
			string userCode,
			string verificationUrl,
			int expiresIn,
			int interval
		) {
			this.DeviceCode = deviceCode;
			this.UserCode = userCode;
			this.VerificationUrl = verificationUrl;
			this.NotBefore = DateTime.UtcNow + TimeSpan.FromSeconds( interval );
			this.NotAfter = DateTime.UtcNow + TimeSpan.FromSeconds( expiresIn );
		}

		public string DeviceCode { get; }
		public string UserCode { get; }
		public string VerificationUrl { get; }
		public DateTime NotBefore { get; }
		public DateTime NotAfter { get; }

	}

}
