﻿using CodeReviewManager.Backend.Integrations.UISpec;
using CodeReviewManager.Util;
using System.Threading.Tasks;

namespace CodeReviewManager.Backend.Integrations.Implementations.GitHub {
	partial class GitHubIntegration {

		public override async Task<ModeViewSpec[]> GetUiSpecAsync() {
			GitHubDeviceAuthInfo authInfo = await GitHubAuthorizer.InitDeviceAuthFlowAsync().SafeAsync();
			return new ModeViewSpec[] {
				new ModeViewSpec(
					new[]{
						new MessageFragment( "To enable GitHub integration, go to " ),
						new MessageFragment( authInfo.VerificationUrl, linksTo: -1 ),
						new MessageFragment( " and enter the 8 digit code displayed below. Once the code has been verified on the GitHub website, click the Sign In button below." )
					},
					new[]{
						new Field( "UserCode", "Verification Code", initialValue: authInfo.UserCode, isReadonly: true )
					},
					new[]{
						new HiddenField( "DeviceCode", authInfo.DeviceCode )
					}
				)
			};
		}

	}
}
