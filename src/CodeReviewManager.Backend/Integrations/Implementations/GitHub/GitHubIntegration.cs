﻿using CodeReviewManager.Backend.Config;
using CodeReviewManager.Util;
using System;

namespace CodeReviewManager.Backend.Integrations.Implementations.GitHub {

	internal sealed partial class GitHubIntegration : IntegrationBase {

		private GitHubIntegration() : base() {}
		internal static GitHubIntegration Instance = new GitHubIntegration();

		public override IntegrationType Type => IntegrationType.GitHub;
		public override string Name => "GitHub";
		protected override IIntegrationAuthorizer Authorizer => GitHubAuthorizer.Instance;

		protected override ISourceControlSignIn CreateSignIn( ICredentials credentials ) {
			(credentials as GitHubCredentials).CredentialsChanged += ( object obj, EventArgs args ) => this.SaveAsync().WaitSync();
			return new GitHubSignIn( credentials );
		}

		protected override JsonConfigFile<ICredentials[]> GetCredentialsFile( string profilePath ) {
			return new JsonConfigFile<GitHubCredentials[], ICredentials[]>(
				profilePath,
				"integrations\\GitHub.json"
			);
		}

	}

}
