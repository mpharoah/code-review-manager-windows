﻿using System;

namespace CodeReviewManager.Backend.Integrations.Implementations.GitHub {

	partial class GitHubSignIn {

		private static readonly Uri GRAPH_QL_ENDPOINT = new Uri( "https://api.github.com/graphql" );
		private const string QUERY_TEMPLATE = @"{{
  search(query: ""is:open is:pr {1}:{0} archived:false"", type: ISSUE, first: {2} ) {{
    pageInfo {{
      hasNextPage
      endCursor
    }}
    nodes {{
      ... on PullRequest {{
        repository {{
          nameWithOwner
        }}
        url
        author {{
          login
          avatarUrl
          ... on User {{
            name
          }}
        }}
        createdAt
        updatedAt
        title
        commits(last: 1) {{
          edges {{
            node {{
              commit {{
                committedDate
                checkSuites(first:1) {{
                  nodes {{
                    createdAt
                  }}
                }}
              }}
            }}
          }}
        }}
        reviews(last: 70) {{
          edges {{
            node {{
              author {{
                login
              }}
              submittedAt
              state
              bodyText
              comments(last: 70) {{
                edges {{
                  node {{
                    author {{
                      login
                    }}
                    publishedAt
                    bodyText
                  }}
                }}
              }}
            }}
          }}
        }}
        comments(last: 70) {{
          edges {{
            node {{
              author {{
                login
              }}
              publishedAt
              bodyText
            }}
          }}
        }}
      }}
    }}
  }}
}}";

	}

}
