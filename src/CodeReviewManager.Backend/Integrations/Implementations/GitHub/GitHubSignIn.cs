﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CodeReviewManager.Backend.Exceptions;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CodeReviewManager.Backend.Integrations.Implementations.GitHub {

	internal sealed partial class GitHubSignIn : ISourceControlSignIn {

		private readonly IDictionary<string, int> m_numPreviousResults;
		private readonly TwoPhaseAsyncLock m_lock;
		private DateTime m_nextRefresh = DateTime.MaxValue;

		internal GitHubSignIn( ICredentials credentials ) {
			Credentials = credentials;
			Errored = false;
			m_numPreviousResults = new Dictionary<string, int>();
			m_lock = new TwoPhaseAsyncLock();
		}

		public ICredentials Credentials { get; }

		public Guid Id => Credentials.IntegrationId;
		public string Username => Credentials.CachedUsername;
		public string LocationInfo => string.Empty;

		public bool Errored { get; private set; }

		public async Task<bool> Verify() {
			using IDisposable xlock = await m_lock.AcquireExclusiveLockAsync().SafeAsync();

			GitHubCredentials ghc = this.Credentials as GitHubCredentials;
			GitHubAuthorizer authorizer = GitHubAuthorizer.Instance;

			AuthResult testResult = await authorizer.RefreshTokenAsync( ghc.RefreshToken ).SafeAsync();
			if( testResult.Type == AuthResultType.Success ) {
				ghc.Update( testResult.Credentials as GitHubCredentials );
				m_nextRefresh = DateTime.UtcNow + TimeSpan.FromHours( 7.75 );

				Errored = false;
				return true;
			}

			Errored = true;
			return false;
		}

		public async Task<PullRequestFetchResults> GetPullRequestsAsync() {
			if( !this.Errored && DateTime.UtcNow >= m_nextRefresh ) {
				await this.Verify().SafeAsync();
			}

			using IDisposable slock = await m_lock.AcquireSharedLockAsync().SafeAsync();
			try {
				if(
					string.IsNullOrEmpty( Credentials.CachedUsername ) &&
					!( await Verify().SafeAsync() )
				) {
					throw new Exception( "Sign in failed" );
				}

				var responseObjects = new ConcurrentDictionary<string, JObject>();

				AsyncRef<bool> hasMissingPullRequests = new AsyncRef<bool>( false );
				await new[]{ "review-requested", "reviewed-by", "assignee" }.ForEachInParallelAsync(
					filter => QueryPullRequestsAsync( filter, responseObjects, hasMissingPullRequests )
				).SafeAsync();

				var pullRequests = new List<PullRequestInfo>();
				foreach( JObject pullRequestData in responseObjects.Values ) {
					DateTime? lastReview = null;
					DateTime? lastAuthorComment = null;
					DateTime? lastReviewerComment = null;
					DateTime? lastReviewerMention = null;
					bool approved = false;

					string authorUsername = (string)pullRequestData["author"]["login"];
					Regex mentionRegex = new Regex(
						pattern: @"(^|[^a-zA-Z0-9\-])" + Regex.Escape( $"@{Credentials.CachedUsername}" ) + @"($|[^a-zA-Z0-9\-])",
						options: RegexOptions.ExplicitCapture
					);

					foreach( CommentInfo comment in ParseReviewsAndComments( pullRequestData ) ) {
						if( mentionRegex.IsMatch( comment.Text ) && !( comment.Timestamp < lastReviewerMention ) ) {
							lastReviewerMention = comment.Timestamp;
						}

						if( authorUsername.Equals( comment.Author ) && !( comment.Timestamp < lastAuthorComment ) ) {
							lastAuthorComment = comment.Timestamp;
						} else if( Credentials.CachedUsername.Equals( comment.Author ) ) {
							if( comment.IsReview && !( comment.Timestamp < lastReview ) ) {
								lastReview = comment.Timestamp;
								approved = comment.IsApproval;
							}

							if( !string.IsNullOrEmpty( comment.Text ) && !( comment.Timestamp < lastReviewerComment ) ) {
								lastReviewerComment = comment.Timestamp;
							}
						}
					}

					if( approved ) {
						Log.WriteDebug( Log.GitHub, "Filtered out approved pull request:" );
						Log.WriteDebug( Log.GitHub, () => pullRequestData.ToString() );
						continue;
					}

					Log.WriteDebug( Log.GitHub, "Found pull request data:" );
					Log.WriteDebug( Log.GitHub, () => pullRequestData.ToString() );

					Uri url = new Uri( (string)pullRequestData["url"] );
					string repoName = (string)pullRequestData["repository"]["nameWithOwner"];
					string authorFullName = (string)pullRequestData["author"]["name"] ?? authorUsername;
					string name = (string)pullRequestData["title"];
					DateTime opened = ((DateTime)pullRequestData["createdAt"]).ToUniversalTime();
					DateTime lastUpdated = ((DateTime)pullRequestData["updatedAt"]).ToUniversalTime();
					string avatarUrl = (string)pullRequestData["author"]["avatarUrl"];

					DateTime? lastPush = null;
					JArray lastCommit = (JArray)pullRequestData["commits"]["edges"];
					if( lastCommit.Count > 0 ) {
						lastPush = ((DateTime?)lastCommit[0]["node"]["commit"]["committedDate"])?.ToUniversalTime();
						JArray checks = (JArray)lastCommit[0]["node"]["commit"]["checkSuites"]["nodes"];
						if( checks != null && checks.Count > 0 ) {
							DateTime? checkDate = ((DateTime?)checks[0]["createdAt"])?.ToUniversalTime();
							if( checkDate.HasValue && (!lastPush.HasValue || checkDate.Value > lastPush.Value) ) {
								lastPush = checkDate;
							}
						}
					}

					PullRequestInfo pullRequestInfo = new PullRequestInfo(
						source: IntegrationType.GitHub,
						integrationId: Id,
						url: url,
						repositoryName: repoName,
						authorName: authorFullName,
						pullRequestName: name,
						openedTimestamp: opened,
						updatedTimestamp: lastUpdated,
						lastPushTimestamp: lastPush ?? opened,
						lastAuthorCommentTimestamp: lastAuthorComment,
						lastReviewerCommentTimestamp: lastReviewerComment,
						lastReviewerMentionTimestamp: lastReviewerMention,
						lastReviewTimestamp: lastReview,
						selfAssigned: string.Equals( Credentials.CachedUsername, authorUsername ),
						avatarUrl: avatarUrl != null ? new Uri( avatarUrl ) : null
					);

					Log.WriteDebug( Log.GitHub, "Parsed pull request data as:" );
					Log.WriteDebug( Log.GitHub, () => JsonConvert.SerializeObject( pullRequestInfo ) );
					pullRequests.Add( pullRequestInfo );
				}

				Errored = false;
				return new PullRequestFetchResults {
					PullRequests = pullRequests.ToArray(),
					Errors = new SourceControlIntegrationException[0],
					Warnings = hasMissingPullRequests.Value ? new string[]{
						"Some GitHub pull requests could not be fetched because an organization you belong to has not allowed authorization from this application."
					} : Array.Empty<string>()
				};
			} catch( Exception exception ) {
				Errored = true;
				Log.WriteException( exception, "[GitHub] Error fetching pull requests." );
				return new PullRequestFetchResults{
					Errors = new[]{ new SourceControlIntegrationException( this, GitHubIntegration.Instance, exception ),  },
					PullRequests = new PullRequestInfo[0],
					Warnings = Array.Empty<string>()
				};
			}
		}

		private async Task QueryPullRequestsAsync(
			string filter,
			ConcurrentDictionary<string, JObject> pullRequests,
			AsyncRef<bool> hasMissingPullRequests,
			string cursor = null,
			int resultsSoFar = 0
		) {
			if( !m_numPreviousResults.TryGetValue( filter, out int pageSize ) ) {
				pageSize = 45;
			}

			pageSize += 5;
			if( pageSize < 15 ) {
				pageSize = 15;
			} else if( pageSize > 95 ) {
				pageSize = 95;
			}

			Log.WriteDebug( Log.GitHub, $"fetching {( cursor != null ? "next" : "first" )} page for {filter} (pageSize = {pageSize})" );

			string query = string.Format(
				QUERY_TEMPLATE,
				Credentials.CachedUsername,
				filter,
				string.Concat( pageSize.ToString(), ( cursor != null ) ? $", after: \"{cursor}\"" : "" )
			);

			using( HttpClient httpClient = Credentials.CreateHttpClient() )
			using( HttpRequestMessage request = new HttpRequestMessage( HttpMethod.Post, GRAPH_QL_ENDPOINT ) ) {
				request.Content = new StringContent( JsonConvert.SerializeObject( new { query } ), Encoding.UTF8 );
				request.Content.Headers.ContentType.MediaType = "application/json";
				request.Content.Headers.ContentType.CharSet = "utf-8";

				using( HttpResponseMessage response = await httpClient.SendAsync( request ).SafeAsync() ) {
					response.EnsureSuccessStatusCode();
					string jsonResponse = await response.Content.ReadAsStringAsync().SafeAsync();

					JToken result = JObject.Parse( jsonResponse )["data"]["search"];

					JArray hits = (JArray)result["nodes"];
					foreach( JToken pr in hits ) {
						if( pr.Type == JTokenType.Null ) {
							hasMissingPullRequests.Value = true;
							continue;
						}
						pullRequests[(string)pr["url"]] = (JObject)pr;
					}

					if( !(bool)result["pageInfo"]["hasNextPage"] ) {
						m_numPreviousResults[filter] = resultsSoFar + hits.Count;
						return;
					}
					cursor = (string)result["pageInfo"]["endCursor"];
				}
			}

			await QueryPullRequestsAsync( filter, pullRequests, hasMissingPullRequests, cursor, resultsSoFar + pageSize ).SafeAsync();
		}

		private IEnumerable<CommentInfo> ParseReviewsAndComments( JObject pullRequestData ) {
			foreach( JToken review in (JArray)pullRequestData["reviews"]["edges"] ) {
				JObject node = (JObject)review["node"];
				DateTime? timestamp = ((DateTime?)node["submittedAt"])?.ToUniversalTime();
				if( !timestamp.HasValue ) continue;

				string state = (string)node["state"];
				yield return new CommentInfo(
					(string)node["author"]["login"],
					timestamp.Value,
					(string)node["bodyText"] ?? string.Empty,
					isReview: !"COMMENTED".Equals( state ),
					isApproval: "APPROVED".Equals( state )
				);

				foreach( CommentInfo commentInfo in ParseComments( node ) ) {
					yield return commentInfo;
				}
			}

			foreach( CommentInfo commentInfo in ParseComments( pullRequestData ) ) {
				yield return commentInfo;
			}
		}

		private IEnumerable<CommentInfo> ParseComments( JObject threadData ) {
			JArray comments = (JArray)threadData["comments"]["edges"];
			if( comments == null ) yield break;

			foreach( JToken review in comments ) {
				JObject node = (JObject)review["node"];
				DateTime? timestamp = ((DateTime?)node["publishedAt"])?.ToUniversalTime();
				if( !timestamp.HasValue ) continue;

				yield return new CommentInfo(
					(string)node["author"]["login"],
					timestamp.Value,
					(string)node["bodyText"] ?? string.Empty
				);
			}
		}

		private sealed class CommentInfo {

			internal CommentInfo(
				string author,
				DateTime timestamp,
				string text,
				bool isReview = false,
				bool isApproval = false
			) {
				Author = author;
				Timestamp = timestamp;
				Text = text;
				IsReview = isReview;
				IsApproval = isApproval;
			}

			internal string Author { get; }
			internal DateTime Timestamp { get; }
			internal string Text { get; }
			internal bool IsReview { get; }
			internal bool IsApproval { get; }

		}

	}

}
