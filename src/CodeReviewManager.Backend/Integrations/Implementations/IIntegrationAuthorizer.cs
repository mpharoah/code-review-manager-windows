﻿using System.Threading.Tasks;
using FormData = System.Collections.Generic.IDictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations.Implementations {

	internal interface IIntegrationAuthorizer {

		Task<AuthResult> TryLoginAsync(
			FormData formData,
			string mfaCode = null
		);

	}

}
