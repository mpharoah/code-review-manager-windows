﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using CodeReviewManager.Backend.Config;
using CodeReviewManager.Backend.Integrations.UISpec;
using CodeReviewManager.Backend.Profiles;
using CodeReviewManager.Util;
using FormData = System.Collections.Generic.IDictionary<string, string>;

namespace CodeReviewManager.Backend.Integrations.Implementations {

	internal abstract class IntegrationBase : ISourceControlIntegration {

		private JsonConfigFile<ICredentials[]> m_authFile;
		private readonly IList<ISourceControlSignIn> m_signIns;

		protected IntegrationBase() {
			m_authFile = GetCredentialsFile( ProfileManager.CurrentProfilePath );
			m_signIns = m_authFile.TryLoadAsync().WaitSync()?.Select( CreateSignIn )?.ToList() ?? new List<ISourceControlSignIn>();

			ProfileManager.OnProfileChangeBegin += @event => {
				SaveAsync().WaitSync();
				m_signIns.Clear();
				m_authFile = GetCredentialsFile( ProfileManager.GetProfilePath( @event.NewProfile ) );
				foreach( ICredentials credentials in m_authFile.TryLoadAsync().WaitSync() ?? new ICredentials[0] ) {
					m_signIns.Add( CreateSignIn( credentials ) );
				}
			};
		}

		protected abstract IIntegrationAuthorizer Authorizer { get; }

		protected abstract JsonConfigFile<ICredentials[]> GetCredentialsFile( string profilePath );
		protected abstract ISourceControlSignIn CreateSignIn( ICredentials credentials );

		public async Task<AuthResult> TryCreateSignInAsync(
			FormData formData,
			string mfaCode,
			CancellationToken cancellationToken
		) {
			AuthResult result = await Authorizer.TryLoginAsync( formData, mfaCode ).SafeAsync();
			if( cancellationToken.IsCancellationRequested ) {
				return new AuthResult( AuthResultType.Cancelled, default( HttpStatusCode ) );
			}

			if( result.Type == AuthResultType.Success ) {
				m_signIns.Add( CreateSignIn( result.Credentials ) );
				await SaveAsync().SafeAsync();
			}

			return result;
		}

		public Task RemoveSignIn( ISourceControlSignInInfo signIn ) {
			m_signIns.Remove( signIn as ISourceControlSignIn );
			return SaveAsync();
		}

		public async Task<PullRequestFetchResults> GetPullRequestsAsync() {
			return (
				await m_signIns.MapInParallelAsync(
					s => s.GetPullRequestsAsync()
				).SafeAsync()
			).Merge();
		}

		public Task SaveAsync() {
			return m_authFile.SaveAsync( m_signIns.Select( s => s.Credentials ).ToArray() );
		}

		public IEnumerable<ISourceControlSignInInfo> SignIns => m_signIns;

		public abstract Task<ModeViewSpec[]> GetUiSpecAsync();
		public abstract IntegrationType Type { get; }
		public abstract string Name { get; }

	}

}
