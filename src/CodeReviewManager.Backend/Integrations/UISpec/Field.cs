﻿using System;

namespace CodeReviewManager.Backend.Integrations.UISpec {

	public sealed class Field {

		internal Field(
			string id,
			string name,
			string initialValue = null,
			bool isReadonly = false,
			bool isPassword = false,
			Func<string,string> valueTransform = null
		) {
			Id = id;
			Name = name;
			DefaultValue = initialValue;
			IsPassword = isPassword;
			Readonly = isReadonly;
			ValueTransform = valueTransform ?? Identity;
		}

		public string Id { get; }
		public string Name { get; }
		public string DefaultValue { get; }
		public bool IsPassword { get; }
		public bool Readonly { get; }

		public Func<string,string> ValueTransform { get; }

		private static string Identity( string value ) => value;
	}

}
