﻿namespace CodeReviewManager.Backend.Integrations.UISpec {

	public sealed class HiddenField {

		internal HiddenField( string id, string value ) {
			this.Id = id;
			this.Value = value;
		}

		public string Id { get; }
		public string Value { get; }

	}

}
