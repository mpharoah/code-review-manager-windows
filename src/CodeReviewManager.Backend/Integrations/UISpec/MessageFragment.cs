﻿namespace CodeReviewManager.Backend.Integrations.UISpec {

	public sealed class MessageFragment {

		internal MessageFragment(
			string text,
			int? linksTo = null
		) {
			Text = text;
			LinksTo = linksTo;
		}

		public string Text { get; }
		public int? LinksTo { get; }

	}

}
