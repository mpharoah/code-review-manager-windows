﻿using System;

namespace CodeReviewManager.Backend.Integrations.UISpec {

	public sealed class ModeViewSpec {

		internal ModeViewSpec(
			MessageFragment[] messageParts,
			Field[] fields,
			HiddenField[] hiddenFields = null
		) {
			Message = messageParts;
			Fields = fields;
			HiddenFields = hiddenFields ?? Array.Empty<HiddenField>();
		}

		public MessageFragment[] Message { get; }
		public Field[] Fields { get; }
		public HiddenField[] HiddenFields { get; }

	}

}
