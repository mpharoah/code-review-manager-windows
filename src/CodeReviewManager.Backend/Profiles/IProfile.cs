﻿namespace CodeReviewManager.Backend.Profiles {

	public interface IProfile {
		string Id { get; }
		string Name { get; set; }

		bool IsActive { get; }
		void Delete();
	}

}
