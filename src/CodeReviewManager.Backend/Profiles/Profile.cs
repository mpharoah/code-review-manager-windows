﻿using Newtonsoft.Json;

namespace CodeReviewManager.Backend.Profiles {

	[JsonObject( MemberSerialization.OptIn )]
	internal sealed class Profile : IProfile {

		private readonly string m_id;
		private string m_name;

		[JsonConstructor]
		internal Profile(
			[JsonProperty( PropertyName = "Id", Required = Required.DisallowNull )] string id,
			[JsonProperty( PropertyName = "Name", Required = Required.DisallowNull )] string name
		) {
			m_id = id;
			m_name = name;
		}

		[JsonProperty( PropertyName = "Id" )]
		string IProfile.Id => m_id;
		
		[JsonProperty( PropertyName = "Name" )]
		string IProfile.Name {
			get => m_name;
			set => Rename( value );
		}

		private void Rename( string name ) {
			m_name = name;
			ProfileManager.Save();
		}

		bool IProfile.IsActive => string.Equals( m_id, ProfileManager.CurrentProfileId );

		void IProfile.Delete() {
			ProfileManager.Delete( m_id );
		}

	}

}
