﻿namespace CodeReviewManager.Backend.Profiles {

	internal sealed class ProfileChangeBeginEvent {

		public ProfileChangeBeginEvent(
			IProfile newProfile
		) {
			NewProfile = newProfile;
		}

		public IProfile NewProfile { get; }

	}

}
