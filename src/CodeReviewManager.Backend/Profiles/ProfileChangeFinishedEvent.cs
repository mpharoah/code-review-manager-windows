﻿using CodeReviewManager.Backend.Config;

namespace CodeReviewManager.Backend.Profiles {

	internal sealed class ProfileChangeFinishedEvent {

		public ProfileChangeFinishedEvent(
			IProfile newProfile,
			ConfigData oldConfig
		) {
			NewProfile = newProfile;
			OldConfig = oldConfig;
		}

		public IProfile NewProfile { get; }
		public ConfigData OldConfig { get; }

	}

}
