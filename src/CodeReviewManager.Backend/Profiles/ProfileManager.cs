﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CodeReviewManager.Backend.Config;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json;

namespace CodeReviewManager.Backend.Profiles {

	public static class ProfileManager {

		private static readonly JsonConfigFile<ProfilesData> m_configFile;
		private static readonly ProfilesData m_data;
		private static readonly object m_lock = new object();

		//TODO: should make profile switching and creation asynchronous

		static ProfileManager() {
			m_configFile = new JsonConfigFile<ProfilesData>( "profiles.json" );
			m_data = m_configFile.TryLoadAsync().WaitSync();

			if( m_data == null ) {
				Profile defaultProfile = new Profile( GenerateId(), "default" );
				m_data = new ProfilesData(
					((IProfile)defaultProfile).Id,
					new[]{ defaultProfile }
				);
				Save();
			}
		}

		public static IProfile Create( string name, bool copyConfig ) {
			IProfile newProfile;
			lock( m_lock ) {
				newProfile = CreateWithoutLock( name );
			}
			if( copyConfig ) {
				ConfigManager.CopyToProfileAsync( newProfile );
			}
			return newProfile;
		}

		public static IReadOnlyList<IProfile> FetchAll() {
			lock( m_lock ) {
				return m_data.Profiles;
			}
		}

		public static void SwitchTo( IProfile profile ) {
			ConfigData oldConfig;
			lock( m_lock ) {
				oldConfig = ConfigManager.Config;
				m_data.CurrentProfileId = profile.Id;
				Save();
				OnProfileChangeBegin?.Invoke( new ProfileChangeBeginEvent( profile ) );
			}
			OnProfileChangeFinish?.Invoke( new ProfileChangeFinishedEvent( profile, oldConfig ) );
		}

		public static void Delete( string profileId ) {
			bool profileChanged = false;
			ProfileChangeFinishedEvent changeEvent = default( ProfileChangeFinishedEvent );

			lock( m_lock ) {
				m_data.Profiles = m_data.Profiles.Where( profile => !profile.Id.Equals( profileId ) ).ToArray();
				if( m_data.Profiles.Length < 1 ) {
					CreateWithoutLock( "default" );
				}
				if( !m_data.Profiles.Any( profile => profile.Id.Equals( m_data.CurrentProfileId ) ) ) {
					IProfile activeProfile = m_data.Profiles[0];
					m_data.CurrentProfileId = activeProfile.Id;
					ConfigData oldConfig = ConfigManager.Config;
					OnProfileChangeBegin?.Invoke( new ProfileChangeBeginEvent( activeProfile ) );
					profileChanged = true;
					changeEvent = new ProfileChangeFinishedEvent( activeProfile, oldConfig );
				}
			}

			Save();
			try {
				Directory.Delete(
					Path.Combine(
						Environment.GetFolderPath( Environment.SpecialFolder.UserProfile ),
						".config\\code-review-manager\\profiles", profileId
					),
					recursive: true
				);
			} catch( Exception exception ) {
				Log.WriteException( exception, "Failed to clean up data from deleted profile." );
			}

			if( profileChanged ) {
				OnProfileChangeFinish?.Invoke( changeEvent );
			}
		}

		internal static string CurrentProfilePath => string.Concat( "profiles\\", CurrentProfileId );

		internal static string GetProfilePath( IProfile profile ) {
			return string.Concat( "profiles\\", profile.Id );
		}

		internal static string CurrentProfileId {
			get {
				lock( m_lock ) return m_data.CurrentProfileId;
			}
		}

		internal static event Action<ProfileChangeBeginEvent> OnProfileChangeBegin;
		internal static event Action<ProfileChangeFinishedEvent> OnProfileChangeFinish;

		private static IProfile CreateWithoutLock( string name ) {
			IProfile profile = new Profile( GenerateId(), name );
			IProfile[] newProfileList = new IProfile[m_data.Profiles.Length + 1];
			Array.Copy( m_data.Profiles, newProfileList, m_data.Profiles.Length );
			newProfileList[m_data.Profiles.Length] = profile;
			m_data.Profiles = newProfileList;
			Save();
			return profile;
		}

		private static string GenerateId() {
			byte[] guid = Guid.NewGuid().ToByteArray();
			byte[] bytes = new byte[6];
			Array.Copy( guid, 10, bytes, 0, 6 );
			return Convert.ToBase64String( bytes ).Replace( '/', '-' );
		}

		internal static void Save() {
			m_configFile.SaveAsync( m_data ).WaitSync();
		}

		[JsonObject( MemberSerialization.OptIn )]
		private sealed class ProfilesData {

			[JsonConstructor]
			public ProfilesData(
				[JsonProperty( PropertyName = nameof( CurrentProfileId), Required = Required.Always )] string currentProfileId,
				[JsonProperty( PropertyName = nameof( Profiles ), Required = Required.Always )] Profile[] profiles
			) {
				CurrentProfileId = currentProfileId;
				Profiles = profiles;
			}

			[JsonProperty]
			internal string CurrentProfileId { get; set; }

			[JsonProperty]
			internal IProfile[] Profiles { get; set; }

		}

	}

}
