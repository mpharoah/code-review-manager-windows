﻿using System;
using CodeReviewManager.Backend.Config;
using Newtonsoft.Json;

namespace CodeReviewManager.Backend {

	[JsonObject( MemberSerialization.OptIn )]
	internal sealed class PullRequest : IPullRequest {

		private readonly PullRequestInfo m_info;

		private NotificationStatus m_notificationStatus;
		private DateTime? m_wakeTime;
		private DateTime m_lastUpdate;

		[JsonConstructor]
		internal PullRequest(
			[JsonProperty( PropertyName = "CachedData" )] PullRequestInfo info,
			[JsonProperty( PropertyName = "Notifications" )] NotificationStatus status,
			[JsonProperty( PropertyName = "LastRefresh" )] DateTime? lastUpdated = null,
			[JsonProperty( PropertyName = "UnsnoozeTime" )] DateTime? wakeTime = null
		) {
			m_info = info;
			m_lastUpdate = lastUpdated ?? DateTime.UtcNow;
			m_wakeTime = wakeTime;
			m_notificationStatus = lastUpdated.HasValue ? status : GetNewNotificationStatus( info, status, info.Opened );
		}

		internal PullRequest(
			PullRequestInfo pullRequestInfo
		) : this(
			info: pullRequestInfo,
			status: ConfigManager.Config.MuteNewPullRequests ? NotificationStatus.AutomaticallyMuted : NotificationStatus.On
		) {}

		internal PullRequest CloneAndUpdate( PullRequestInfo updatedInfo ) {
			if( !updatedInfo.Key.Equals( m_info.Key ) || !updatedInfo.Source.Equals( m_info.Source ) ) {
				throw new ArgumentException(
					message: @"Pull request information appears to be for a different pull request",
					paramName: nameof( updatedInfo )
				);
			}

			return new PullRequest(
				updatedInfo,
				GetNewNotificationStatus( updatedInfo, m_notificationStatus, m_lastUpdate ),
				DateTime.UtcNow,
				m_wakeTime
			);
		}

		private static NotificationStatus GetNewNotificationStatus(
			PullRequestInfo updatedInfo,
			NotificationStatus previousStatus,
			DateTime lastUpdated
		) {
			ConfigData config = ConfigManager.Config;

			NotificationStatus status = previousStatus;
			if( updatedInfo.LastPush > lastUpdated && ShouldUnmute( status, config.UnmuteOnPush ) ) {
				lastUpdated = updatedInfo.LastPush;
				status = NotificationStatus.On;
			}

			if( updatedInfo.LastAuthorComment > lastUpdated && ShouldUnmute( status, config.UnmuteOnAuthorComment ) ) {
				lastUpdated = updatedInfo.LastAuthorComment.Value;
				status = NotificationStatus.On;
			}

			if( updatedInfo.LastReviewerMention > lastUpdated && ShouldUnmute( status, config.UnmuteOnMention ) ) {
				lastUpdated = updatedInfo.LastReviewerMention.Value;
				status = NotificationStatus.On;
			}

			if( status == NotificationStatus.On && updatedInfo.LastReview > lastUpdated ) {
				status = NotificationStatus.AutomaticallyMuted;
			}

			if(
				config.MuteAfterCommenting &&
				status == NotificationStatus.On &&
				updatedInfo.LastReviewerComment > lastUpdated
			) {
				status = NotificationStatus.AutomaticallyMuted;
			}

			return status;
		}

		public void Mute( bool automatic = false ) {
			bool wasMuted = this.Muted;
			m_notificationStatus = automatic ? NotificationStatus.AutomaticallyMuted : NotificationStatus.ManuallyMuted;

			if( !wasMuted ) {
				CodeReviewManagerCore.NotifyPullRequestMuted();
				if( ConfigManager.Config.MutedAtBottom ) {
					CodeReviewManagerCore.NotifyOrderChanged();
				}
			}
		}

		public void Unmute() {
			bool wasMuted = this.Muted;
			m_notificationStatus = NotificationStatus.On;

			m_wakeTime = null;

			if( wasMuted ) {
				CodeReviewManagerCore.NotifyPullRequestUnmuted();
				if( ConfigManager.Config.MutedAtBottom ) {
					CodeReviewManagerCore.NotifyOrderChanged();
				}
			}
		}

		public void Snooze() {
			bool wasMuted = this.Muted;
			m_wakeTime = DateTime.UtcNow + TimeSpan.FromHours(
				CodeReviewManagerCore.Instance.GetSettings().SnoozeTimeHours
			);

			if( !wasMuted ) {
				CodeReviewManagerCore.NotifyPullRequestMuted();
				if( ConfigManager.Config.MutedAtBottom ) {
					CodeReviewManagerCore.NotifyOrderChanged();
				}
			}
		}


		public Uri Url => m_info.Url;
		public Guid SignInId => m_info.IntegrationId;
		public string Repository => m_info.Repository;
		public string AuthorName => m_info.AuthorName;
		public string Name => m_info.Name;
		public DateTime Opened => m_info.Opened;
		public DateTime LastUpdated => m_info.LastUpdated;
		public DateTime LastPushed => m_info.LastPush;
		public DateTime LastAuthorComment => m_info.LastAuthorComment ?? m_info.Opened;
		public DateTime LastReviewerComment => m_info.LastReviewerComment ?? m_info.Opened;
		public IntegrationType Source => m_info.Source;
		public Uri AuthorAvatarUrl => m_info.AvatarUrl;

		public NotificationStatus NotificationStatus => this.Snoozed ? NotificationStatus.Snooze : m_notificationStatus;
		public bool Muted => this.NotificationStatus != NotificationStatus.On;
		public bool Snoozed => DateTime.UtcNow < m_wakeTime;
		[JsonProperty( PropertyName = "Notifications" )]
		private NotificationStatus ActualNotificationStatus => m_notificationStatus;

		internal string Key => m_info.Key;

		[JsonProperty( PropertyName = "CachedData" )]
		private PullRequestInfo Info => m_info;

		[JsonProperty( PropertyName = "LastRefresh" )]
		private DateTime LastRefresh => m_lastUpdate;

		[JsonProperty( PropertyName = "UnsnoozeTime", NullValueHandling = NullValueHandling.Include)]
		private DateTime? WakeTime => m_wakeTime;

		public bool Errored { get; internal set; } = false;

		private static bool ShouldUnmute(
			NotificationStatus currentStatus,
			ConfigData.UnmuteBehaviour unmuteBehaviour
		) {
			switch( unmuteBehaviour ) {
				case ConfigData.UnmuteBehaviour.AlwaysUnmute:
					return true;
				case ConfigData.UnmuteBehaviour.UnmuteIfAutoMuted:
					return currentStatus != NotificationStatus.ManuallyMuted;
				case ConfigData.UnmuteBehaviour.NeverUnmute:
					return false;
			}
			return false;
		}

	}
}
