﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CodeReviewManager.Backend.Config;

using SortOrder = CodeReviewManager.Backend.Config.ConfigData.SortOrder;

namespace CodeReviewManager.Backend {

	internal sealed class PullRequestComparer : IComparer<IPullRequest> {

		private readonly Func<IPullRequest, DateTime> m_dateExtractor;
		private readonly int m_order;
		private readonly bool m_mutedAtBottom;

		public PullRequestComparer( ConfigData config ) {
			m_order = config.NewestAtTop ? -1 : 1;
			m_mutedAtBottom = config.MutedAtBottom;
			switch( config.OrderBy ) {
				case SortOrder.OpenDate: m_dateExtractor = pr => pr.Opened; break;
				case SortOrder.LastUpdated: m_dateExtractor = pr => pr.LastUpdated; break;
				case SortOrder.LastAuthorComment: m_dateExtractor = pr => pr.LastAuthorComment; break;
				case SortOrder.LastReviewerComment: m_dateExtractor = pr => pr.LastReviewerComment; break;
				case SortOrder.LastPush: m_dateExtractor = pr => pr.LastPushed; break;
				default: throw new InvalidEnumArgumentException(
					argumentName: nameof( config.OrderBy ),
					invalidValue: (int)config.OrderBy,
					enumClass: typeof( SortOrder )
				);
			}
		}

		int IComparer<IPullRequest>.Compare( IPullRequest x, IPullRequest y ) {
			if( m_mutedAtBottom && x.Muted != y.Muted ) {
				return x.Muted ? 1 : -1;
			}

			return m_order * DateTime.Compare(
				m_dateExtractor( x ),
				m_dateExtractor( y )
			);
		}

	}

}
