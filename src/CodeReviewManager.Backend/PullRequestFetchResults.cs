﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using CodeReviewManager.Backend.Exceptions;

namespace CodeReviewManager.Backend {

	internal struct PullRequestFetchResults {
		public PullRequestInfo[] PullRequests;
		public SourceControlIntegrationException[] Errors;
		public string[] Warnings;
	}

	[EditorBrowsable( EditorBrowsableState.Never )]
	internal static class PullRequestFetchResults_Extensions {

		internal static PullRequestFetchResults Merge( this IEnumerable<PullRequestFetchResults> results ) {
			IEnumerable<PullRequestInfo> pullRequests = Enumerable.Empty<PullRequestInfo>();
			IEnumerable<SourceControlIntegrationException> errors = Enumerable.Empty<SourceControlIntegrationException>();
			IEnumerable<string> warnings = Enumerable.Empty<string>();

			foreach( PullRequestFetchResults result in results ) {
				pullRequests = pullRequests.Concat( result.PullRequests );
				errors = errors.Concat( result.Errors );
				warnings = warnings.Concat( result.Warnings );
			}

			return new PullRequestFetchResults{
				PullRequests = pullRequests.ToArray(),
				Errors = errors.ToArray(),
				Warnings = warnings.Distinct().ToArray()
			};
		}

	}

}
