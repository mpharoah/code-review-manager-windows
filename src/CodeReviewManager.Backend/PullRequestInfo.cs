﻿using System;
using Newtonsoft.Json;

namespace CodeReviewManager.Backend {

	[JsonObject( MemberSerialization.OptOut )]
	internal sealed class PullRequestInfo {

		[JsonConstructor]
		public PullRequestInfo(
			[JsonProperty( PropertyName = nameof( Source ) )] IntegrationType source,
			[JsonProperty( PropertyName = nameof( IntegrationId ) )] Guid integrationId,
			[JsonProperty( PropertyName = nameof( Url ) )] Uri url,
			[JsonProperty( PropertyName = nameof( Repository ) )] string repositoryName,
			[JsonProperty( PropertyName = nameof( AuthorName ) )] string authorName,
			[JsonProperty( PropertyName = nameof( Name ) )] string pullRequestName,
			[JsonProperty( PropertyName = nameof( Opened ) )] DateTime openedTimestamp,
			[JsonProperty( PropertyName = nameof( LastUpdated ) )] DateTime updatedTimestamp,
			[JsonProperty( PropertyName = nameof( LastPush ) )] DateTime lastPushTimestamp,
			[JsonProperty( PropertyName = nameof( LastAuthorComment ) )] DateTime? lastAuthorCommentTimestamp,
			[JsonProperty( PropertyName = nameof( LastReviewerComment ) )] DateTime? lastReviewerCommentTimestamp,
			[JsonProperty( PropertyName = nameof( LastReviewerMention ) )] DateTime? lastReviewerMentionTimestamp,
			[JsonProperty( PropertyName = nameof( LastReview ) )] DateTime? lastReviewTimestamp,
			[JsonProperty( PropertyName = nameof( SelfAssigned) )] bool selfAssigned,
			[JsonProperty( PropertyName = nameof( AvatarUrl ) )] Uri avatarUrl = null
		) {
			Source = source;
			IntegrationId = integrationId;
			Url = url;
			Repository = repositoryName;
			AuthorName = authorName;
			Name = pullRequestName;
			Opened = openedTimestamp;
			LastUpdated = updatedTimestamp;
			LastPush = lastPushTimestamp;
			LastAuthorComment = lastAuthorCommentTimestamp;
			LastReviewerComment = lastReviewerCommentTimestamp;
			LastReviewerMention = lastReviewerMentionTimestamp;
			LastReview = lastReviewTimestamp;
			SelfAssigned = selfAssigned;
			AvatarUrl = avatarUrl;
		}

		public IntegrationType Source { get; }
		public Guid IntegrationId { get; }
		public Uri Url { get; }
		public string Repository { get; }
		public string AuthorName { get; }
		public string Name { get; }
		public DateTime Opened { get; }
		public DateTime LastUpdated { get; }
		public DateTime LastPush { get; internal set; }
		public DateTime? LastAuthorComment { get; }
		public DateTime? LastReviewerComment { get; }
		public DateTime? LastReviewerMention { get; }
		public DateTime? LastReview { get; }
		public bool SelfAssigned { get; }
		public Uri AvatarUrl { get; }

		[JsonIgnore]
		internal string Key => this.Url.AbsoluteUri;

	}

}
