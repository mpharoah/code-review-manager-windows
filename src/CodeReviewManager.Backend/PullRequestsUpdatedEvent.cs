﻿using System.Collections.Generic;

namespace CodeReviewManager.Backend {

	public sealed class PullRequestsUpdatedEvent {

		internal PullRequestsUpdatedEvent(
			IReadOnlyList<IPullRequest> removedPullRequests,
			IReadOnlyList<IPullRequest> updatedPullRequests,
			IReadOnlyList<IPullRequest> newPullRequests,
			IReadOnlyList<IPullRequest> erroredPullRequests,
			IReadOnlyList<string> warnings,
			bool completedWithErrors
		) {
			RemovedPullRequests = removedPullRequests;
			UpdatedPullRequests = updatedPullRequests;
			NewPullRequests = newPullRequests;
			ErroredPullRequests = erroredPullRequests;
			Warnings = warnings;
			CompletedWithErrors = completedWithErrors;
		}

		public IReadOnlyList<IPullRequest> RemovedPullRequests { get; }
		public IReadOnlyList<IPullRequest> UpdatedPullRequests { get; }
		public IReadOnlyList<IPullRequest> NewPullRequests { get; }
		public IReadOnlyList<IPullRequest> ErroredPullRequests { get; }

		public bool CompletedWithErrors { get; }
		public IReadOnlyList<string> Warnings { get; }

	}

}
