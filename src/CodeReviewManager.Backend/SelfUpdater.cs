﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;
using Newtonsoft.Json.Linq;
using Version = CodeReviewManager.Util.Version;

namespace CodeReviewManager.Backend {

	public static class SelfUpdater {

		private const string REPO_URL = "https://gitlab.com/mpharoah/code-review-manager-windows";
		private const string PROJECT_ID = "7278613";

		private static readonly Regex AttachmentRegex = new Regex(
			@"\[.*\]\((\/uploads\/.+\/CodeReviewManager\.msi)\)",
			RegexOptions.IgnoreCase | RegexOptions.Compiled
		);

		public static async Task<Uri> CheckForUpdatesAsync() {
			try {
				using( HttpClient httpClient = new HttpClient() ) {
					SimpleResponse response = await httpClient.SimpleSendAsync(
						HttpMethod.Get,
						new Uri( $"https://gitlab.com/api/v4/projects/{PROJECT_ID}/repository/tags?order_by=updated&sort=desc" ),
						ensureSuccess: true
					).SafeAsync();

					JArray tags = JArray.Parse( response.ResponseBody );
					foreach( JToken tagInfo in tags ) {
						if( !((JObject)tagInfo).TryGetValue( "release", out JToken release ) ) {
							continue;
						}

						string releaseName = (string)release["tag_name"] ?? string.Empty;
						if( !Version.TryParse( releaseName, out Version version ) ) {
							continue;
						}

						if( version <= Version.Current ) {
							return null;
						}

						string releaseDescription = (string)release["description"] ?? string.Empty;
						Match match = AttachmentRegex.Match( releaseDescription );
						if( match.Success && match.Groups.Count > 1 ) {
							return new Uri( string.Concat( REPO_URL, match.Groups[1].Value ) );
						}
					}

					return null;
				}
			} catch( Exception exception ) {
				Log.WriteException( exception, "Update Check Failed" );
				return null;
			}
		}

		public static async Task<Process> InstallUpdateAsync( Uri installerUri ) {
			string downloadLocation = Path.Combine(
				Path.GetTempPath(),
				"CodeReviewManager.msi"
			);

			try {
				using( HttpClient httpClient = new HttpClient() ) {
					httpClient.DefaultRequestHeaders.Accept.Clear();
					httpClient.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue( "application/octet-stream" ) );

					using( HttpResponseMessage download = await httpClient.GetAsync( installerUri ).SafeAsync() ) {
						download.EnsureSuccessStatusCode();

						using( FileStream tempFile = File.OpenWrite( downloadLocation ) ) {
							await download.Content.CopyToAsync( tempFile ).SafeAsync();
						}
					}

					return Process.Start( downloadLocation );
				}
			} catch( Exception exception ) {
				Log.WriteException( exception, "Failed to download and install app update." );
				return null;
			}
		}

	}

}
