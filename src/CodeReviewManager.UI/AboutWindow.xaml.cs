﻿using System.Windows;
using CodeReviewManager.Util;

namespace CodeReviewManager.UI {

	/// <summary>
	/// Interaction logic for AboutWindow.xaml
	/// </summary>
	public partial class AboutWindow : Window {

		public AboutWindow() {
			InitializeComponent();

			this.VersionLabel.Content = $"Version {AppInfo.Version}";
		}

	}

}
