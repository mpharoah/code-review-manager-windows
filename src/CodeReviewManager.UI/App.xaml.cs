﻿using System;
using System.Threading;
using System.Windows;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Config;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;

namespace CodeReviewManager.UI {

	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application {

		private Mutex m_appLock;
		private MainWindow m_mainWindow = null;
		private AboutWindow m_aboutWindow = null;

		private void Application_Exit( object sender, ExitEventArgs e ) {
			CodeReviewManagerCore.Instance.SavePullRequestsAsync().WaitSync();
		}

		private void Application_SessionEnding( object sender, SessionEndingCancelEventArgs e ) {
			CodeReviewManagerCore.Instance.SavePullRequestsAsync().WaitSync();
		}

		private void Application_Startup( object sender, StartupEventArgs e ) {
			m_appLock = new Mutex( false, $"CodeReviewManager_{Environment.UserName}" );
			if( !m_appLock.WaitOne( TimeSpan.Zero, exitContext: false ) ) {
				Console.Out.WriteLine( "Code Review Manager is already running." );
				Console.Out.Flush();
				this.Shutdown();
			}

			AppDomain.CurrentDomain.UnhandledException += ( _source, args ) => {
				Log.WriteException( args.ExceptionObject as Exception, "Unhandled Exception!" );
			};

			m_mainWindow = new MainWindow();

			CodeReviewManagerCore.Instance.OnConfigUpdated += @event => Dispatcher.Invoke( () => {
				if( @event.OldConfig.DockedToTray != @event.NewConfig.DockedToTray ) {
					m_mainWindow?.Dispose();

					m_mainWindow = new MainWindow();
					m_mainWindow.ShowSettingsTab();
					m_mainWindow.ShowWindow();
				}
			});

			ConfigData config = CodeReviewManagerCore.Instance.GetSettings();
			if( !AppInfo.Version.Equals( config.Version ) ) {
				ShowAboutWindow();
				config.Version = AppInfo.Version;
				CodeReviewManagerCore.Instance.UpdateSettingsAsync( config ).SafeDetach();
			}
		}

		internal void ShowAboutWindow() {
			m_aboutWindow?.Close();
			m_aboutWindow = new AboutWindow();
			m_aboutWindow.Show();
		}

	}

}
