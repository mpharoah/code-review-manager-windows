﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Integrations;

namespace CodeReviewManager.UI.Components {

	/// <summary>
	/// Interaction logic for Integration.xaml
	/// </summary>
	public partial class Integration : UserControl {

		private readonly ISourceControlIntegrationInfo m_integration;
		private readonly ISourceControlSignInInfo m_signIn;
		private readonly Action<ISourceControlIntegrationInfo, ISourceControlSignInInfo> m_deleteAction;

		public Integration(
			ISourceControlIntegrationInfo integration,
			ISourceControlSignInInfo signIn,
			Action<ISourceControlIntegrationInfo, ISourceControlSignInInfo> onDeleteClicked
		) {
			m_integration = integration;
			m_signIn = signIn;
			m_deleteAction = onDeleteClicked;
			InitializeComponent();
			this.DataContext = this;
			this.Icon.Type = integration.Type;
		}

		public string SourceLabel => m_integration.Name;
		public IntegrationType SourceType => m_integration.Type;

		public string LocationInfo => m_signIn.LocationInfo ?? string.Empty;
		public string Username => m_signIn.Username ?? "Unknown";

		public string StatusLabel => m_signIn.Errored ? "Auth Error" : "Signed In";

		public Brush StatusColor => m_signIn.Errored ? Palette.Red : Palette.Blue;

		private void Button_Click( object sender, RoutedEventArgs e ) {
			m_deleteAction?.Invoke( m_integration, m_signIn );
		}
	}

}
