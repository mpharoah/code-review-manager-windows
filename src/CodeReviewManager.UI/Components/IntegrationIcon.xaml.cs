﻿using System.Windows;
using System.Windows.Controls;
using CodeReviewManager.Backend;
using CodeReviewManager.UI.Components.Vector;

namespace CodeReviewManager.UI.Components {

	/// <summary>
	/// Interaction logic for IntegrationIcon.xaml
	/// </summary>
	public partial class IntegrationIcon : UserControl {

		public IntegrationIcon() {
			InitializeComponent();
		}

		public IntegrationType Type {
			get => (IntegrationType)GetValue( TypeProperty );
			set => SetValue( TypeProperty, value );
		}

		public static readonly DependencyProperty TypeProperty = DependencyProperty.Register(
			name: "Type",
			propertyType: typeof( IntegrationType ),
			ownerType: typeof( IntegrationIcon ),
			typeMetadata: new PropertyMetadata(
				defaultValue: (IntegrationType)(-1),
				propertyChangedCallback: OnTypeChanged
			)
		);

		private static void OnTypeChanged( DependencyObject instance, DependencyPropertyChangedEventArgs args ) {
			IntegrationIcon @this = (IntegrationIcon)instance;
			switch( (IntegrationType)args.NewValue ) {
				case IntegrationType.GitHub: @this.Icon.Child = new GitHubIcon(); break;
				case IntegrationType.BitBucketServer: @this.Icon.Child = new BitBucketIcon(); break;
				case IntegrationType.BitBucketCloud: @this.Icon.Child = new BitBucketCloudIcon(); break;
				default: @this.Icon.Child = null; break;
			}
		}

	}

}
