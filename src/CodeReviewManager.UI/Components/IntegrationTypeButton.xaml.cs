﻿using System;
using System.Windows;
using System.Windows.Controls;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Integrations;

namespace CodeReviewManager.UI.Components {
	/// <summary>
	/// Interaction logic for IntegrationTypeButton.xaml
	/// </summary>
	public partial class IntegrationTypeButton : UserControl {

		private readonly ISourceControlIntegrationInfo m_integration;

		public IntegrationTypeButton( ISourceControlIntegrationInfo integration ) {
			m_integration = integration;
			InitializeComponent();
			this.DataContext = this;
		}

		public event Action<ISourceControlIntegrationInfo> OnPress;

		public IntegrationType IntegrationType => m_integration.Type;
		public string IntegrationName => m_integration.Name;

		private void Button_Click( object sender, RoutedEventArgs e ) {
			this.OnPress?.Invoke( m_integration );
		}

	}
}
