﻿using System.Windows;
using System.Windows.Controls;
using CodeReviewManager.Backend;
using CodeReviewManager.UI.Components.Vector;

namespace CodeReviewManager.UI.Components {

	/// <summary>
	/// Interaction logic for NotificationIcon.xaml
	/// </summary>
	public partial class NotificationIcon : UserControl {

		public NotificationIcon() {
			InitializeComponent();
		}

		public NotificationStatus Status {
			get => (NotificationStatus)GetValue( StatusProperty );
			set => SetValue( StatusProperty, value );
		}

		public static readonly DependencyProperty StatusProperty = DependencyProperty.Register(
			name: "Status",
			propertyType: typeof( NotificationStatus ),
			ownerType: typeof( NotificationIcon ),
			typeMetadata: new PropertyMetadata(
				defaultValue: (NotificationStatus)(0xff),
				propertyChangedCallback: OnStatusChanged
			)
		);

		private static void OnStatusChanged( DependencyObject instance, DependencyPropertyChangedEventArgs args ) {
			NotificationIcon @this = (NotificationIcon)instance;
			switch( (NotificationStatus)args.NewValue ) {
				case NotificationStatus.On: @this.Icon.Child = new NotificationsOn(); break;
				case NotificationStatus.AutomaticallyMuted: @this.Icon.Child = new NotificationsAuto(); break;
				case NotificationStatus.ManuallyMuted: @this.Icon.Child = new NotificationsOff(); break;
				case NotificationStatus.Snooze: @this.Icon.Child = new NotificationsSnooze(); break;
				default: @this.Icon.Child = null; break;
			}
		}

	}

}
