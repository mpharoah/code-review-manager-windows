﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace CodeReviewManager.UI.Components {

	public class NumberBox : TextBox {

		private static readonly Regex VALUE_REGEX = new Regex( @"^\d+$", RegexOptions.Compiled );

		public NumberBox() : base() {
			this.PreviewTextInput += OnPreviewTextInput;
			this.LostFocus += OnBlur;
			DataObject.AddPastingHandler( this, OnPasteText );
		}

		public uint Value {
			get => (uint)GetValue( ValueProperty );
			set => SetValue( ValueProperty, value );
		}

		public uint MinValue {
			get => (uint)GetValue( MinValueProperty );
			set => SetValue( MinValueProperty, value );
		}

		public uint MaxValue {
			get => (uint)GetValue( MaxValueProperty );
			set => SetValue( MaxValueProperty, value );
		}

		private static void OnBlur( object sender, RoutedEventArgs args ) {
			NumberBox @this = (NumberBox)sender;

			if(
				!VALUE_REGEX.IsMatch( @this.Text ) ||
				!uint.TryParse( @this.Text, out uint newValue )
			) {
				@this.Text = @this.Value.ToString();
				return;
			}

			if( newValue < @this.MinValue ) {
				newValue = @this.MinValue;
			} else if( newValue > @this.MaxValue ) {
				newValue = @this.MaxValue;
			}

			@this.Text = newValue.ToString();
			@this.Value = newValue;
		}

		private static void OnPreviewTextInput( object sender, TextCompositionEventArgs args ) {
			if( !string.IsNullOrEmpty( args.Text ) && !VALUE_REGEX.IsMatch( args.Text ) ) {
				args.Handled = true;
			}
		}

		private static void OnPasteText( object sender, DataObjectPastingEventArgs args) {
			// Don't event want to deal with validating this
			args.CancelCommand();
		}

		public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
			name: "Value",
			propertyType: typeof( uint ),
			ownerType: typeof( NumberBox ),
			typeMetadata: new FrameworkPropertyMetadata(
				defaultValue: (uint)0,
				propertyChangedCallback: OnValueChanged
			) {
				BindsTwoWayByDefault = true,
				DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
			}
		);

		public static readonly DependencyProperty MinValueProperty = DependencyProperty.Register(
			name: "MinValue",
			propertyType: typeof( uint ),
			ownerType: typeof( NumberBox ),
			typeMetadata: new PropertyMetadata(
				defaultValue: (uint)0,
				propertyChangedCallback: OnMinValueChanged
			)
		);

		public static readonly DependencyProperty MaxValueProperty = DependencyProperty.Register(
			name: "MaxValue",
			propertyType: typeof( uint ),
			ownerType: typeof( NumberBox ),
			typeMetadata: new PropertyMetadata(
				defaultValue: uint.MaxValue,
				propertyChangedCallback: OnMaxValueChanged
			)
		);

		private static void OnValueChanged( DependencyObject instance, DependencyPropertyChangedEventArgs args ) {
			NumberBox @this = (NumberBox)instance;
			@this.Text = ((uint)args.NewValue).ToString();
		}

		private static void OnMinValueChanged( DependencyObject instance, DependencyPropertyChangedEventArgs args ) {
			if( (uint)instance.GetValue( ValueProperty ) < (uint)args.NewValue ) {
				instance.SetValue( ValueProperty, args.NewValue );
			}
		}

		private static void OnMaxValueChanged( DependencyObject instance, DependencyPropertyChangedEventArgs args ) {
			if( (uint)instance.GetValue( ValueProperty ) > (uint)args.NewValue ) {
				instance.SetValue( ValueProperty, args.NewValue );
			}
		}

	}

}
