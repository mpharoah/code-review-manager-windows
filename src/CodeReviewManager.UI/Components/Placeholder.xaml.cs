﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CodeReviewManager.UI.Components {

    /// <summary>
    /// Interaction logic for ProfilePlaceholder.xaml
    /// </summary>
    public partial class Placeholder : UserControl {

        public Placeholder() {
			Text = string.Empty;
            InitializeComponent();
			this.DataContext = this;
        }

		public Placeholder( string text ) {
			Text = text;
			InitializeComponent();
			this.DataContext = this;
		}

		public string Text { get; set; }

		public event Action<object, RoutedEventArgs> Click;

		private void Viewbox_MouseLeftButtonDown( object sender, MouseButtonEventArgs e ) {
			this.Click?.Invoke( sender, e );
		}

		private void Viewbox_KeyDown( object sender, KeyEventArgs e ) {
			if( e.Key == Key.Enter ) {
				this.Click?.Invoke( sender, e );
			}
		}

	}

}
