﻿using System;
using System.Windows;
using System.Windows.Controls;
using CodeReviewManager.Backend.Profiles;

namespace CodeReviewManager.UI.Components {

    /// <summary>
    /// Interaction logic for Profile.xaml
    /// </summary>
    public partial class Profile : UserControl {

		private readonly IProfile m_profile;

		public Profile( IProfile profile ) {
			InitializeComponent();
			m_profile = profile;
			Refresh();
		}

		public event Action<IProfile> OnClickRename;
		public event Action<IProfile> OnClickDelete;

		public void Refresh() {
			this.MainButton.Content = m_profile.Name;
			this.MainButton.IsChecked = m_profile.IsActive;
		}

		private void MainButton_Click( object sender, RoutedEventArgs e ) {
			if( m_profile.IsActive ) {
				this.MainButton.IsChecked = true;
				return;
			}

			ProfileManager.SwitchTo( m_profile );
		}

		private void RenameButton_Click( object sender, RoutedEventArgs e ) {
			OnClickRename?.Invoke( m_profile );
			Refresh();
		}

		private void DeleteButton_Click( object sender, RoutedEventArgs e ) {
			OnClickDelete?.Invoke( m_profile );
		}

	}

}
