﻿using System;
using System.Windows;
using System.Windows.Controls;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Config;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CodeReviewManager.UI.Util;
using NotificationStatus = CodeReviewManager.Backend.NotificationStatus;

namespace CodeReviewManager.UI.Components {

	/// <summary>
	/// Interaction logic for PullRequestComponent.xaml
	/// </summary>
	public partial class PullRequestComponent : UserControl {

		private static readonly Brush RedBackground = new SolidColorBrush( Color.FromRgb( 0x76, 0x08, 0x08 ) );
		private readonly IPullRequest m_pullRequest;

		public PullRequestComponent( IPullRequest pullRequest ) {
			m_pullRequest = pullRequest;
			InitializeComponent();

			ConfigData settings = CodeReviewManagerCore.Instance.GetSettings();

			this.SourceIcon.Type = pullRequest.Source;
			if( settings.ShowAvatars && pullRequest.AuthorAvatarUrl != null ) {
				this.Avatar.Fill = new ImageBrush{
					ImageSource = new BitmapImage( pullRequest.AuthorAvatarUrl ),
					Stretch = Stretch.UniformToFill
				};
			} else {
				this.Avatar.Visibility = Visibility.Hidden;
				this.SourceIconBackdrop.Visibility = Visibility.Hidden;
				this.SourceIcon.SetValue( Grid.RowProperty, 0 );
				this.SourceIcon.SetValue( Grid.ColumnProperty, 0 );
				this.SourceIcon.SetValue( Grid.RowSpanProperty, 2 );
				this.SourceIcon.SetValue( Grid.ColumnSpanProperty, 2 );
			}
			this.RepoName.Text = pullRequest.Repository;
			this.AuthorName.Content = pullRequest.AuthorName;
			this.Title.Text = pullRequest.Name;
			StatusChanged();

			if( pullRequest.Errored ) {
				this.Box.Background = RedBackground;
			}

			switch( settings.OrderBy ) {
				case ConfigData.SortOrder.OpenDate:
					this.Timestamp.Content = $"Opened: {FormatDate( m_pullRequest.Opened )}";
					break;
				case ConfigData.SortOrder.LastUpdated:
					this.Timestamp.Content = $"Last Updated: {FormatDate( m_pullRequest.LastUpdated )}";
					break;
				case ConfigData.SortOrder.LastAuthorComment:
					this.Timestamp.Content = $"Last Author Comment: {FormatDate( m_pullRequest.LastAuthorComment )}";
					break;
				case ConfigData.SortOrder.LastReviewerComment:
					this.Timestamp.Content = $"My Last Comment: {FormatDate( m_pullRequest.LastReviewerComment )}";
					break;
				case ConfigData.SortOrder.LastPush:
					this.Timestamp.Content = $"Last Push: { FormatDate( m_pullRequest.LastPushed )}";
					break;
			}
		}

		private void StatusChanged() {
			this.NotificationIcon.Status = m_pullRequest.NotificationStatus;
			this.MuteUnmuteMenuItem.Header = m_pullRequest.Muted ? "Un_mute" : "_Mute";
			this.AutoMuteMenuItem.Visibility = (
				m_pullRequest.NotificationStatus == NotificationStatus.AutomaticallyMuted ||
				m_pullRequest.NotificationStatus == NotificationStatus.Snooze
			) ? Visibility.Collapsed : Visibility.Visible;
		}

		internal Uri Url => m_pullRequest.Url;
		internal NotificationStatus NotificationStatus => m_pullRequest.NotificationStatus;

		private static string FormatDate( DateTime timestamp ) {
			DateTime localTime = timestamp.ToLocalTime();
			return string.Concat(
				localTime.ToString( "yyyy-MM-dd" ),
				" at ",
				localTime.ToString( "h\\:mm\\:ss tt" )
			);
		}

		private void NotificationButton_Click( object sender, MouseButtonEventArgs e ) {
			if( e.ChangedButton == MouseButton.Middle ) {
				m_pullRequest.Snooze();

				StatusChanged();
				e.Handled = true;
			} else if( e.ChangedButton == MouseButton.Left ) {
				MenuMuteUnmute_Clicked( sender, e );
				e.Handled = true;
			}
		}

		private void PullRequest_Click( object sender, MouseButtonEventArgs e ) {
			if( e.ChangedButton == MouseButton.Left ) {
				DefaultWebBrowser.Open( m_pullRequest.Url );
			}
		}

		private void MenuOpen_Clicked( object sender, RoutedEventArgs e ) {
			DefaultWebBrowser.Open( m_pullRequest.Url );
		}

		private void MenuMuteUnmute_Clicked( object sender, RoutedEventArgs e ) {
			if( m_pullRequest.Muted ) {
				m_pullRequest.Unmute();
			} else {
				m_pullRequest.Mute();
			}

			StatusChanged();
		}

		private void MenuAutoMute_Clicked( object sender, RoutedEventArgs e ) {
			m_pullRequest.Mute( automatic: true );
			StatusChanged();
		}

		private void MenuSnooze_Clicked( object sender, RoutedEventArgs e ) {
			m_pullRequest.Snooze();
			StatusChanged();
		}

	}

}
