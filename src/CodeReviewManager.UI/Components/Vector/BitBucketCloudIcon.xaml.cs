﻿using System.Windows.Controls;

namespace CodeReviewManager.UI.Components.Vector {
	/// <summary>
	/// Interaction logic for BitBucketCloudIcon.xaml
	/// </summary>
	public partial class BitBucketCloudIcon : UserControl {
		public BitBucketCloudIcon() {
			InitializeComponent();
		}
	}
}
