﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Media;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Config;
using CodeReviewManager.UI.Util;
using CodeReviewManager.Util;

namespace CodeReviewManager.UI {

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, IDisposable {

		private Uri m_updateUrl = null;
		private readonly Timer m_appUpdateCheckTimer;
		private bool m_disposing = false;

		public MainWindow() {
			InitializeComponent();

			ConfigData config = CodeReviewManagerCore.Instance.GetSettings();

			this.ShowInTaskbar = !config.DockedToTray;
			this.WindowStyle = config.DockedToTray ? WindowStyle.None : WindowStyle.SingleBorderWindow;
			this.AllowsTransparency = config.DockedToTray;

			this.PullRequestsView.ConfigureIntegrationsButton.Click += ConfigureIntegrationsButton_Clicked;
			this.PullRequestsView.OnNewPullRequestsAdded += newPrs => {
				if( config.NotifyOnNewPullRequest ) {
					string s = newPrs.Count == 1 ? string.Empty : "s";

					this.TrayIcon.ShowBalloonTip(
						title: $"New Pull Request{s}",
						message: $"You have been added as a reviewer to {newPrs.Count} new pull request{s}.",
						customIcon: Properties.Resources.WindowIcon,
						largeIcon: true
					);
				}
			};

			CodeReviewManagerCore.Instance.OnReminderAlert += OnReminderAlert;
			CodeReviewManagerCore.Instance.OnPullRequestMuted += OnPullRequestMuted;
			CodeReviewManagerCore.Instance.OnPullRequestUnmuted += OnPullRequestUnmuted;
			CodeReviewManagerCore.Instance.UpdateAsync().SafeDetach();
			CheckForAppUpdates();

			m_appUpdateCheckTimer = new Timer{
				AutoReset = true,
				Enabled = true,
				Interval = TimeSpan.FromHours( 3.0 ).TotalMilliseconds
			};
			m_appUpdateCheckTimer.Elapsed += ( sender, args ) => Application.Current.Dispatcher.Invoke( CheckForAppUpdates );

			this.TrayIcon.Icon = CodeReviewManagerCore.Instance.GetPullRequests().All( pr => pr.Muted ) ?
				Properties.Resources.TrayIconNormal :
				Properties.Resources.TrayIconAlert;

			this.Hide();
		}

		public void Dispose() {
			if( !m_disposing ) {
				m_disposing = true;
				this.Close();
			}
		}

		public void ShowWindow() {

			ConfigData config = CodeReviewManagerCore.Instance.GetSettings();

			this.Width = config.WidthPixels;
			this.Height = config.HeightPixels;

			PositionWindow( config );

			if( config.DockedToTray && Taskbar.UsesAccentColour ) {
				this.Border.Background = new SolidColorBrush( Taskbar.GetTransparentAccentColour() );
			} else {
				this.Border.Background = Palette.TransparentBlack;
			}

			this.WindowState = WindowState.Normal;
			this.Show();
			this.Activate();

			if( config.RefreshOnOpen ) {
				CodeReviewManagerCore.Instance.UpdateAsync().SafeDetach();
			}
		}

		private void PositionWindow( ConfigData config ) {

			if( config.DockedToTray ) {
				int leftSide = (int)SystemParameters.WorkArea.Left;
				int topSide = (int)SystemParameters.WorkArea.Top;
				int rightSide = (int)SystemParameters.WorkArea.Right;
				int bottomSide = (int)SystemParameters.WorkArea.Bottom;

				switch( Taskbar.GetLocation() ) {
					case Taskbar.Side.Top:
						this.Left = rightSide - config.WidthPixels;
						this.Top = topSide;
						this.Border.BorderThickness = new Thickness( 1, 0, 0, 1 );
						break;
					case Taskbar.Side.Left:
						this.Left = leftSide;
						this.Top = bottomSide - config.HeightPixels;
						this.Border.BorderThickness = new Thickness( 0, 1, 1, 0 );
						break;
					case Taskbar.Side.Bottom:
					case Taskbar.Side.Right:
					default:
						this.Left = rightSide - config.WidthPixels;
						this.Top = bottomSide - config.HeightPixels;
						this.Border.BorderThickness = new Thickness( 1, 1, 0, 0 );
						break;
				}
			} else {
				this.Border.BorderThickness = new Thickness( 1, 1, 1, 1 );
			}
		}

		private void Window_Loaded( object sender, RoutedEventArgs e ) {
			Compositor.SetBlurEffectEnabled( this, CodeReviewManagerCore.Instance.GetSettings().DockedToTray );
		}

		private void Window_Deactivated( object sender, EventArgs e ) {
			MaybeSaveConfig();
			if( CodeReviewManagerCore.Instance.GetSettings().DockedToTray ) {
				this.Hide();
			}
		}

		private void IntegrationsTab_Checked( object sender, RoutedEventArgs e ) {
			if( !this.IsInitialized ) return;
			MaybeSaveConfig();
			this.PullRequestsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Collapsed;
			this.IntegrationsView.Visibility = Visibility.Visible;
			this.ProfilesView.Visibility = Visibility.Collapsed;
		}

		private void PullRequestsTab_Checked( object sender, RoutedEventArgs e ) {
			if( !this.IsInitialized ) return;
			MaybeSaveConfig();
			this.IntegrationsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Collapsed;
			this.PullRequestsView.Visibility = Visibility.Visible;
			this.ProfilesView.Visibility = Visibility.Collapsed;
		}

		private void SettingsTab_Checked( object sender, RoutedEventArgs e ) {
			if( !this.IsInitialized ) return;
			MaybeSaveConfig();
			this.IntegrationsView.Visibility = Visibility.Collapsed;
			this.PullRequestsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Visible;
			this.ProfilesView.Visibility = Visibility.Collapsed;
		}

		private void ProfilesTab_Checked( object sender, RoutedEventArgs e ) {
			if( !this.IsInitialized ) return;
			MaybeSaveConfig();
			this.IntegrationsView.Visibility = Visibility.Collapsed;
			this.PullRequestsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Collapsed;
			this.ProfilesView.Visibility = Visibility.Visible;
		}

		private void ConfigureIntegrationsButton_Clicked( object sender, RoutedEventArgs e ) {
			ShowIntegrationsTab();
		}

		public void ShowIntegrationsTab() {
			MaybeSaveConfig();
			this.IntegrationsTab.IsChecked = true;
			this.PullRequestsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Collapsed;
			this.IntegrationsView.Visibility = Visibility.Visible;
			this.ProfilesView.Visibility = Visibility.Collapsed;
		}

		public void ShowPullRequestsTab() {
			MaybeSaveConfig();
			this.PullRequestsTab.IsChecked = true;
			this.IntegrationsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Collapsed;
			this.PullRequestsView.Visibility = Visibility.Visible;
			this.ProfilesView.Visibility = Visibility.Collapsed;
		}

		public void ShowSettingsTab() {
			this.SettingsTab.IsChecked = true;
			this.IntegrationsView.Visibility = Visibility.Collapsed;
			this.PullRequestsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Visible;
			this.ProfilesView.Visibility = Visibility.Collapsed;
		}

		public void ShowProfilesTab() {
			MaybeSaveConfig();
			this.ProfilesTab.IsChecked = true;
			this.IntegrationsView.Visibility = Visibility.Collapsed;
			this.PullRequestsView.Visibility = Visibility.Collapsed;
			this.SettingsView.Visibility = Visibility.Collapsed;
			this.ProfilesView.Visibility = Visibility.Visible;
		}

		private void MaybeSaveConfig() {
			if( this.SettingsView.Visibility == Visibility.Visible ) {
				this.SettingsView.CommitChangesAsync().SafeDetach();
			}
		}

		private void TrayIcon_Clicked( object sender, RoutedEventArgs e ) {
			ShowWindow();
		}

		private void NotificationClicked( object sender, RoutedEventArgs e ) {
			ShowPullRequestsTab();
			ShowWindow();
		}

		private void Quit( object sender, RoutedEventArgs e ) {
			m_appUpdateCheckTimer?.Dispose();
			Application.Current.Shutdown();
		}

		private void ViewSource( object sender, RoutedEventArgs e ) {
			DefaultWebBrowser.Open( "https://gitlab.com/mpharoah/code-review-manager-windows" );
		}

		private void CheckForAppUpdates() {
			SelfUpdater.CheckForUpdatesAsync().ThenDispatch( updateUrl => {
				if( updateUrl != null ) {
					m_updateUrl = updateUrl;
					this.UpdateAppMenuItem.Header = "Install _Update";

					this.TrayIcon.ShowBalloonTip(
						title: "App Update Available!",
						message: @"Install the update by right clicking on the icon in the system tray and selecting 'Install Update'",
						customIcon: Properties.Resources.WindowIcon,
						largeIcon: true
					);
				}
			} );
		}

		private void AppUpdate( object sender, RoutedEventArgs e ) {
			if( m_updateUrl != null ) {
				SelfUpdater.InstallUpdateAsync( m_updateUrl ).ThenDispatch( Application.Current.Shutdown );
			} else {
				Application.Current.Dispatcher.Invoke( CheckForAppUpdates );
			}
		}

		private void ShowAboutWindow( object sender, RoutedEventArgs e ) {
			( Application.Current as App )?.ShowAboutWindow();
		}

		private void OnReminderAlert( IReadOnlyList<IPullRequest> pullRequests ) {
			Application.Current.Dispatcher.Invoke( () => {
				string prs = pullRequests.Count == 1 ? "pull request" : "pull requests";
				string are = pullRequests.Count == 1 ? "is" : "are";
				string require = pullRequests.Count == 1 ? "requires" : "require";

				this.TrayIcon.ShowBalloonTip(
					title: "Reminder: Review me!",
					message: $"There {are} {pullRequests.Count} {prs} that {require} your attention.",
					customIcon: Properties.Resources.WindowIcon,
					largeIcon: true
				);
			});
		}

		private void OnPullRequestUnmuted() {
			Application.Current.Dispatcher.Invoke( () => {
				this.TrayIcon.Icon = this.IsActive ? Properties.Resources.TrayIconInfo : Properties.Resources.TrayIconAlert;
			});
		}

		private void OnPullRequestMuted() {
			Application.Current.Dispatcher.Invoke( () => {
				if( CodeReviewManagerCore.Instance.GetPullRequests().All( pr => pr.Muted ) ) {
					this.TrayIcon.Icon = Properties.Resources.TrayIconNormal;
				}
			});
		}

		private void Window_Closing( object sender, System.ComponentModel.CancelEventArgs e ) {
			if( m_disposing ) {
				this.TrayIcon?.Dispose();
				this.PullRequestsView?.Dispose();
				m_appUpdateCheckTimer?.Dispose();
				CodeReviewManagerCore.Instance.OnReminderAlert -= OnReminderAlert;
				CodeReviewManagerCore.Instance.OnPullRequestMuted -= OnPullRequestMuted;
				CodeReviewManagerCore.Instance.OnPullRequestUnmuted -= OnPullRequestUnmuted;
			} else {
				e.Cancel = true;
				this.Hide();
			}
		}

		private void Window_Activated( object sender, EventArgs e ) {
			this.TrayIcon.Icon = CodeReviewManagerCore.Instance.GetPullRequests().All( pr => pr.Muted ) ?
				Properties.Resources.TrayIconNormal :
				Properties.Resources.TrayIconInfo;
		}

	}
}
