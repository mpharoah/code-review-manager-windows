﻿using System.Windows.Media;

namespace CodeReviewManager.UI {

	internal static class Palette {

		public static readonly Brush Blue = new SolidColorBrush( Color.FromRgb( 0x00, 0xaa, 0xde ) );
		public static readonly Brush Red = new SolidColorBrush( Color.FromRgb( 0xff, 0xca, 0xd5 ) );
		public static readonly Brush White = new SolidColorBrush( Color.FromRgb( 0xff, 0xff, 0xff ) );
		public static readonly Brush Grey = new SolidColorBrush( Color.FromRgb( 0xba, 0xba, 0xba ) );

		public static readonly Brush TransparentBlack = new SolidColorBrush( Color.FromArgb( 0xcc, 0x2a, 0x2a, 0x2a ) );

	}

}
