﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using CodeReviewManager.Util;

[assembly: AssemblyTitle( "CodeReviewManager" )]
[assembly: AssemblyProduct( "CodeReviewManager" )]
[assembly: AssemblyCopyright( "Copyright © Matt Pharoah 2017 - 2023" )]
[assembly: ComVisible( false )]
[assembly: AssemblyVersion( AppInfo.Version )]
[assembly: AssemblyFileVersion( AppInfo.Version )]


[assembly: ThemeInfo(
	ResourceDictionaryLocation.None,
	ResourceDictionaryLocation.SourceAssembly
)]
