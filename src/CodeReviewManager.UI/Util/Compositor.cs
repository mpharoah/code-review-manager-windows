﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace CodeReviewManager.UI.Util {

	internal static class Compositor {

		private enum AccentState {
			ACCENT_DISABLED = 0,
			ACCENT_ENABLE_GRADIENT = 1,
			ACCENT_ENABLE_TRANSPARENTGRADIENT = 2,
			ACCENT_ENABLE_BLURBEHIND = 3,
			ACCENT_INVALID_STATE = 4
		}

		[StructLayout( LayoutKind.Sequential )]
		private struct AccentPolicy {
			public AccentState AccentState;
			public int AccentFlags;
			public int GradientColor;
			public int AnimationId;
		}

		[StructLayout( LayoutKind.Sequential )]
		private struct WindowCompositionAttributeData {
			public WindowCompositionAttribute Attribute;
			public IntPtr Data;
			public int SizeOfData;
		}

		private enum WindowCompositionAttribute {
			WCA_ACCENT_POLICY = 19
			// Don't care about other values
		}

		[DllImport( "user32.dll" )]
		private static extern int SetWindowCompositionAttribute( IntPtr hwnd, ref WindowCompositionAttributeData data );

		public static void SetBlurEffectEnabled( Window window, bool blur ) {
			SetAccent( window, blur ? AccentState.ACCENT_ENABLE_BLURBEHIND : AccentState.ACCENT_DISABLED );
		}

		private static void SetAccent( Window window, AccentState accent ) {
			AccentPolicy accentPolicy = new AccentPolicy {
				AccentState = accent
			};

			int accentBytes = Marshal.SizeOf( accentPolicy );
			IntPtr accentPtr = Marshal.AllocHGlobal( accentBytes );
			try {
				Marshal.StructureToPtr( accentPolicy, accentPtr, fDeleteOld: false );

				var attributeData = new WindowCompositionAttributeData {
					Attribute = WindowCompositionAttribute.WCA_ACCENT_POLICY,
					SizeOfData = accentBytes,
					Data = accentPtr
				};

				SetWindowCompositionAttribute(
					new WindowInteropHelper( window ).Handle,
					ref attributeData
				);
			} finally {
				Marshal.FreeHGlobal( accentPtr );
			}
		}


	}

}
