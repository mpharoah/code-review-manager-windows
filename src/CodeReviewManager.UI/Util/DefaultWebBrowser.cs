﻿using System;
using CodeReviewManager.Util.Logging;

namespace CodeReviewManager.UI.Util {

	internal static class DefaultWebBrowser {

		public static void Open( string url ) {
			try {
				//TODO: Is there a less stupid way to do this? One that raises the browser's focus?
				System.Diagnostics.Process.Start( url );
			} catch( Exception exception ) {
				Log.WriteException( exception, "Failed to launch web browser" );
			}
		}

		public static void Open( Uri url ) {
			Open( url.AbsoluteUri );
		}

	}

}
