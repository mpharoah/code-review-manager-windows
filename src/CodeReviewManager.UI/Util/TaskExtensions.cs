﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;

namespace CodeReviewManager.UI.Util {

	[EditorBrowsable( EditorBrowsableState.Never )]
	internal static class TaskExtensions {

		public static async void ThenDispatch(
			this Task promise,
			Action then = null,
			Action<Exception> @catch = null,
			Action @finally = null
		) {
			try {
				await promise.SafeAsync();
				then?.SafeDispatch();
			} catch( Exception exception ) {
				if( @catch != null ) {
					@catch.SafeDispatch( exception );
				} else {
					Log.WriteException( exception, "Exception in detached thread" );
				}
			} finally {
				@finally?.SafeDispatch();
			}
		}

		public static async void ThenDispatch<T>(
			this Task<T> promise,
			Action<T> then = null,
			Action<Exception> @catch = null,
			Action @finally = null
		) {
			try {
				T result = await promise.SafeAsync();
				then?.SafeDispatch( result );
			} catch( Exception exception ) {
				if( @catch != null ) {
					@catch.SafeDispatch( exception );
				} else {
					Log.WriteException( exception, "Exception in detached thread" );
				}
			} finally {
				@finally?.SafeDispatch();
			}
		}

		private static void SafeDispatch( this Action action ) {
			try {
				Application.Current.Dispatcher.Invoke( action );
			} catch( Exception exception ) {
				Log.WriteException( exception, "Exception in callback to UI thread" );
			}
		}

		private static void SafeDispatch<T>( this Action<T> action, T arg ) {
			SafeDispatch( () => action( arg ) );
		}

	}

}
