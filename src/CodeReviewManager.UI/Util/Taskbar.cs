﻿using System;
using System.IO;
using System.Windows.Media;
using CodeReviewManager.Util.Logging;
using Microsoft.Win32;

namespace CodeReviewManager.UI.Util {

	public static class Taskbar {

		public static Side GetLocation() {
			try {
				byte[] data = (byte[])Registry.GetValue(
					keyName: @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\StuckRects3",
					valueName: "Settings",
					defaultValue: null
				);
				byte side = data[12];

				if( side > 0x03 ) {
					throw new InvalidDataException();
				}

				return (Side)side;
			} catch( Exception exception ) {
				Log.WriteException( exception, "Failed to get taskbar position. Assuming bottom." );
				return Side.Bottom;
			}
		}

		public static Color GetTransparentAccentColour() {
			try {
				byte[] data = (byte[])Registry.GetValue(
					keyName: @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Accent",
					valueName: "AccentPalette",
					defaultValue: null
				);

				return Color.FromArgb( 0xCC, data[20], data[21], data[22] );
			} catch( Exception exception ) {
				Log.WriteException( exception, "Failed to get accent colour. Assuming default blue." );
				return Color.FromArgb( 0xCC, 0x00, 0x42, 0x75 );
			}
		}

		public static bool UsesAccentColour {
			get {
				try {
					int data = (int)Registry.GetValue(
						keyName: @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Themes\Personalize",
						valueName: "ColorPrevalence",
						defaultValue: 0
					);

					return data == 1;
				} catch( Exception exception ) {
					Log.WriteException( exception, "Failed to get theme settings. Assuming default theme." );
					return false;
				}
			}

		}

		public enum Side : byte {
			Left = 0x00,
			Top = 0x01,
			Right = 0x02,
			Bottom = 0x03
		}

	}

}
