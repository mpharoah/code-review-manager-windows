﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Integrations;
using CodeReviewManager.Backend.Integrations.UISpec;
using CodeReviewManager.Backend.Profiles;
using CodeReviewManager.UI.Components;
using CodeReviewManager.UI.Util;
using CodeReviewManager.Util;
using FormData = System.Collections.Generic.Dictionary<string, string>;

namespace CodeReviewManager.UI.Views {

	/// <summary>
	/// Interaction logic for IntegrationsView.xaml
	/// </summary>
	public partial class IntegrationsView : UserControl {

		private ISourceControlIntegrationInfo m_integration; // The integration type current being added (or NULL)
		private ISourceControlSignInInfo m_signIn; // The sign in to be deleted
		private CancellationTokenSource m_cancellationTokenSource;
		private readonly IList<FieldMapping> m_fieldMappings;
		private readonly IList<HiddenField> m_hiddenFields;

		public IntegrationsView() {
			InitializeComponent();

			int i = 0;
			foreach( ISourceControlIntegrationInfo integration in CodeReviewManagerCore.Instance.Integrations ) {
				IntegrationTypeButton button = new IntegrationTypeButton( integration );
				button.OnPress += IntegrationTypeSelected;
				IntegrationTypes.Children.Add( button );
				Grid.SetRow( button, i / 3 );
				Grid.SetColumn( button, i % 3 );
				i++;
			}

			RefreshIntegrations();

			m_fieldMappings = new List<FieldMapping>();
			m_hiddenFields = new List<HiddenField>();
			m_integration = null;

			CodeReviewManagerCore.Instance.OnProfileChanged += ProfileChanged;
		}

		~IntegrationsView() {
			CodeReviewManagerCore.Instance.OnProfileChanged -= ProfileChanged;
		}

		private void ProfileChanged( IProfile newProfile ) {
			m_integration = null;
			Dispatcher.Invoke( RefreshIntegrations );
		}

		private void OnDeleteClicked(
			ISourceControlIntegrationInfo integration,
			ISourceControlSignInInfo signIn
		) {
			m_integration = integration;
			m_signIn = signIn;

			this.DeletedSignInIcon.Type = integration.Type;
			this.DeletedSignInType.Content = integration.Name;
			this.DeletedSignInLocationInfo.Content = signIn.LocationInfo ?? string.Empty;
			this.DeletedSignInUsername.Content = signIn.Username ?? "Unknown";

			this.IntegrationsListView.Visibility = Visibility.Collapsed;
			this.ConfirmDeleteView.Visibility = Visibility.Visible;
		}

		public void RefreshIntegrations() {
			this.IntegrationList.Children.Clear();
			foreach( ISourceControlIntegrationInfo integration in CodeReviewManagerCore.Instance.Integrations ) {
				foreach( ISourceControlSignInInfo signIn in integration.SignIns ) {
					this.IntegrationList.Children.Add( new Integration( integration, signIn, OnDeleteClicked ) );
				}
			}

			Placeholder createButton = new Placeholder( "➕ New Integration" );
			createButton.Click += ( source, args ) => {
				this.IntegrationsListView.Visibility = Visibility.Collapsed;
				this.ChooseTypeView.Visibility = Visibility.Visible;
			};
			this.IntegrationList.Children.Add( createButton );
		}

		private void ShowIntegrationListView() {
			this.MfaView.Visibility = Visibility.Collapsed;
			this.SignInOutView.Visibility = Visibility.Collapsed;
			this.ChooseTypeView.Visibility = Visibility.Collapsed;
			this.ConfirmDeleteView.Visibility = Visibility.Collapsed;

			m_integration = null;

			RefreshIntegrations();
			this.IntegrationsListView.Visibility = Visibility.Visible;
		}

		private void IntegrationTypeSelected( ISourceControlIntegrationInfo integration ) {
			ShowSignInOrOutView( integration, 0 );
		}

		private void ShowSignInOrOutView( ISourceControlIntegrationInfo integration, uint mode ) {
			this.ChooseTypeView.Visibility = Visibility.Collapsed;
			this.SignInOutView.Visibility = Visibility.Visible;
			this.IntegrationsListView.Visibility = Visibility.Collapsed;
			this.MfaView.Visibility = Visibility.Collapsed;

			m_integration = integration;
			this.SourceIcon.Type = integration.Type;

			ModeViewSpec uiSpec = integration.GetUiSpecAsync().WaitSync()[mode]; // fixme

			m_fieldMappings.Clear();
			m_hiddenFields.Clear();
			this.Form.Children.Clear();
			this.SignInMessageArea.Inlines.Clear();

			foreach( MessageFragment fragment in uiSpec.Message ) {
				if( fragment.LinksTo.HasValue ) {
					Hyperlink link = new Hyperlink( new Run( fragment.Text ) );
					link.Foreground = Application.Current.Resources["Brush01"] as Brush;
					if( fragment.LinksTo >= 0 ) {
						link.Click += ( s, e ) => ShowSignInOrOutView( integration, (uint)fragment.LinksTo.Value );
					} else {
						link.Click += ( s, e ) => DefaultWebBrowser.Open( fragment.Text );
					}
					this.SignInMessageArea.Inlines.Add( link );
				} else {
					this.SignInMessageArea.Inlines.Add( new Run( fragment.Text ) );
				}
			}

			int fieldNum = 0;
			foreach( Field field in uiSpec.Fields ) {
				Func<string> valueGetter;
				Control fieldElement;
				if( field.IsPassword ) {
					PasswordBox passwordBox = new PasswordBox();
					valueGetter = () => field.ValueTransform( passwordBox.Password );
					fieldElement = passwordBox;
				} else {
					TextBox textBox = new TextBox();
					textBox.IsReadOnly = field.Readonly;
					textBox.Text = field.DefaultValue ?? string.Empty;
					valueGetter = () => field.ValueTransform( textBox.Text );
					fieldElement = textBox;
				}

				int nextField = ++fieldNum;
				fieldElement.KeyDown += ( object sender, KeyEventArgs @event ) => {
					if( @event.Key == Key.Enter ) {
						if( nextField >= m_fieldMappings.Count ) {
							this.SignInButton_Click( sender, @event );
						} else {
							m_fieldMappings[nextField].Field.Focus();
						}
					}
				};

				this.Form.Children.Add( new Label { Content = field.Name } );
				this.Form.Children.Add( fieldElement );
				m_fieldMappings.Add( new FieldMapping( field.Id, valueGetter, fieldElement ) );
			}

			foreach( HiddenField field in uiSpec.HiddenFields ) {
				m_hiddenFields.Add( field );
			}

			this.ErrorLabel.Visibility = Visibility.Hidden;
			this.SignInOutView.Visibility = Visibility.Visible;

			if( m_fieldMappings.Count > 0 ) {
				m_fieldMappings[0].Field.Focus();
			}
		}

		private void ShowMfaView() {
			this.SignInOutView.Visibility = Visibility.Collapsed;

			this.MfaCodeField.Text = string.Empty;
			this.MfaErrorLabel.Visibility = Visibility.Hidden;

			this.MfaView.Visibility = Visibility.Visible;
			this.MfaCodeField.Focus();
		}

		private void CancelButton_Click( object sender, RoutedEventArgs e ) {
			m_cancellationTokenSource?.Cancel();
			SetLoading( false );
			this.ShowIntegrationListView();
		}

		private void DeleteButton_Click( object sender, RoutedEventArgs e ) {
			m_integration.RemoveSignIn( m_signIn ).ThenDispatch( RefreshIntegrations );
			this.ShowIntegrationListView();
		}

		private void SignInButton_Click( object sender, RoutedEventArgs e ) {
			SetLoading( true );
			TrySignInAsync().ThenDispatch( then: authResult => {
				switch( authResult.Type ) {
					case AuthResultType.Success:
						ShowIntegrationListView();
						break;
					case AuthResultType.Requires2FA:
						ShowMfaView();
						break;
					case AuthResultType.Denied:
						this.ErrorLabel.Text = authResult.Message ?? "Login Failed";
						this.ErrorLabel.Visibility = Visibility.Visible;
						break;
					case AuthResultType.Error:
						this.ErrorLabel.Text = authResult.Message ?? $"Error Connecting to {m_integration.Name}";
						this.ErrorLabel.Visibility = Visibility.Visible;
						break;
				}
			}, @catch: exception => {
				this.ErrorLabel.Text = $"Error Connecting to {m_integration.Name}";
				this.ErrorLabel.Visibility = Visibility.Visible;
			}, @finally: () => {
				CodeReviewManagerCore.Instance.UpdateAsync( force: true ).SafeDetach();
				SetLoading( false );
			});
		}

		private void VerifyButton_Click( object sender, RoutedEventArgs e ) {
			SetLoading( true );
			TrySignInAsync().ThenDispatch( then: authResult => {
				switch( authResult.Type ) {
					case AuthResultType.Success:
						ShowIntegrationListView();
						break;
					case AuthResultType.Requires2FA:
					case AuthResultType.Denied:
						this.MfaErrorLabel.Content = authResult.Message ?? "Verification Failed";
						this.MfaErrorLabel.Visibility = Visibility.Visible;
						break;
					case AuthResultType.Error:
						this.MfaErrorLabel.Content = authResult.Message ?? $"Error Connecting to {m_integration.Name}";
						this.MfaErrorLabel.Visibility = Visibility.Visible;
						break;
				}
			}, @catch: exception => {
				this.MfaErrorLabel.Content = $"Error Connecting to {m_integration.Name}";
				this.MfaErrorLabel.Visibility = Visibility.Visible;
			}, @finally: () => {
				SetLoading( false );
			});
		}

		private Task<AuthResult> TrySignInAsync() {
			FormData formData = new FormData();
			foreach( FieldMapping fieldMapping in m_fieldMappings ) {
				formData[fieldMapping.Id] = fieldMapping.Getter();
			}

			foreach( HiddenField hiddenFields in m_hiddenFields ) {
				formData[hiddenFields.Id] = hiddenFields.Value;
			}

			return m_integration.TryCreateSignInAsync( formData, this.MfaCodeField.Text, m_cancellationTokenSource.Token );
		}

		private void SetLoading( bool isLoading ) {
			m_cancellationTokenSource = isLoading ? new CancellationTokenSource() : null;

			foreach( FieldMapping fieldMapping in m_fieldMappings ) {
				fieldMapping.Field.IsEnabled = !isLoading;
			}
			this.MfaCodeField.IsEnabled = !isLoading;
			this.SignInButton.IsEnabled = !isLoading;
			this.VerifyButton.IsEnabled = !isLoading;
		}

		private void MfaCodeField_KeyDown( object sender, KeyEventArgs e ) {
			if( e.Key == Key.Enter ) {
				this.VerifyButton_Click( sender, e );
			}
		}

		private struct FieldMapping {

			public FieldMapping(
				string id,
				Func<string> getter,
				Control field
			) {
				Id = id;
				Getter = getter;
				Field = field;
			}

			public readonly string Id;
			public readonly Func<string> Getter;
			public readonly Control Field;
		}

	}
}
