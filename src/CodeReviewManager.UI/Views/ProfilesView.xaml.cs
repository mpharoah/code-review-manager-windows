﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Profiles;
using CodeReviewManager.UI.Components;

namespace CodeReviewManager.UI.Views {

    /// <summary>
    /// Interaction logic for ProfilesView.xaml
    /// </summary>
	public partial class ProfilesView : UserControl {

		private Action m_confirmAction = null;

		public ProfilesView() {
			InitializeComponent();
			RefreshAll();

			CodeReviewManagerCore.Instance.OnProfileChanged += OnProfileChanged;
		}

		~ProfilesView() {
			CodeReviewManagerCore.Instance.OnProfileChanged -= OnProfileChanged;
		}

		private void RefreshAll() {
			this.ProfileList.Children.Clear();
			foreach( IProfile profile in ProfileManager.FetchAll() ) {
				Profile profileComponent = new Profile( profile );
				profileComponent.OnClickRename += OnRenameStart;
				profileComponent.OnClickDelete += OnDeleteStart;
				this.ProfileList.Children.Add( profileComponent );
			}
			Placeholder addButton = new Placeholder( "➕ New Profile" );
			addButton.Click += OnAddStart;
			this.ProfileList.Children.Add( addButton );
		}

		private void OnProfileChanged( IProfile profile ) {
			Dispatcher.Invoke( () => {
				RefreshAll();
				this.ActionView.Visibility = Visibility.Collapsed;
				this.ListView.Visibility = Visibility.Visible;
			});
		}

		private void OnRenameStart( IProfile profile ) {
			this.ListView.Visibility = Visibility.Collapsed;
			this.ActionView.Visibility = Visibility.Visible;

			this.ActionMessage.Content = "Enter a new name for this profile:";
			this.ActionCheckbox.Visibility = Visibility.Collapsed;
			this.ActionField.Visibility = Visibility.Visible;
			this.ActionField.Text = profile.Name;
			this.ConfirmButton.Content = "Rename";

			this.ActionField.Focus();
			m_confirmAction = () => {
				profile.Name = this.ActionField.Text;
				Dispatcher.Invoke( RefreshAll );
			};
		}

		private void OnDeleteStart( IProfile profile ) {
			this.ListView.Visibility = Visibility.Collapsed;
			this.ActionView.Visibility = Visibility.Visible;

			this.ActionMessage.Content = $"Are you sure you want to delete '{profile.Name}'?";
			this.ActionCheckbox.Visibility = Visibility.Collapsed;
			this.ActionField.Visibility = Visibility.Collapsed;
			this.ConfirmButton.Content = "Delete";

			m_confirmAction = () => {
				profile.Delete();
				Dispatcher.Invoke( RefreshAll );
			};
		}

		private void OnAddStart( object sender, RoutedEventArgs e  ) {
			this.ListView.Visibility = Visibility.Collapsed;
			this.ActionView.Visibility = Visibility.Visible;

			this.ActionMessage.Content = "Enter a name for this profile:";
			this.ActionCheckbox.Visibility = Visibility.Visible;
			this.ActionCheckbox.IsChecked = true;
			this.ActionField.Visibility = Visibility.Visible;
			this.ActionField.Text = string.Empty;
			this.ConfirmButton.Content = "Create";

			this.ActionField.Focus();
			m_confirmAction = () => {
				IProfile newProfile = ProfileManager.Create( this.ActionField.Text, this.ActionCheckbox.IsChecked ?? false );
				ProfileManager.SwitchTo( newProfile );
				RefreshAll();
			};
		}

		private void CancelButton_Click( object sender, RoutedEventArgs e ) {
			this.ActionView.Visibility = Visibility.Collapsed;
			this.ListView.Visibility = Visibility.Visible;
			m_confirmAction = null;
		}

		private void ConfirmButton_Click( object sender, RoutedEventArgs e ) {
			this.ActionView.Visibility = Visibility.Collapsed;
			this.ListView.Visibility = Visibility.Visible;
			m_confirmAction?.Invoke();
		}

		private void ActionField_KeyDown( object sender, KeyEventArgs e ) {
			if( e.Key == Key.Enter ) {
				this.ConfirmButton_Click( sender, e );
			}
		}

	}
}

