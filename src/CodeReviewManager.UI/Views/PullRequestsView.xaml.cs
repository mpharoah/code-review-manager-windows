﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Exceptions;
using CodeReviewManager.UI.Components;
using CodeReviewManager.UI.Util;
using CodeReviewManager.Util;
using CodeReviewManager.Util.Logging;

namespace CodeReviewManager.UI.Views {

	/// <summary>
	/// Interaction logic for PullRequestsView.xaml
	/// </summary>
	public partial class PullRequestsView : UserControl, IDisposable {

		private static readonly Brush UPDATE_FAILED_COLOUR = new SolidColorBrush( Color.FromRgb( 0xff, 0x40, 0x40 ) );
		private static readonly Brush WARNING_FOREGROUND_COLOUR = new SolidColorBrush( Color.FromRgb( 0x1a, 0x1a, 0x1e ) );
		private static readonly Brush WARNING_BACKGROUND_COLOUR = new SolidColorBrush( Color.FromRgb( 0xfb, 0xba, 0x11 ) );

		public PullRequestsView() {
			InitializeComponent();
			this.Reload( true );

			var backend = CodeReviewManagerCore.Instance;
			backend.OnBeginUpdate += OnUpdateStarted;
			backend.OnUpdate += OnUpdateFinished;
			backend.OnUpdateFailure += OnUpdateFailed;
			backend.OnOrderChanged += OnOrderChanged;
			backend.OnConfigUpdated += OnSettingsChanged;

			CheckIntegrations();
		}

		public void Dispose() {
			var backend = CodeReviewManagerCore.Instance;
			backend.OnBeginUpdate -= OnUpdateStarted;
			backend.OnUpdate -= OnUpdateFinished;
			backend.OnUpdateFailure -= OnUpdateFailed;
			backend.OnOrderChanged -= OnOrderChanged;
			backend.OnConfigUpdated -= OnSettingsChanged;
		}

		public event Action<IReadOnlyList<IPullRequest>> OnNewPullRequestsAdded;

		public void Reload( bool updateBannerText ) {
			bool differentPullRequests = false;
			bool differentStatuses = false;
			bool differentWarnings = false;

			IReadOnlyList<IPullRequest> newList = CodeReviewManagerCore.Instance.GetPullRequests();
			IReadOnlyList<string> warnings = CodeReviewManagerCore.Instance.GetWarnings().Select(
				warning => string.Concat( "⚠ ", warning )
			).ToArray();

			if( newList.Count != this.PullRequestContainer.Children.Count ) {
				differentPullRequests = true;
				differentStatuses = true;
			} else {
				int i = 0;
				foreach( var pr in this.PullRequestContainer.Children ) {
					PullRequestComponent oldPullRequest = (PullRequestComponent)pr;
					if( !oldPullRequest.Url.Equals( newList[i].Url ) ) {
						differentPullRequests = true;
						differentStatuses = true;
						break;
					}
					if( oldPullRequest.NotificationStatus != newList[i].NotificationStatus ) {
						differentStatuses = true;
					}
					i++;
				}
			}

			if( warnings.Count != this.WarningsContainer.Children.Count ) {
				differentWarnings = true;
			} else {
				int i = 0;
				foreach( var warningLabel in this.WarningsContainer.Children ) {
					if( !warnings[i++].Equals( (warningLabel as TextBlock)?.Text ) ) {
						differentWarnings = true;
						break;
					}
				}
			}

			double previousScrollPosition = this.ScrollView.VerticalOffset;

			this.PullRequestContainer.Children.Clear();
			foreach( IPullRequest pullRequest in newList ) {
				this.PullRequestContainer.Children.Add( new PullRequestComponent( pullRequest ) );
			}

			if( differentWarnings ) {
				this.WarningsContainer.Children.Clear();
				foreach( string warningString in warnings ) {
					TextBlock warning = new TextBlock();
					warning.Text = warningString;
					warning.FontSize = 13;
					warning.Padding = new Thickness( 4 );
					warning.TextWrapping = TextWrapping.Wrap;
					warning.Foreground = WARNING_FOREGROUND_COLOUR;
					warning.Background = WARNING_BACKGROUND_COLOUR;
					this.WarningsContainer.Children.Add( warning );
				}
			}

			if( !differentPullRequests && !differentWarnings ) {
				this.ScrollView.ScrollToVerticalOffset( previousScrollPosition );
			}

			if( differentPullRequests || differentStatuses || differentWarnings ) {
				// Disable interaction for 0.5 seconds to prevent misclicks
				this.Blocker.Visibility = Visibility.Visible;
				this.PullRequestContainer.Effect = new BlurEffect { Radius = 8.0 };

				Task.Delay( TimeSpan.FromSeconds( 0.5 ) ).ThenDispatch( () => {
					this.Blocker.Visibility = Visibility.Collapsed;
					this.PullRequestContainer.Effect = null;
				} );
			}

			if( updateBannerText ) {
				this.BannerText.Foreground = Palette.Grey;
				this.BannerText.Content = string.Concat(
					$"Reviewing {newList.Count} pull request",
					( newList.Count == 1 ) ? string.Empty : "s"
				);
			}

			this.RefreshButton.IsEnabled = true;
			CheckIntegrations();
		}

		private void OnSettingsChanged( ConfigUpdatedEvent @event ) {
			// Other option changes are already handled. Might want to refactor?
			if( @event.OldConfig.ShowAvatars != @event.NewConfig.ShowAvatars ) {
				OnOrderChanged();
			}
		}

		private void OnOrderChanged() {
			Application.Current.Dispatcher.Invoke( () => {
				IReadOnlyList<IPullRequest> newOrder = CodeReviewManagerCore.Instance.GetPullRequests();
				this.PullRequestContainer.Children.Clear();
				foreach( IPullRequest pullRequest in newOrder ) {
					this.PullRequestContainer.Children.Add( new PullRequestComponent( pullRequest ) );
				}
			});
		}

		private void CheckIntegrations() {
			bool signedIn = CodeReviewManagerCore.Instance.Integrations.Any(
				integration => integration.SignIns.Any( signIn => !signIn.Errored )
			);

			bool hasPullRequests = this.PullRequestContainer.Children.Count > 0;

			this.NoIntegrationsMessage.Visibility = !signedIn ? Visibility.Visible : Visibility.Collapsed;
			this.NoPullRequestsMessage.Visibility = ( signedIn && !hasPullRequests ) ? Visibility.Visible : Visibility.Collapsed;
		}

		private void OnUpdateStarted() {
			Application.Current.Dispatcher.Invoke( () => {
				this.BannerText.Foreground = Palette.Blue;
				this.BannerText.Content = "Updating...";
				this.RefreshButton.IsEnabled = false;
			});
		}

		private void OnUpdateFinished( PullRequestsUpdatedEvent @event ) {
			Application.Current.Dispatcher.Invoke( () => {
				if( @event.NewPullRequests.Count > 0 ) {
					this.OnNewPullRequestsAdded?.Invoke( @event.NewPullRequests );
				}
				this.Reload( !@event.CompletedWithErrors );
			});
			CodeReviewManagerCore.Instance.SavePullRequestsAsync().SafeDetach();
		}

		private void OnUpdateFailed( UpdateFailedException exception ) {
			Application.Current.Dispatcher.Invoke( () => {
				Log.WriteException( exception, "Failed to update active pull request list" );
				this.BannerText.Foreground = UPDATE_FAILED_COLOUR;
				this.BannerText.Content = "Failed to fetch active pull requests";
				this.RefreshButton.IsEnabled = true;
			});
		}

		private void RefreshButton_Click( object sender, RoutedEventArgs e ) {
			CodeReviewManagerCore.Instance.UpdateAsync( force: true ).SafeDetach();
		}

		private void UserControl_IsVisibleChanged( object sender, DependencyPropertyChangedEventArgs e ) {
			CheckIntegrations();
		}

	}

}
