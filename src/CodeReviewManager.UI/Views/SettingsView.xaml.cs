﻿using System.Threading.Tasks;
using System.Windows.Controls;
using CodeReviewManager.Backend;
using CodeReviewManager.Backend.Config;
using CodeReviewManager.Backend.Profiles;
using CodeReviewManager.Util;
using UnmuteBehaviour = CodeReviewManager.Backend.Config.ConfigData.UnmuteBehaviour;

namespace CodeReviewManager.UI.Views {

	/// <summary>
	/// Interaction logic for SettingsView.xaml
	/// </summary>
	public partial class SettingsView : UserControl {

		private ConfigData m_settings;

		public SettingsView() {
			m_settings = CodeReviewManagerCore.Instance.GetSettings();
			DataContext = this;
			InitializeComponent();
			this.SortOrderComboBox.SelectedItem = this.SortOrder;

			CodeReviewManagerCore.Instance.OnProfileChanged += ProfileChanged;
		}

		~SettingsView() {
			CodeReviewManagerCore.Instance.OnProfileChanged -= ProfileChanged;
		}

		private void ProfileChanged( IProfile newProfile ) {
			DataContext = null;
			m_settings = CodeReviewManagerCore.Instance.GetSettings();
			DataContext = this;
		}

		public Task CommitChangesAsync() {
			return CodeReviewManagerCore.Instance.UpdateSettingsAsync( m_settings );
		}

		public uint RefreshInterval {
			get => m_settings.RefreshIntervalMinutes;
			set => m_settings.RefreshIntervalMinutes = value;
		}

		public bool RefreshOnOpen {
			get => m_settings.RefreshOnOpen;
			set => m_settings.RefreshOnOpen = value;
		}

		public bool IncludeSelfAssigned {
			get => m_settings.IncludeSelfAssigned;
			set => m_settings.IncludeSelfAssigned = value;
		}

		public uint NotificationInterval {
			get => m_settings.NotificationIntervalMinutes;
			set => m_settings.NotificationIntervalMinutes = value;
		}

		public bool NotifyOnNewPullRequest {
			get => m_settings.NotifyOnNewPullRequest;
			set => m_settings.NotifyOnNewPullRequest = value;
		}

		public bool NotifyOnStartup {
			get => m_settings.NotifyOnStartup;
			set => m_settings.NotifyOnStartup = value;
		}

		public bool MuteNewPullRequests {
			get => m_settings.MuteNewPullRequests;
			set => m_settings.MuteNewPullRequests = value;
		}

		public bool MuteAfterCommenting {
			get => m_settings.MuteAfterCommenting;
			set => m_settings.MuteAfterCommenting = value;
		}

		public uint SnoozeDuration {
			get => m_settings.SnoozeTimeHours;
			set => m_settings.SnoozeTimeHours = value;
		}

		public bool UnmuteOnPush_Always {
			get => m_settings.UnmuteOnPush == UnmuteBehaviour.AlwaysUnmute;
			set { if( value ) m_settings.UnmuteOnPush = UnmuteBehaviour.AlwaysUnmute; }
		}
		public bool UnmuteOnPush_Auto {
			get => m_settings.UnmuteOnPush == UnmuteBehaviour.UnmuteIfAutoMuted;
			set { if( value ) m_settings.UnmuteOnPush = UnmuteBehaviour.UnmuteIfAutoMuted; }
		}
		public bool UnmuteOnPush_Never {
			get => m_settings.UnmuteOnPush == UnmuteBehaviour.NeverUnmute;
			set { if( value ) m_settings.UnmuteOnPush = UnmuteBehaviour.NeverUnmute; }
		}

		public bool UnmuteOnComment_Always {
			get => m_settings.UnmuteOnAuthorComment == UnmuteBehaviour.AlwaysUnmute;
			set { if( value ) m_settings.UnmuteOnAuthorComment = UnmuteBehaviour.AlwaysUnmute; }
		}
		public bool UnmuteOnComment_Auto {
			get => m_settings.UnmuteOnAuthorComment == UnmuteBehaviour.UnmuteIfAutoMuted;
			set { if( value ) m_settings.UnmuteOnAuthorComment = UnmuteBehaviour.UnmuteIfAutoMuted; }
		}
		public bool UnmuteOnComment_Never {
			get => m_settings.UnmuteOnAuthorComment == UnmuteBehaviour.NeverUnmute;
			set { if( value ) m_settings.UnmuteOnAuthorComment = UnmuteBehaviour.NeverUnmute; }
		}

		public bool UnmuteOnMention_Always {
			get => m_settings.UnmuteOnMention == UnmuteBehaviour.AlwaysUnmute;
			set { if( value ) m_settings.UnmuteOnMention = UnmuteBehaviour.AlwaysUnmute; }
		}
		public bool UnmuteOnMention_Auto {
			get => m_settings.UnmuteOnMention == UnmuteBehaviour.UnmuteIfAutoMuted;
			set { if( value ) m_settings.UnmuteOnMention = UnmuteBehaviour.UnmuteIfAutoMuted; }
		}
		public bool UnmuteOnMention_Never {
			get => m_settings.UnmuteOnMention == UnmuteBehaviour.NeverUnmute;
			set { if( value ) m_settings.UnmuteOnMention = UnmuteBehaviour.NeverUnmute; }
		}

		public bool ShowAuthorAvatars {
			get => m_settings.ShowAvatars;
			set => m_settings.ShowAvatars = value;
		}

		public ComboBoxItem SortOrder {
			get {
				switch( m_settings.OrderBy ) {
					case ConfigData.SortOrder.OpenDate: return SortOrder_DateOpened;
					case ConfigData.SortOrder.LastUpdated: return SortOrder_LastUpdate;
					case ConfigData.SortOrder.LastAuthorComment: return SortOrder_LastAuthorComment;
					case ConfigData.SortOrder.LastReviewerComment: return SortOrder_MyLastComment;
					case ConfigData.SortOrder.LastPush: return SortOrder_LastPush;
				}
				return null;
			}
			set {
				if( value == SortOrder_DateOpened ) {
					m_settings.OrderBy = ConfigData.SortOrder.OpenDate;
				} else if( value == SortOrder_LastUpdate ) {
					m_settings.OrderBy = ConfigData.SortOrder.LastUpdated;
				} else if( value == SortOrder_LastAuthorComment ) {
					m_settings.OrderBy = ConfigData.SortOrder.LastAuthorComment;
				} else if( value == SortOrder_MyLastComment ) {
					m_settings.OrderBy = ConfigData.SortOrder.LastReviewerComment;
				} else if( value == SortOrder_LastPush ) {
					m_settings.OrderBy = ConfigData.SortOrder.LastPush;
				}
			}
		}

		public bool NewestAtTop {
			get => m_settings.NewestAtTop;
			set => m_settings.NewestAtTop = value;
		}

		public bool MutedAtBottom {
			get => m_settings.MutedAtBottom;
			set => m_settings.MutedAtBottom = value;
		}

		public bool DockedToTray {
			get => m_settings.DockedToTray;
			set {
				m_settings.DockedToTray = value;
				CodeReviewManagerCore.Instance.UpdateSettingsAsync( m_settings ).SafeDetach();
			}
		}

	}

}
