﻿using System;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	internal sealed class AsyncIteratorChain<TIn,TOut> : IAsyncIterator<TOut> {

		private readonly IAsyncIterator<TIn> m_inner;
		private readonly Func<TIn, Task<Optional<TOut>>> m_transform;
		private TOut m_current;

		internal AsyncIteratorChain(
			IAsyncIterator<TIn> sourceIterator,
			Func<TIn, Task<Optional<TOut>>> transform
		) {
			m_inner = sourceIterator;
			m_transform = transform;
		}

		internal AsyncIteratorChain(
			IAsyncIterator<TIn> sourceIterator,
			Func<TIn, Optional<TOut>> transform
		) {
			m_inner = sourceIterator;
			m_transform = input => Task.FromResult( transform( input ) );
		}

		async Task<bool> IAsyncIterator<TOut>.MoveNextAsync() {
			while( true ) {
				if( !( await m_inner.MoveNextAsync().SafeAsync() ) ) {
					return false;
				}

				Optional<TOut> transformResult = await m_transform( m_inner.Current ).SafeAsync();
				if( transformResult.HasValue ) {
					m_current = transformResult.Value;
					return true;
				}
			}
		}

		TOut IAsyncIterator<TOut>.Current => m_current;

	}

}
