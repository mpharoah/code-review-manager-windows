﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	public sealed class AsyncLock {

		private readonly SemaphoreSlim m_semaphore;

		public AsyncLock() {
			m_semaphore = new SemaphoreSlim( 1, 1 );
		}

		public async Task<IDisposable> AcquireAsync() {
			await m_semaphore.WaitAsync().SafeAsync();
			return new DisposeAction( () => m_semaphore.Release() );
		}

		~AsyncLock() {
			m_semaphore.Dispose();
		}

	}

}
