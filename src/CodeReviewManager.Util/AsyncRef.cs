﻿namespace CodeReviewManager.Util {

	public sealed class AsyncRef<T> {

		public AsyncRef( T value = default ) {
			this.Value = value;
		}

		public T Value { get; set; }

	}

}
