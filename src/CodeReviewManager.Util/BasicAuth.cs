﻿using System;
using System.Net.Http.Headers;
using System.Text;

namespace CodeReviewManager.Util {

	public sealed class BasicAuth : AuthenticationHeaderValue {

		public BasicAuth( string username, string password ) : base(
			scheme: "Basic",
			parameter: Convert.ToBase64String( 
				Encoding.UTF8.GetBytes( $"{username}:{password}" )
			)
		) {}

	}

}
