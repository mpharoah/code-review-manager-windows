﻿using System;

namespace CodeReviewManager.Util {

	internal sealed class DisposeAction : IDisposable {

		private readonly Action m_action;

		public DisposeAction( Action onDispose ) {
			m_action = onDispose;
		}

		void IDisposable.Dispose() {
			m_action.Invoke();
		}

	}

}
