﻿using System;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	[EditorBrowsable( EditorBrowsableState.Never )]
	public static class HttpClientExtensions {

		public static async Task<SimpleResponse> SimpleSendAsync(
			this HttpClient httpClient,
			HttpMethod method,
			Uri uri,
			bool ensureSuccess = false
		) {
			using( HttpRequestMessage request = new HttpRequestMessage( method, uri ) )
			using( HttpResponseMessage response = await httpClient.SendAsync( request ).SafeAsync() ) {
				string responseBody = null;
				if( response.Content != null ) {
					responseBody = await response.Content.ReadAsStringAsync().SafeAsync();
				}

				if( ensureSuccess ) {
					response.EnsureSuccessStatusCode();
				}

				return new SimpleResponse(
					response.StatusCode,
					responseBody,
					response.IsSuccessStatusCode
				);
			}
		}

	}

	public sealed class SimpleResponse {

		internal SimpleResponse(
			HttpStatusCode statusCode,
			string response,
			bool isSuccess
		) {
			StatusCode = statusCode;
			ResponseBody = response;
			IsSuccessStatusCode = isSuccess;
		}

		public HttpStatusCode StatusCode { get; }
		public string ResponseBody { get; }
		public bool IsSuccessStatusCode { get; }

	}

}
