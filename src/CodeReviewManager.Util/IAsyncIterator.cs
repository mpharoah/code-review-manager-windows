﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	public interface IAsyncIterator<T> {
		Task<bool> MoveNextAsync();
		T Current { get; }
	}

	[EditorBrowsable( EditorBrowsableState.Never )]
	public static class AsyncIteratorExtensions {

		public static async Task ForEachAsync<T>(
			this IAsyncIterator<T> iterator,
			Action<T> syncAction
		) {
			while( await iterator.MoveNextAsync().SafeAsync() ) {
				syncAction.Invoke( iterator.Current );
			}
		}

		public static async Task ForEachAsync<T>(
			this IAsyncIterator<T> iterator,
			Func<T,Task> asyncAction
		) {
			while( await iterator.MoveNextAsync().SafeAsync() ) {
				await asyncAction.Invoke( iterator.Current ).SafeAsync();
			}
		}

		public static async Task ForEachInParallelAsync<T>(
			this IAsyncIterator<T> iterator,
			Func<T, Task> asyncAction,
			int maxConcurrency
		) {
			Parallelization.ValidateMaxConcurrency( maxConcurrency );

			IList<Task> tasks = new List<Task>();
			using( SemaphoreSlim limiter = new SemaphoreSlim( maxConcurrency ) ) {
				while( await iterator.MoveNextAsync().SafeAsync() ) {
					await limiter.WaitAsync().SafeAsync();
					tasks.Add( asyncAction( iterator.Current ).ReleaseOnCompletion( limiter ) );
				}
				await Parallelization.WaitOnAllTasksAsync( tasks ).SafeAsync();
			}
		}

		public static async Task<IReadOnlyList<T>> FetchRestAsync<T>(
			this IAsyncIterator<T> iterator
		) {
			List<T> results = new List<T>();
			await iterator.ForEachAsync( x => results.Add( x ) ).SafeAsync();
			return results;
		}

		public static IAsyncIterator<TOut> Select<TIn,TOut>(
			this IAsyncIterator<TIn> iterator,
			Func<TIn,TOut> transform
		) {
			return new AsyncIteratorChain<TIn,TOut>(
				iterator,
				input => transform( input )
			);
		}

		public static IAsyncIterator<TOut> Select<TIn, TOut>(
			this IAsyncIterator<TIn> iterator,
			Func<TIn, Task<TOut>> transform
		) {
			return new AsyncIteratorChain<TIn, TOut>(
				iterator,
				async input => await transform( input ).SafeAsync()
			);
		}

		public static IAsyncIterator<T> Where<T>(
			this IAsyncIterator<T> iterator,
			Func<T,bool> predicate
		) {
			return new AsyncIteratorChain<T, T>(
				iterator,
				input => predicate( input ) ? input : Optional<T>.Undefined
			);
		}

		public static IAsyncIterator<T> Where<T>(
			this IAsyncIterator<T> iterator,
			Func<T, Task<bool>> predicate
		) {
			return new AsyncIteratorChain<T, T>(
				iterator,
				async input => await predicate( input ).SafeAsync() ? input : Optional<T>.Undefined
			);
		}

		public static IAsyncIterator<TOut> SelectWhere<TIn,TOut>(
			this IAsyncIterator<TIn> iterator,
			Func<TIn,Optional<TOut>> transform
		) {
			return new AsyncIteratorChain<TIn,TOut>( iterator, transform );
		}

		public static IAsyncIterator<TOut> SelectWhere<TIn, TOut>(
			this IAsyncIterator<TIn> iterator,
			Func<TIn, Task<Optional<TOut>>> transform
		) {
			return new AsyncIteratorChain<TIn, TOut>( iterator, transform );
		}

	}

}

