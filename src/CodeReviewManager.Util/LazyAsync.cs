﻿using System;
using System.Threading.Tasks;
using CodeReviewManager.Util.Logging;

namespace CodeReviewManager.Util {

	public sealed class LazyAsync<T> {

		private T m_value;
		private bool m_hasValue;
		private readonly Func<Task<T>> m_valueFactory;

		public LazyAsync(
			Func<Task<T>> asyncGenerator,
			bool prefetch = false
		) {
			m_value = default( T );
			m_hasValue = false;
			m_valueFactory = asyncGenerator;

			if( prefetch ) {
				this.Prefetch();
			}
		}

		public async void Prefetch() {
			try {
				await this.GetValue().SafeAsync();
			} catch( Exception exception ) {
				Log.WriteException( exception, "LazyAsync: Prefetch() Failed"  );
			}
		}

		public bool TryGetImmediately( out T value ) {
			value = m_value;
			return m_hasValue;
		}

		public async Task<T> GetValue() {
			if( !m_hasValue ) {
				m_value = await m_valueFactory().SafeAsync();
				m_hasValue = true;
			}

			return m_value;
		}

		public void Forget() {
			m_hasValue = false;
			m_value = default( T );
		}
		
	}

}
