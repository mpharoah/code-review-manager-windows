﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace CodeReviewManager.Util {

	public sealed class LinkHeaderPagedRequest : OnDemandPagedIterator<JToken> {

		private static Regex m_nextLinkRegex = new Regex(
			@"<([^>]+)>;\s*rel=""next""",
			RegexOptions.Compiled | RegexOptions.IgnoreCase
		);

		private readonly Func<HttpClient> m_httpClientFactory;
		private readonly string[] m_pageDataPath;
		private Uri m_nextUri;

		public LinkHeaderPagedRequest(
			Func<HttpClient> httpClientFactory,
			Uri firstPageUri,
			string[] pageDataPath = null
		) {
			m_httpClientFactory = httpClientFactory;
			m_nextUri = firstPageUri;
			m_pageDataPath = pageDataPath ?? new string[0];
		}

		protected override async Task<IEnumerable<JToken>> TryFetchNextPageAsync() {
			if( m_nextUri == null ) {
				return null;
			}

			using( HttpClient httpClient = m_httpClientFactory.Invoke() )
			using( HttpResponseMessage response = await httpClient.GetAsync( m_nextUri ).SafeAsync() ) {
				response.EnsureSuccessStatusCode();

				JToken responseBody = JToken.Parse( await response.Content.ReadAsStringAsync().SafeAsync() );

				m_nextUri = null;
				if( response.Headers.TryGetValues( "Link", out IEnumerable<string> links ) ) {
					foreach( string link in links ) {
						Match match = m_nextLinkRegex.Match( link );
						if( match.Success ) {
							m_nextUri = new Uri( match.Groups[1].Value );
							break;
						}
					}
				}

				return GetPageData( responseBody );
			}
		}

		private JArray GetPageData( JToken token ) {
			foreach( string property in m_pageDataPath ) {
				token = token[property];
			}
			return (JArray)token;
		}

	}

}
