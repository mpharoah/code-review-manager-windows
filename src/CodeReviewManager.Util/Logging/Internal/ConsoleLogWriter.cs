﻿using System;
using System.Threading.Tasks;

namespace CodeReviewManager.Util.Logging.Internal {

	internal sealed class ConsoleLogWriter : ILogWriter {

		Task ILogWriter.WriteAsync( string message ) {
			return Console.Error.WriteLineAsync( message );
		}

	}

}
