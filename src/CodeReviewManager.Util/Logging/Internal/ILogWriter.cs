﻿using System.Threading.Tasks;

namespace CodeReviewManager.Util.Logging.Internal {

	internal interface ILogWriter {

		Task WriteAsync( string message );

	}

}
