﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace CodeReviewManager.Util.Logging.Internal {

	internal sealed class RollingFileLogWriter : ILogWriter {

		private readonly string m_path;
		private readonly int m_lastFileNumber;
		private readonly long m_maxFileSize;

		private FileStream m_logFile;

		public RollingFileLogWriter(
			string logFilePath,
			int maxFiles,
			long maxFileSize
		) {
			if( maxFiles < 1 ) {
				throw new ArgumentException( "maxFiles must be a positive integer" );
			}

			m_path = logFilePath;
			m_lastFileNumber = maxFiles - 1;
			m_maxFileSize = maxFileSize;
			m_logFile = null;
		}

		async Task ILogWriter.WriteAsync( string message ) {
			if( m_logFile == null ) {
				Directory.CreateDirectory( Path.GetDirectoryName( m_path ) );
				m_logFile = new FileStream( m_path, FileMode.Append, FileAccess.Write );
			}

			if( m_logFile.Position > m_maxFileSize ) {
				await RotateLogsAsync().SafeAsync();
			}

			byte[] data = Encoding.UTF8.GetBytes( message );
			await m_logFile.WriteAsync( data, 0, data.Length ).SafeAsync();
			await m_logFile.FlushAsync().SafeAsync();
		}

		private async Task RotateLogsAsync() {
			if( m_logFile != null ) {
				await m_logFile.FlushAsync().SafeAsync();
				m_logFile.Dispose();
				m_logFile = null;
			}

			if( m_lastFileNumber == 0 ) {
				File.Delete( m_path );
			} else {
				File.Delete( $"{m_path}.{m_lastFileNumber}" );
				for( int i = m_lastFileNumber - 1; i > 0; i-- ) {
					if( File.Exists( $"{m_path}.{i}" ) ) {
						File.Move( $"{m_path}.{i}", $"{m_path}.{i + 1}" );
					}
				}
				File.Move( m_path, $"{m_path}.1" );
			}

			m_logFile = new FileStream( m_path, FileMode.Append, FileAccess.Write );
		}

		~RollingFileLogWriter() {
			m_logFile?.Dispose();
		}

	}

}
