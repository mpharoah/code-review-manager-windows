﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using CodeReviewManager.Util.Logging.Internal;

namespace CodeReviewManager.Util.Logging {

	public static class Log {

		private static readonly string HOME = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile );

		private static readonly ILogWriter[] m_logWriters = {
			new ConsoleLogWriter(),
			new RollingFileLogWriter(
				logFilePath: Path.Combine( HOME, ".config/code-review-manager/logs/log" ),
				maxFiles: 4,
				maxFileSize: 256 * 1024
			)
		};

		private static readonly AsyncLock m_lock = new AsyncLock();

		public static void WriteException(
			Exception exception,
			string message = null
		) {
			string logMessage = string.Empty;
			if( !string.IsNullOrEmpty( message ) ) {
				logMessage = string.Concat( message, Environment.NewLine );
			}

			logMessage = string.Concat( logMessage, exception?.ToString() ?? string.Empty, Environment.NewLine );

			WriteLine( logMessage );
		}

		public static async void WriteLine(
			string message
		) {
			string logMessage = string.Concat( message ?? string.Empty, Environment.NewLine );
			using( await m_lock.AcquireAsync().SafeAsync() ) {
				await m_logWriters.ForEachInParallelAsync( async writer => {
					try {
						await writer.WriteAsync( logMessage ).SafeAsync();
					} catch( Exception ) {}
				}).SafeAsync();
			}
		}

		public struct DebugLoggingContext {

			internal DebugLoggingContext( string name, bool isEnabled ) {
				Name = name;
				Enabled = isEnabled;
			}

			public readonly string Name;
			public readonly bool Enabled;
		}

		/* Debug Logging Switches */
		private const bool ENABLE_GITHUB_LOGGING = false;
		private const bool ENABLE_BITBUCKET_SERVER_LOGGING = false; //TODO: add logging for BitBucket Server integration
		private const bool ENABLE_BITBUCKET_CLOUD_LOGGING = false;
		private const bool ENABLE_MISCELLANEOUS_LOGGING = true;

		public static DebugLoggingContext GitHub = new DebugLoggingContext( "[GITHUB] ", ENABLE_GITHUB_LOGGING );
		public static DebugLoggingContext BitBucketServer = new DebugLoggingContext( "[BITBUCKET SERVER] ", ENABLE_BITBUCKET_SERVER_LOGGING );
		public static DebugLoggingContext BitBucketCloud = new DebugLoggingContext( "[BITBUCKET CLOUD] ", ENABLE_BITBUCKET_CLOUD_LOGGING );
		public static DebugLoggingContext Misc = new DebugLoggingContext( string.Empty, ENABLE_MISCELLANEOUS_LOGGING );

#if DEBUG
		public static void WriteDebug( DebugLoggingContext context, string message ) {
			if( context.Enabled ) {
				WriteLine( string.Concat( "[DEBUG] ", context.Name, message ) );
			}
		}

		public static void WriteDebug( DebugLoggingContext context, Func<string> messageGenerator ) {
			if( context.Enabled ) {
				WriteDebug( context, messageGenerator() );
			}
		}
#else
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static void WriteDebug( DebugLoggingContext context, string message ) {
			// Do not log debug messages in RELEASE configuration
		}

		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static void WriteDebug( DebugLoggingContext context, Func<string> messageGenerator ) {
			// Do not log debug messages in RELEASE configuration
		}
#endif

	}

}
