﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	public abstract class OnDemandPagedIterator<T> : IAsyncIterator<T> {

		private IEnumerator<T> m_currentPageIterator = Enumerable.Empty<T>().GetEnumerator();

		protected abstract Task<IEnumerable<T>> TryFetchNextPageAsync();

		async Task<bool> IAsyncIterator<T>.MoveNextAsync() {
			if( m_currentPageIterator == null ) {
				return false;
			}

			if( m_currentPageIterator.MoveNext() ) {
				return true;
			}

			m_currentPageIterator = ( await TryFetchNextPageAsync().SafeAsync() )?.GetEnumerator();
			return await ( (IAsyncIterator<T>)this ).MoveNextAsync().SafeAsync();
		}

		T IAsyncIterator<T>.Current => m_currentPageIterator.Current;

	}

}
