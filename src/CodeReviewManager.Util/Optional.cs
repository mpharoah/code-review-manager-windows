﻿using System;

namespace CodeReviewManager.Util {

	public struct Optional<T> {

		private readonly T m_value;

		private Optional( bool hasValue, T value = default(T) ) {
			HasValue = hasValue;
			m_value = value;
		}

		public static Optional<T> Undefined = new Optional<T>( false );

		public static implicit operator Optional<T>( T value ) {
			return new Optional<T>( true, value );
		}

		public bool HasValue { get; }
		public T Value {
			get {
				if( !HasValue ) throw new InvalidOperationException();
				return m_value;
			}
		}

		public bool TryGetValue( out T value ) {
			value = m_value;
			return HasValue;
		}

		public static bool operator==( Optional<T> A, Optional<T> B ) {
			return(
				( !A.HasValue && !B.HasValue ) ||
				( A.HasValue && B.HasValue && object.Equals( A.Value, B.Value ) )
			);
		}

		public static bool operator!=( Optional<T> A, Optional<T> B ) {
			return !( A == B );
		}

		public override bool Equals( object obj ) {
			return(
				obj is Optional<T> optional &&
				this == optional
			);
		}

		public override int GetHashCode() {
			return HasValue ? Value.GetHashCode() : -1;
		}

		public override string ToString() {
			return HasValue ? Value.ToString() : "undefined";
		}

	}

}
