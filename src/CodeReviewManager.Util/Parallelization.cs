﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	public static class Parallelization {

		public static Task ForEachInParallelAsync<T>(
			this IEnumerable<T> collection,
			Func<T, Task> function
		) {
			return WaitOnAllTasksAsync( collection.Select( function ) );
		}

		public static async Task ForEachInParallelAsync<T>(
			this IEnumerable<T> collection,
			Func<T, Task> function,
			int maxConcurrency
		) {
			ValidateMaxConcurrency( maxConcurrency );
			IList<Task> tasks = new List<Task>();
			using( SemaphoreSlim limiter = new SemaphoreSlim( maxConcurrency ) ) {
				foreach( T input in collection ) {
					await limiter.WaitAsync().SafeAsync();
					tasks.Add( function( input ).ReleaseOnCompletion( limiter ) );
				}
				await WaitOnAllTasksAsync( tasks ).SafeAsync();
			}
		}

		public static async Task<IEnumerable<T>> FilterInParallelAsync<T>(
			this IEnumerable<T> collection,
			Func<T, Task<bool>> predicate
		) {
			IList<T> storedCollection = collection as IList<T> ?? collection.ToList();
			return Filter(
				storedCollection,
				await storedCollection.MapInParallelAsync( predicate ).SafeAsync()
			);
		}

		public static async Task<IEnumerable<T>> FilterInParallelAsync<T>(
			this IEnumerable<T> collection,
			Func<T, Task<bool>> predicate,
			int maxConcurrency
		) {
			ValidateMaxConcurrency( maxConcurrency );
			IList<T> storedCollection = collection as IList<T> ?? collection.ToList();
			return Filter(
				storedCollection,
				await storedCollection.MapInParallelAsync( predicate, maxConcurrency ).SafeAsync()
			);
		}

		public static async Task<IEnumerable<TOut>> MapInParallelAsync<TIn, TOut>(
			this IEnumerable<TIn> collection,
			Func<TIn, Task<TOut>> transform
		) {
			IList<Task<TOut>> tasks = collection.Select( transform ).ToList();
			await WaitOnAllTasksAsync( tasks ).SafeAsync();
			return tasks.Select( completedTask => completedTask.Result );
		}

		public static async Task<IEnumerable<TOut>> MapInParallelAsync<TIn, TOut>(
			this IEnumerable<TIn> collection,
			Func<TIn, Task<TOut>> transform,
			int maxConcurrency
		) {
			ValidateMaxConcurrency( maxConcurrency );
			IList<Task<TOut>> tasks = new List<Task<TOut>>();
			using( SemaphoreSlim limiter = new SemaphoreSlim( maxConcurrency ) ) {
				foreach( TIn input in collection ) {
					await limiter.WaitAsync().SafeAsync();
					tasks.Add( transform( input ).ReleaseOnCompletion<TOut>( limiter ) );
				}
				await WaitOnAllTasksAsync( tasks ).SafeAsync();
				return tasks.Select( completedTask => completedTask.Result );
			}
		}

		internal static async Task WaitOnAllTasksAsync( IEnumerable<Task> tasks ) {
			Task allTasks = Task.WhenAll( tasks );
			try {
				await allTasks.SafeAsync();
			} catch( Exception exception ) {
				throw allTasks.Exception ?? exception;
			}
		}

		internal static async Task ReleaseOnCompletion(
			this Task task,
			SemaphoreSlim semaphore
		) {
			try {
				await task.SafeAsync();
			} finally {
				semaphore.Release();
			}
		}

		internal static async Task<T> ReleaseOnCompletion<T>(
			this Task<T> task,
			SemaphoreSlim semaphore
		) {
			try {
				return await task.SafeAsync();
			} finally {
				semaphore.Release();
			}
		}

		private static IEnumerable<T> Filter<T>(
			IList<T> values,
			IEnumerable<bool> includeBits
		) {
			int i = 0;
			foreach( bool shouldInclude in includeBits ) {
				if( shouldInclude ) {
					yield return values[i];
				}
				i++;
			}
		}

		internal static void ValidateMaxConcurrency( int maxConcurrency ) {
			if( maxConcurrency <= 0 ) {
				throw new ArgumentOutOfRangeException(
					paramName: nameof( maxConcurrency ),
					message: "maxConcurrency must be a positive integer"
				);
			}
		}

	}

}
