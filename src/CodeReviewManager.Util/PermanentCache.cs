﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	internal sealed class PermanentCache<TKey,TValue> {

		private readonly IDictionary<TKey, TValue> m_cache;

		public PermanentCache() {
			m_cache = new Dictionary<TKey, TValue>();
		}

		public bool TryGetCached( TKey key, out TValue value ) {
			return m_cache.TryGetValue( key, out value );
		}

		public Task<TValue> Fetch( TKey key, Func<TKey, Task<TValue>> asyncFetchFunction ) {
			return this.Fetch( key, () => asyncFetchFunction( key ) );
		}

		public async Task<TValue> Fetch( TKey key, Func<Task<TValue>> asyncFetchFunction ) {
			if( !m_cache.TryGetValue( key, out TValue value ) ) {
				m_cache[key] = value = await asyncFetchFunction().SafeAsync();
			}
			return value;
		}

		public void Set( TKey key, TValue value ) {
			m_cache[key] = value;
		}

		public void Clear( TKey key ) {
			m_cache.Remove( key );
		}

	}

}
