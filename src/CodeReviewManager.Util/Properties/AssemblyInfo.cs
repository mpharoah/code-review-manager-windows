﻿using System.Reflection;
using System.Runtime.InteropServices;
using CodeReviewManager.Util;

[assembly: AssemblyTitle( "CodeReviewManager.Util" )]
[assembly: AssemblyProduct( "CodeReviewManager" )]
[assembly: AssemblyCopyright( "Copyright © Matt Pharoah 2017 - 2023" )]
[assembly: ComVisible( false )]
[assembly: AssemblyVersion( AppInfo.Version )]
