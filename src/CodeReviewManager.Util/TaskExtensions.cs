﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CodeReviewManager.Util.Logging;

namespace CodeReviewManager.Util {

	[EditorBrowsable( EditorBrowsableState.Never )]
	public static class TaskExtensions {

		public static ConfiguredTaskAwaitable SafeAsync( this Task task ) {
			return task.ConfigureAwait( continueOnCapturedContext: false );
		}

		public static ConfiguredTaskAwaitable<T> SafeAsync<T>( this Task<T> task ) {
			return task.ConfigureAwait( continueOnCapturedContext: false );
		}

		public static void WaitSync( this Task task ) {
			task.SafeAsync().GetAwaiter().GetResult();
		}

		public static T WaitSync<T>( this Task<T> task ) {
			return task.SafeAsync().GetAwaiter().GetResult();
		}

		public static async Task<T> CatchAsync<T>( this Task<T> task, T fallback ) {
			try {
				return await task.SafeAsync();
			} catch( Exception ) {
				return fallback;
			}
		}

		public static async void SafeDetach( this Task task ) {
			try {
				await task.SafeAsync();
			} catch( Exception exception ) {
				Log.WriteException( exception, "Exception thrown from detached task." );
			}
		}

	}

}
