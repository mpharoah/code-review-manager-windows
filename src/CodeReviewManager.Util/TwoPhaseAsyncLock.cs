﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodeReviewManager.Util {

	public sealed class TwoPhaseAsyncLock {

		private readonly AsyncLock m_exclusiveLock;
		private readonly ISet<AsyncLock> m_sharedLocks;
		private readonly object m_metaLock;
		
		public TwoPhaseAsyncLock() {
			m_exclusiveLock = new AsyncLock();
			m_sharedLocks = new HashSet<AsyncLock>();
			m_metaLock = new object();
		}

		public async Task<IDisposable> AcquireSharedLockAsync() {

			AsyncLock sharedLock = new AsyncLock();
			IDisposable acquiredSharedLock = await sharedLock.AcquireAsync().SafeAsync();

			using( await m_exclusiveLock.AcquireAsync().SafeAsync() ) {
				lock( m_metaLock ) {
					m_sharedLocks.Add( sharedLock );
				}
			}

			return new DisposeAction( () => {
				lock( m_metaLock ) {
					acquiredSharedLock.Dispose();
					m_sharedLocks.Remove( sharedLock );
				}
			});
		}

		public async Task<IDisposable> AcquireExclusiveLockAsync() {

			IDisposable acquiredExclusiveLock = await m_exclusiveLock.AcquireAsync().SafeAsync();

			IList<AsyncLock> sharedLocksCopy;
			lock( m_metaLock ) {
				sharedLocksCopy = new List<AsyncLock>( m_sharedLocks );
			}

			foreach( AsyncLock sharedLock in sharedLocksCopy ) {
				using( await sharedLock.AcquireAsync().SafeAsync() );
			}

			return acquiredExclusiveLock;
		}

	}

}
