﻿using System;
using System.Text.RegularExpressions;

namespace CodeReviewManager.Util {

	public struct Version {

		private static readonly Regex VERSION_REGEX = new Regex(
			@"^v?(\d+)\.(\d+)\.(\d+)$",
			RegexOptions.Compiled
		);

		public static readonly Version Zero = new Version( 0, 0, 0 );
		public static readonly Version Current = Version.Parse( AppInfo.Version );

		public uint Major { get; }
		public uint Minor { get; }
		public uint Patch { get; }

		public Version( uint major, uint minor, uint patch ) {
			this.Major = major;
			this.Minor = minor;
			this.Patch = patch;
		}

		public static bool TryParse( string versionString, out Version version ) {
			version = Version.Zero;

			if( versionString == null ) {
				return false;
			}

			Match match = VERSION_REGEX.Match( versionString );
			if(
				!match.Success ||
				!uint.TryParse( match.Groups[1].Value, out uint major ) ||
				!uint.TryParse( match.Groups[2].Value, out uint minor ) ||
				!uint.TryParse( match.Groups[3].Value, out uint patch )
			) {
				return false;
			}

			version = new Version( major, minor, patch );
			return true;
		}

		public static Version Parse( string versionString ) {
			if( TryParse( versionString, out Version version ) ) {
				return version;
			}
			throw new FormatException( "Unsupported version format string." );
		}

		public override string ToString() {
			return $"{Major}.{Minor}.{Patch}";
		}

		public override bool Equals( object obj ) {
			return (
				obj is Version other &&
				this.Major == other.Major &&
				this.Minor == other.Minor &&
				this.Patch == other.Patch
			);
		}

		public override int GetHashCode() {
			return this.ToString().GetHashCode();
		}

		public static bool operator ==( Version v1, Version v2 ) {
			return v1.Equals( v2 );
		}

		public static bool operator !=( Version v1, Version v2 ) {
			return !v1.Equals( v2 );
		}

		public static bool operator <( Version v1, Version v2 ) {
			return (
				( v1.Major < v2.Major ) ||
				( v1.Major == v2.Major && v1.Minor < v2.Minor ) ||
				( v1.Major == v2.Major && v1.Minor == v2.Minor && v1.Patch < v2.Patch )
			);
		}

		public static bool operator <=( Version v1, Version v2 ) {
			return ( v1 == v2 || v1 < v2 );
		}

		public static bool operator >=( Version v1, Version v2 ) {
			return !( v1 < v2 );
		}

		public static bool operator >( Version v1, Version v2 ) {
			return !( v1 <= v2 );
		}

	}

}
