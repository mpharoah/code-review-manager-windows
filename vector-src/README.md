This folder contains the source SVG vector images used to generate icons and XAML canvases.

The SVG images can be converted to XAML using Inkscape on Linux or Windows.

The SVG images can be converted to Windows icon files (.ico) by running  the `iconify.sh` shell script with the svg file to convert as an argument. This must be done on a Linux machine with Inkscape and ImageMagick installed.
