sizes=( 16 20 24 32 40 48 64 96 128 256 )
for i in "${sizes[@]}"
do
	inkscape -z -e ${1%.svg}-$i.png -w $i -h $i $1
done

convert ${1%.svg}-*.png ${1%.svg}.ico
rm ${1%.svg}-*.png

